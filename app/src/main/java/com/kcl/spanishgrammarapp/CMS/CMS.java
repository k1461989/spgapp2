package com.kcl.spanishgrammarapp.CMS;

import android.content.Context;

import com.kcl.spanishgrammarapp.R;
import com.kcl.spanishgrammarapp.resources.data.Day;
import com.kcl.spanishgrammarapp.resources.data.Glossary;
import com.kcl.spanishgrammarapp.resources.data.Holiday;
import com.kcl.spanishgrammarapp.resources.data.Letter;
import com.kcl.spanishgrammarapp.resources.data.ListeningComprehension;
import com.kcl.spanishgrammarapp.resources.data.Number;
import com.kcl.spanishgrammarapp.resources.data.Region;
import com.kcl.spanishgrammarapp.resources.data.Season;
import com.kcl.spanishgrammarapp.resources.data.Time;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by janaldoustorres on 29/11/2016.
 */

public class CMS {
    public static int getTopicNo(String topicStr) {
        int topicNo;
        switch(topicStr) {
            case "Greetings": topicNo = 1; break;
            case "Checking-in": topicNo=2; break;
            case "Sightseeing": topicNo=3; break;
            case "Directions": topicNo=4; break;
            case "Eating": topicNo=5; break;
            case "Likes": topicNo = 6; break;
            case "Planning": topicNo = 7; break;
            case "Shopping": topicNo=8; break;
            case "Dating": topicNo=9; break;
            default: topicNo=0;
        }
        return topicNo;
    }

    public static ListeningComprehension getListCompQuestion(Context context, int topicNo) {
        InputStream inputStream = context.getResources().openRawResource(R.raw.listening_comprehension_q_a);
        return new ListeningComprehension(inputStream, topicNo);
    }

    public static ArrayList<Season> getSeasonsAndMonths(Context context) {
        InputStream inputStream = context.getResources().openRawResource(R.raw.resources_seasons_months);
        return Season.getSeasons(inputStream);
    }

    public static ArrayList<Time> getTimes(Context context) {
        InputStream inputStream = context.getResources().openRawResource(R.raw.resources_time);
        return Time.getTimesFromFile(inputStream);
    }

    public static ArrayList<Day> getDays() {
        return Day.getDays();
    }

    public static ArrayList<Holiday> getHolidays(Context context, Region region) {
        InputStream inputStream = context.getResources().openRawResource(R.raw.resources_holidays);
        return Holiday.getHolidays(inputStream, region);
    }

    public static ArrayList<Glossary> getGlossary(Context context) {
        InputStream inputStream = context.getResources().openRawResource(R.raw.glossary);
        return Glossary.getGlossaryFromFile(inputStream);
    }

    public static ArrayList<Number> get0to40(){
        ArrayList<Number> numbers = new ArrayList<>();

        //0-10
        /*
        * 0 - cero
        * 1 - uno
        * 2 - dos
        * 3 - tres
        * 4 - cuatro
        * 5 - cinco
        * 6 - seis
        * 7 - siete
        * 8 - ocho
        * 9 - nueve
        * 10 - diez*/
        Number n1 = new Number("1", "uno", R.raw.sp_mx_01, R.raw.sp_mx_01);
        Number n2 = new Number("2", "dos", R.raw.sp_mx_02, R.raw.sp_mx_02);
        Number n3 = new Number("3", "tres", R.raw.sp_mx_03, R.raw.sp_mx_03);
        Number n4 = new Number("4", "cuatro", R.raw.sp_mx_04, R.raw.sp_mx_04);
        Number n5 = new Number("5", "cinco", R.raw.sp_mx_05, R.raw.sp_mx_05);
        Number n6 = new Number("6", "seis", R.raw.sp_mx_06, R.raw.sp_mx_06);
        Number n7 = new Number("7", "siete", R.raw.sp_mx_07, R.raw.sp_mx_07);
        Number n8 = new Number("8", "ocho", R.raw.sp_mx_08, R.raw.sp_mx_08);
        Number n9 = new Number("9", "nueve", R.raw.sp_mx_09, R.raw.sp_mx_09);
        Number n10 = new Number("10", "diez", R.raw.sp_mx_10, R.raw.sp_mx_10);
        numbers.add(n1);
        numbers.add(n2);
        numbers.add(n3);
        numbers.add(n4);
        numbers.add(n5);
        numbers.add(n6);
        numbers.add(n7);
        numbers.add(n8);
        numbers.add(n9);
        numbers.add(n10);

        //11-20
        /*11 - once
        * 12 - doce
        * 13 - trece
        * 14 - catroce
        * 15 - quince
        * 16 - diez y siedieciséis
        * 17 - diez y sietediecisiete
        * 18 - diez y ochodieciocho
        * 19 - diez y nuevediecinueve
        * 20 - veinte*/
        Number n11 = new Number("11", "once", R.raw.sp_mx_11, R.raw.sp_mx_11);
        Number n12 = new Number("12", "doce", R.raw.sp_mx_12, R.raw.sp_mx_12);
        Number n13 = new Number("13", "trece", R.raw.sp_mx_13, R.raw.sp_mx_13);
        Number n14 = new Number("14", "catroce", R.raw.sp_mx_14, R.raw.sp_mx_14);
        Number n15 = new Number("15", "quince", R.raw.sp_mx_15, R.raw.sp_mx_15);
        Number n16 = new Number("16", "diez y seis\ndieciséis", R.raw.sp_mx_16, R.raw.sp_mx_16);
        Number n17 = new Number("17", "diez y siete\ndiecisiete", R.raw.sp_mx_17, R.raw.sp_mx_17);
        Number n18 = new Number("18", "diez y ocho\ndieciocho", R.raw.sp_mx_18, R.raw.sp_mx_18);
        Number n19 = new Number("19", "diez y nueve\ndiecinueve", R.raw.sp_mx_19, R.raw.sp_mx_19);
        Number n20 = new Number("20", "veinte\n", R.raw.sp_mx_20, R.raw.sp_mx_20);
        numbers.add(n11);
        numbers.add(n12);
        numbers.add(n13);
        numbers.add(n14);
        numbers.add(n15);
        numbers.add(n16);
        numbers.add(n17);
        numbers.add(n18);
        numbers.add(n19);
        numbers.add(n20);

        //21-30
        /*21- veintiuno
        * 22 - veintidós
        * 23 - veintitrés
        * 24 - veinticuatro
        * 25 - veinticinco
        * 26 - veintiséis
        * 27 - veintisiete
        * 28 - veintiocho
        * 29 - veintinueve
        * 30 - treinta*/
        Number n21 = new Number("21", "veintiuno\n", R.raw.sp_mx_21, R.raw.sp_mx_21);
        Number n22 = new Number("22", "veintidós", R.raw.sp_mx_22, R.raw.sp_mx_22);
        Number n23 = new Number("23", "veintitrés", R.raw.sp_mx_23, R.raw.sp_mx_23);
        Number n24 = new Number("24", "veinticuatro", R.raw.sp_mx_24, R.raw.sp_mx_24);
        Number n25 = new Number("25", "veinticinco", R.raw.sp_mx_25, R.raw.sp_mx_25);
        Number n26 = new Number("26", "veintiséis", R.raw.sp_mx_26, R.raw.sp_mx_26);
        Number n27 = new Number("27", "veintisiete", R.raw.sp_mx_27, R.raw.sp_mx_27);
        Number n28 = new Number("28", "veintiocho", R.raw.sp_mx_28, R.raw.sp_mx_28);
        Number n29 = new Number("29", "veintinueve", R.raw.sp_mx_29, R.raw.sp_mx_29);
        Number n30 = new Number("30", "treinta", R.raw.sp_mx_30, R.raw.sp_mx_30);
        numbers.add(n21);
        numbers.add(n22);
        numbers.add(n23);
        numbers.add(n24);
        numbers.add(n25);
        numbers.add(n26);
        numbers.add(n27);
        numbers.add(n28);
        numbers.add(n29);
        numbers.add(n30);

        return numbers;
    }

    public static ArrayList<Number> get40to100() {
        ArrayList<Number> numbers = new ArrayList<>();
        //40-100
        /*40 - cuarenta
        * 50 - cincuenta
        * 60 - sesenta
        * 70 - setenta
        * 80 - ochenta
        * 90 - noventa
        * 100- cien*/
        Number n40 = new Number("40", "cuarenta", R.raw.sp_40, R.raw.mx_40);
        Number n50 = new Number("50", "cincuenta", R.raw.sp_50, R.raw.mx_50);
        Number n60 = new Number("60", "sesenta", R.raw.sp_60, R.raw.mx_60);
        Number n70 = new Number("70", "setenta", R.raw.sp_70, R.raw.mx_70);
        Number n80 = new Number("80", "ochenta", R.raw.sp_80, R.raw.mx_80);
        Number n90 = new Number("90", "noventa", R.raw.sp_90, R.raw.mx_90);
        Number n100 = new Number("100", "cien", R.raw.sp_100, R.raw.mx_100);
        numbers.add(n40);
        numbers.add(n50);
        numbers.add(n60);
        numbers.add(n70);
        numbers.add(n80);
        numbers.add(n90);
        numbers.add(n100);

        return numbers;
    }

    public static ArrayList<Number> get1hto1k() {
        ArrayList<Number> numbers = new ArrayList<>();
        //101-1000
        /*101 - ciento uno/ciento un
        * 202 - doscientos/as dos
        * 303 - trescientos/as tres
        * 404 - cuatrocientos/as cuarto
        * 505 - quiniento/as cinco
        * 606 - quinientos/as cinco
        * 707 - setecientos/as siete/tas
        * 808 - ochocientos/as ocho/tas
        * 909 - novecientos/as nueve/tas
        * 1000- mil*/
        Number n101 = new Number("101", "ciento uno | ciento un" , R.raw.sp_101, R.raw.mx_101); // THESE ARE USING MX DIALECT FILES FOR SPANISH
        Number n202 = new Number("202", "doscientos/as dos" , R.raw.sp_202, R.raw.mx_202); // THESE ARE USING MX DIALECT FILES FOR SPANISH
        Number n303 = new Number("303", "trescientos/as tres" , R.raw.sp_303, R.raw.mx_303); // THESE ARE USING MX DIALECT FILES FOR SPANISH
        Number n404 = new Number("404", "cuatrocientos/as cuarto" , R.raw.sp_404, R.raw.mx_404);
        Number n505 = new Number("505", "quiniento/as cinco" , R.raw.sp_505, R.raw.mx_505);
        Number n606 = new Number("606", "seiscientos/as seis" , R.raw.sp_606, R.raw.mx_606);
        Number n707 = new Number("707", "setecientos/as siete" , R.raw.sp_707, R.raw.mx_707);
        Number n808 = new Number("808", "ochocientos/as ocho" , R.raw.sp_808, R.raw.mx_808);
        Number n909 = new Number("909", "novecientos/as nueve" , R.raw.sp_909, R.raw.mx_909);
        Number n1000 = new Number("1000", "mil", R.raw.sp_1000, R.raw.mx_1000);
        numbers.add(n101);
        numbers.add(n202);
        numbers.add(n303);
        numbers.add(n404);
        numbers.add(n505);
        numbers.add(n606);
        numbers.add(n707);
        numbers.add(n808);
        numbers.add(n909);
        numbers.add(n1000);

        return numbers;
    }

    public static ArrayList<Number> get1000s()  {
        ArrayList<Number> numbers = new ArrayList<>();

        //1000-10000
        /*1000 - mil
        * 2002 - dos mil dos
        * 3333 - tres mil trescientos/as treinta y tres
        * 4404 - cuatro mil cuatrocientos/as cuatro
        * 5055 - cinco mil cincuenta y cinco
        * 6666 - seis mil seiscientos/as sesenta y seis
        * 7707 - siete mil setecientos/as siete
        * 8088 - ocho mil ochenta y ocho
        * 9999 - nueve mil novescientos/as noventa y nueve
        * 10,000- diez mil*/
        Number n2002 = new Number("2002", "dos mil dos" , R.raw.sp_2002, R.raw.mx_2002);
        Number n3333 = new Number("3333", "tres mil trescientos/as treinta y tres" , R.raw.sp_3333, R.raw.mx_3333);
        Number n4404 = new Number("4404", "cuatro mil cuatrocientos/as cuatro" , R.raw.sp_4404, R.raw.mx_4404);
        Number n5055 = new Number("5055", "cinco mil cincuenta y cinco" , R.raw.sp_5055, R.raw.mx_5055);
        Number n6666 = new Number("6666", "seis mil seiscientos/as sesenta y seis" , R.raw.sp_6666, R.raw.mx_6666);
        Number n7707 = new Number("7707", "siete mil setecientos/as siete" , R.raw.sp_7707, R.raw.mx_7707);
        Number n8088 = new Number("8088", "ocho mil ochenta y ocho" , R.raw.sp_8088, R.raw.mx_8088);
        Number n9999 = new Number("9999", "nueve mil novescientos/as noventa y nueve" , R.raw.sp_9999, R.raw.mx_9999);
        Number n10000 = new Number("10000", "diez mil", R.raw.sp_10000, R.raw.mx_10000);
        numbers.add(n2002);
        numbers.add(n3333);
        numbers.add(n4404);
        numbers.add(n5055);
        numbers.add(n6666);
        numbers.add(n7707);
        numbers.add(n8088);
        numbers.add(n9999);
        numbers.add(n10000);

        return numbers;
    }

    public static ArrayList<Number> get10kto986k() {
        ArrayList<Number> numbers = new ArrayList<>();

        //10k-986k
        /*11,000 - once mil
        * 15,000 - quince mil
        * 18,000 - dieciocho mil
        * 22,000 - veintidós mil
        * 28,000 - veintiocho mil
        * 37,000 - treinta y siete mil
        * 85,000 - ochenta y cinco mil
        * 100,000 - cien mil
        * 108,000 - ciento ocho mil
        * 160,000 - ciento sesenta mil
        * 575,000 - quinientos/as setenta y cinco mil
        * 986,000 - novecientos/as ochenta y seis mil*/
        Number n11000 = new Number("11,000", "once mil" , R.raw.sp_11k, R.raw.mx_11k);
        Number n15000 = new Number("15,000", "quince mil" , R.raw.sp_15k, R.raw.mx_15k);
        Number n18000 = new Number("18,000", "dieciocho mil" , R.raw.sp_18k, R.raw.mx_18k);
        Number n22000 = new Number("22,000", "veintidós mil" , R.raw.sp_22k, R.raw.mx_22k);
        Number n28000 = new Number("28,000", "veintiocho mil" , R.raw.sp_28k, R.raw.mx_28k);
        Number n37000 = new Number("37,000", "treinta y siete mil" , R.raw.sp_37k, R.raw.mx_37k);
        Number n85000 = new Number("85,000", "ochenta y cinco mil" , R.raw.sp_85k, R.raw.mx_85k);
        Number n100000 = new Number("100,000", "cien mil" , R.raw.sp_100k, R.raw.mx_100k);
        Number n108000 = new Number("108,000", "ciento ocho mil", R.raw.sp_108k, R.raw.mx_108k);
        Number n160000 = new Number("160,000", "ciento sesenta mil" , R.raw.sp_160k, R.raw.mx_160k);
        Number n575000 = new Number("575,000", "quinientos/as setenta y cinco mil", R.raw.sp_575k, R.raw.mx_575k);
        Number n986000 = new Number("986,000", "novecientos/as ochenta y seis mil" , R.raw.sp_986k, R.raw.mx_986k);
        numbers.add(n11000);
        numbers.add(n15000);
        numbers.add(n18000);
        numbers.add(n22000);
        numbers.add(n28000);
        numbers.add(n37000);
        numbers.add(n85000);
        numbers.add(n100000);
        numbers.add(n108000);
        numbers.add(n160000);
        numbers.add(n575000);
        numbers.add(n986000);

        return numbers;
    }

    public static ArrayList<Number> get1mto1c() {
        ArrayList<Number> numbers = new ArrayList<>();

        //1m-1c
        /*1,000,000 - un millón
        * 2,000,000 - dos milliones
        * 500,000,000 - quinientos milliones
        * 1,000,000,000 - mil milliones
        * 1,200,000,000 - mil doscientos milliones
        * 5,000,000,000 - cinco mil milliones
        * 1,000,000,000,000 - un billón
        * 1,000,000,000,000,000 - mil billiones
        * 1,000,000,000,000,000,000 - un trillón
        * 1,000,000,000,000,000,000,000 - un cuatrillón*/
        Number n1m = new Number("1,000,000", "un millón", R.raw.sp_1m, R.raw.mx_1m);
        Number n2m = new Number("2,000,000", "dos milliones", R.raw.sp_2m, R.raw.mx_2m);
        Number n500m  = new Number("500,000,000", "quinientos milliones", R.raw.sp_500m, R.raw.mx_500m);
        Number n1000m = new Number("1,000,000,000", "mil milliones", R.raw.sp_1000m, R.raw.mx_1000m);
        Number n1200m = new Number("1,200,000,000", "mil doscientos milliones", R.raw.sp_1200m, R.raw.mx_1200m);
        Number n5000m = new Number("5,000,000,000", "cinco mil milliones", R.raw.sp_5000m, R.raw.mx_5000m);
        Number n1b    = new Number("1,000,000,000,000", "un billón", R.raw.sp_1b, R.raw.mx_1b);
        Number n1000b = new Number("1,000,000,000,000,000", "mil billiones", R.raw.sp_1000b, R.raw.mx_1000b);
        Number n1t    = new Number("1,000,000,000,000,000,000", "un trillón", R.raw.sp_1t, R.raw.mx_1t);
        Number n1c    = new Number("1,000,000,000,000,000,000,000", "un cuatrillón", R.raw.sp_1c, R.raw.mx_1c);
        numbers.add(n1m);
        numbers.add(n2m);
        numbers.add(n500m );
        numbers.add(n1000m);
        numbers.add(n1200m);
        numbers.add(n5000m);
        numbers.add(n1b);
        numbers.add(n1000b);
        numbers.add(n1t);
        numbers.add(n1c);

        return numbers;
    }

    public static ArrayList<Number> getOrdinalNos() {
        ArrayList<Number> numbers = new ArrayList<>();

        //Ordinal
        /*1.o (1.er) primero (primer) | 1.a primera
        * 2.o segundo | 2.a segunda
        * 3.o (3.er) tercero (tercer) | 3.a tercera
        * 4.o cuarto | 4.a cuarta
        * 5.o quinto | 5.a quinta
        * 6.o sexto | 6.a sexta
        * 7.o séptimo | 7.a séptima
        * 8.o octavo | 8.a octava
        * 9.o noveno | 9.a novena
        * 10.o décimo | 10.a décima*/
        Number primero = new Number("1.ᵒ (1.er) | 1.ᵃ", "primero (primer) | primera", R.raw.sp_01_o , R.raw.mx_01_o);
        Number segundo = new Number("2.ᵒ | 2.ᵃ", "segundo | segunda", R.raw.sp_02_o , R.raw.mx_02_o);
        Number tercero = new Number("3.ᵒ (3.er) | 3.ᵃ", "tercero (tercer) | tercera", R.raw.sp_03_o , R.raw.mx_03_o);
        Number cuarto  = new Number("4.ᵒ | 4.ᵃ", "cuarto | cuarta", R.raw.sp_04_o , R.raw.mx_04_o);
        Number quinto  = new Number("5.ᵒ | 5.ᵃ", "quinto | quinta", R.raw.sp_05_o , R.raw.mx_05_o);
        Number sexto   = new Number("6.ᵒ | 6.ᵃ", "sexto | sexta", R.raw.sp_06_o , R.raw.mx_06_o);
        Number séptimo = new Number("7.ᵒ | 7.ᵃ", "séptimo | séptima", R.raw.sp_07_o , R.raw.mx_07_o);
        Number octavo = new Number("8.ᵒ | 8.ᵃ", "octavo | octava", R.raw.sp_08_o , R.raw.mx_08_o);
        Number noveno = new Number("9.ᵒ | 9.ᵃ", "noveno | novena", R.raw.sp_09_o , R.raw.mx_09_o);
        Number decimo = new Number("10.ᵒ | 10.ᵃ", "décimo | décima", R.raw.sp_10_o , R.raw.mx_10_o);
        numbers.add(primero);
        numbers.add(segundo);
        numbers.add(tercero);
        numbers.add(cuarto);
        numbers.add(quinto);
        numbers.add(sexto);
        numbers.add(séptimo);
        numbers.add(octavo);
        numbers.add(noveno);
        numbers.add(decimo);

        return numbers;
    }

    /**
     * This method makes the letter objects for the letters ArrayList that is then
     * used to construct the interactive elements*/
    public static ArrayList<Letter> getLetters(){
        ArrayList<Letter> letters = new ArrayList<>();

        Letter letter_a =  new Letter("a", "a", R.raw.a_sp, R.raw.a_mx);
        Letter letter_b =  new Letter("b", "b", R.raw.b_sp, R.raw.b_mx);
        Letter letter_c =  new Letter("c", "ce", R.raw.c_sp, R.raw.c_mx);
        Letter letter_ch =  new Letter("ch", "che", R.raw.ch_sp, R.raw.ch_mx);
        Letter letter_d =  new Letter("d", "de", R.raw.d_sp, R.raw.d_mx);
        Letter letter_e =  new Letter("e", "e", R.raw.e_sp, R.raw.e_mx);
        Letter letter_f =  new Letter("f", "efe", R.raw.f_sp, R.raw.f_mx);
        Letter letter_g =  new Letter("g", "ge", R.raw.g_sp, R.raw.g_mx);
        Letter letter_h =  new Letter("h", "hache", R.raw.h_sp, R.raw.h_mx);
        Letter letter_i =  new Letter("i", "i", R.raw.i_sp, R.raw.i_mx);
        Letter letter_j =  new Letter("j", "jota", R.raw.j_sp, R.raw.j_mx);
        Letter letter_k =  new Letter("k", "ka", R.raw.k_sp, R.raw.k_mx);
        Letter letter_l =  new Letter("l", "ele", R.raw.l_sp, R.raw.l_mx);
        Letter letter_ll =  new Letter("ll", "elle", R.raw.ll_sp, R.raw.ll_mx);
        Letter letter_m =  new Letter("m", "eme", R.raw.m_sp, R.raw.m_mx);
        Letter letter_n =  new Letter("n", "ene", R.raw.n_sp, R.raw.n_mx);
        Letter letter_ñ =  new Letter("ñ", "eñe", R.raw.n2_sp, R.raw.n2_mx);
        Letter letter_o =  new Letter("o", "o", R.raw.o_sp, R.raw.o_mx);
        Letter letter_p =  new Letter("p", "pe", R.raw.p_sp, R.raw.p_mx);
        Letter letter_q =  new Letter("q", "cu", R.raw.q_sp, R.raw.q_mx);
        Letter letter_r =  new Letter("r", "erre", R.raw.r_sp, R.raw.r_mx);
        Letter letter_s =  new Letter("s", "ese", R.raw.s_sp, R.raw.s_mx);
        Letter letter_t =  new Letter("t", "te", R.raw.t_sp, R.raw.t_mx);
        Letter letter_u =  new Letter("u", "u", R.raw.u_sp, R.raw.u_mx);
        Letter letter_v =  new Letter("v", "uve", R.raw.v_sp, R.raw.v_mx);
        Letter letter_w =  new Letter("w", "uve doble", R.raw.w_sp, R.raw.w_mx);
        Letter letter_x =  new Letter("x", "equis", R.raw.x_sp, R.raw.x_mx);
        Letter letter_y =  new Letter("y", "igriega", R.raw.y_sp, R.raw.y_mx);
        Letter letter_z =  new Letter("z", "zeta", R.raw.z_sp, R.raw.z_mx);

        letters.add(letter_a);
        letters.add(letter_b);
        letters.add(letter_c);
        letters.add(letter_ch);
        letters.add(letter_d);
        letters.add(letter_e);
        letters.add(letter_f);
        letters.add(letter_g);
        letters.add(letter_h);
        letters.add(letter_i);
        letters.add(letter_j);
        letters.add(letter_k);
        letters.add(letter_l);
        letters.add(letter_ll);
        letters.add(letter_m);
        letters.add(letter_n);
        letters.add(letter_ñ);
        letters.add(letter_o);
        letters.add(letter_p);
        letters.add(letter_q);
        letters.add(letter_r);
        letters.add(letter_s);
        letters.add(letter_t);
        letters.add(letter_u);
        letters.add(letter_v);
        letters.add(letter_w);
        letters.add(letter_x);
        letters.add(letter_y);
        letters.add(letter_z);

        return letters;
    }

    public static ArrayList<Number> getAllNumbers() {
        ArrayList<Number> numbers = new ArrayList<>();
        numbers.addAll(get0to40());
        numbers.addAll(get40to100());
        numbers.addAll(get1hto1k());
        numbers.addAll(get1000s());
        numbers.addAll(get10kto986k());
        numbers.addAll(get1mto1c());
        numbers.addAll(getOrdinalNos());

        return numbers;
    }

    public static ArrayList<Season.Month> getAllMonths(Context context) {
        ArrayList<Season.Month> months = new ArrayList<>();
        ArrayList<Season> seasons = getSeasonsAndMonths(context);
        for(Season season: seasons) {
            months.addAll(season.getMonths());
        }

        return months;
    }
}
