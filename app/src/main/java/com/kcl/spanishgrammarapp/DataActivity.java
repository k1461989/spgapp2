package com.kcl.spanishgrammarapp;

import java.util.ArrayList;
import java.util.Collections;

/**
 * This class holds all the Exercise/Question data and constructs the corresponding objects that
 * are then later used in ExerciseActivity.
 */
class DataActivity {

    private ArrayList<Exercise> allExercises = new ArrayList<>();

    DataActivity(){

        //adding Greetings exercises
        Exercise greetingsPronombres = new Exercise("Greetings/Pronombres");
        allExercises.add(greetingsPronombres);
        greetingsPronombres.addQuestion(new Question("mc","_________ te llamas","tú",
                arrayStringToList(new String[]{"nosotros","vosotros","ellos/ellas/ustedes","yo","él/ella/usted","tú"})));
        greetingsPronombres.addQuestion(new Question("mc","_________ se llaman","ellos/ellas/ustedes",
                arrayStringToList(new String[]{"tú","nosotros","él/ella/usted","vosotros","yo","ellos/ellas/ustedes"})));
        greetingsPronombres.addQuestion(new Question("mc","_____ os llamáis","vosotros",
                arrayStringToList(new String[]{"yo","nosotros","ellos/ellas/ustedes","tú","él/ella/usted","vosotros"})));
        greetingsPronombres.addQuestion(new Question("mc","________ me llamo","yo",
                arrayStringToList(new String[]{"vosotros","nosotros","ellos/ellas/ustedes","tú","él/ella/usted","yo"})));
        greetingsPronombres.addQuestion(new Question("mc","______ se llama","él/ella/usted",
                arrayStringToList(new String[]{"vosotros","nosotros","ellos/ellas/ustedes","tú","yo","él/ella/usted"})));
        greetingsPronombres.addQuestion(new Question("mc","________ nos llamamos","nosotros",
                arrayStringToList(new String[]{"vosotros","él/ella/usted","ellos/ellas/ustedes","tú","yo","nosotros"})));

        Exercise greetingsSerYEstar = new Exercise("Greetings/Ser y Estar");
        allExercises.add(greetingsSerYEstar);
        greetingsSerYEstar.addQuestion(new Question("mc","Ella ______ mi amiga Gema", "es",
                arrayStringToList(new String[]{"está","soy","eres","es"})));
        greetingsSerYEstar.addQuestion(new Question("mc","¿Dónde _________ Pedro?","está",
                arrayStringToList(new String[]{"estás","es","eres","está"})));
        greetingsSerYEstar.addQuestion(new Question("mc","Yo _________ Cecilia","soy",
                arrayStringToList(new String[]{"estás","estoy","eres","soy"})));
        greetingsSerYEstar.addQuestion(new Question("mc","Él _________ Miguel","es",
                arrayStringToList(new String[]{"está","son","eres","es"})));
        greetingsSerYEstar.addQuestion(new Question("mc","Nosotras ______ Marta y Julia","somos",
                arrayStringToList(new String[]{"estamos","son","eres","somos"})));

        Exercise greetingsLlamarse = new Exercise("Greetings/Llamarse");
        allExercises.add(greetingsLlamarse);
        greetingsLlamarse.addQuestion(new Question("mc","Tú","te llamas",
                arrayStringToList(new String[]{"me llamo","se llama","nos llamamos","te llamas"})));
        greetingsLlamarse.addQuestion(new Question("mc","Él/ella/usted","se llama",
                arrayStringToList(new String[]{"me llamo","te llamas","os llamáis","se llama"})));
        greetingsLlamarse.addQuestion(new Question("mc","Ellos/ellas ustedes","se llaman",
                arrayStringToList(new String[]{"me llamo","se llama","os llamáis","se llaman"})));
        greetingsLlamarse.addQuestion(new Question("mc","Vosotros","os llamáis",
                arrayStringToList(new String[]{"me llamo","se llama","te llamas","os llamáis"})));
        greetingsLlamarse.addQuestion(new Question("mc","Nosotros","nos llamamos",
                arrayStringToList(new String[]{"me llamo","te llamas","os llamáis","nos llamamos"})));
        greetingsLlamarse.addQuestion(new Question("mc","Yo","me llamo",
                arrayStringToList(new String[]{"te llamas","se llama","os llamáis","me llamo"})));

        Exercise greetingsLasLetras = new Exercise("Greetings/Las Letras");
        allExercises.add(greetingsLasLetras);
        greetingsLasLetras.addQuestion(new Question("au", "u01e06_1","Julio",
                arrayStringToList(new String[]{"Junio","Julia","Julio"})));
        greetingsLasLetras.addQuestion(new Question("au", "u01e06_2","Lucy",
                arrayStringToList(new String[]{"Lucy","Luca","Luz"})));
        greetingsLasLetras.addQuestion(new Question("au", "u01e06_3","Gema",
                arrayStringToList(new String[]{"Gema","Henna","Goma"})));
        greetingsLasLetras.addQuestion(new Question("au", "u01e06_4","Yolanda",
                arrayStringToList(new String[]{"Holanda","Fernanda","Yolanda"})));
        greetingsLasLetras.addQuestion(new Question("au", "u01e06_5","Juan",
                arrayStringToList(new String[]{"Juan","Juana","Iguana"})));
        greetingsLasLetras.addQuestion(new Question("au", "u01e06_6","Maga",
                arrayStringToList(new String[]{"Majo","Maja","Maga"})));
        greetingsLasLetras.addQuestion(new Question("au", "u01e06_7","Greta",
                arrayStringToList(new String[]{"Grito","Greta","Grata"})));
        greetingsLasLetras.addQuestion(new Question("au", "u01e06_8","Jerónimo",
                arrayStringToList(new String[]{"Gerónimo","Jerónimo","Geranio"})));
        greetingsLasLetras.addQuestion(new Question("au", "u01e06_9","Miguel",
                arrayStringToList(new String[]{"Miguel","Micaela","Malena"})));
        greetingsLasLetras.addQuestion(new Question("au", "u01e06_10","Camila",
                arrayStringToList(new String[]{"Camila","Camino","Camilo"})));

        Exercise greetingsVocabulario = new Exercise("Greetings/Vocabulario");
        allExercises.add(greetingsVocabulario);
        greetingsVocabulario.addQuestion(new Question("dd","tren","u1_train",
                arrayStringToList(new String[]{"u1_plane","u1_train","u1_car",
                "u1_ship","u1_bus","u1_bike"})));
        greetingsVocabulario.addQuestion(new Question("dd","avión","u1_plane",
                arrayStringToList(new String[]{"u1_plane","u1_train","u1_car",
                        "u1_ship","u1_bus","u1_bike"})));
        greetingsVocabulario.addQuestion(new Question("dd","coche","u1_car",
                arrayStringToList(new String[]{"u1_plane","u1_train","u1_car",
                        "u1_ship","u1_bus","u1_bike"})));
        greetingsVocabulario.addQuestion(new Question("dd","autobús","u1_bus",
                arrayStringToList(new String[]{"u1_plane","u1_train","u1_car",
                        "u1_ship","u1_bus","u1_bike"})));
        greetingsVocabulario.addQuestion(new Question("dd","barco","u1_ship",
                arrayStringToList(new String[]{"u1_plane","u1_train","u1_car",
                        "u1_ship","u1_bus","u1_bike"})));
        greetingsVocabulario.addQuestion(new Question("dd","bicicleta","u1_bike",
                arrayStringToList(new String[]{"u1_plane","u1_train","u1_car",
                        "u1_ship","u1_bus","u1_bike"})));


        //adding Checking-in exercises
        Exercise checkingInAI = new Exercise("Checking-in/Artículo Indeterminado");
        allExercises.add(checkingInAI);
        checkingInAI.addQuestion(new Question("mc","______ escuela","una",
                arrayStringToList(new String[]{"un","una"})));
        checkingInAI.addQuestion(new Question("mc","______ bar","un",
                arrayStringToList(new String[]{"un","una"})));
        checkingInAI.addQuestion(new Question("mc","______ supermercado","un",
                arrayStringToList(new String[]{"un","una"})));
        checkingInAI.addQuestion(new Question("mc","______ carnicería","una",
                arrayStringToList(new String[]{"un","una"})));
        checkingInAI.addQuestion(new Question("mc","______ florería","una",
                arrayStringToList(new String[]{"un","una"})));
        checkingInAI.addQuestion(new Question("mc","______ panadería","una",
                arrayStringToList(new String[]{"un","una"})));
        checkingInAI.addQuestion(new Question("mc","______ universidad","una",
                arrayStringToList(new String[]{"un","una"})));
        checkingInAI.addQuestion(new Question("mc","______ banco","un",
                arrayStringToList(new String[]{"un","una"})));
        checkingInAI.addQuestion(new Question("mc","______ tienda","una",
                arrayStringToList(new String[]{"un","una"})));
        checkingInAI.addQuestion(new Question("mc","______ restaurante","un",
                arrayStringToList(new String[]{"un","una"})));

        Exercise checkingInAD = new Exercise("Checking-in/Artículo Determinado");
        allExercises.add(checkingInAD);
        checkingInAD.addQuestion(new Question("mc","______ escuela","la",
                arrayStringToList(new String[]{"el","la"})));
        checkingInAD.addQuestion(new Question("mc","______ bar","el",
                arrayStringToList(new String[]{"el","la"})));
        checkingInAD.addQuestion(new Question("mc","______ supermercado","el",
                arrayStringToList(new String[]{"el","la"})));
        checkingInAD.addQuestion(new Question("mc","______ hotel","el",
                arrayStringToList(new String[]{"el","la"})));
        checkingInAD.addQuestion(new Question("mc","______ canicería","la",
                arrayStringToList(new String[]{"el","la"})));
        checkingInAD.addQuestion(new Question("mc","______ florería","la",
                arrayStringToList(new String[]{"el","la"})));
        checkingInAD.addQuestion(new Question("mc","______ panadería","la",
                arrayStringToList(new String[]{"el","la"})));
        checkingInAD.addQuestion(new Question("mc","______ universidad","la",
                arrayStringToList(new String[]{"el","la"})));
        checkingInAD.addQuestion(new Question("mc","______ banco","el",
                arrayStringToList(new String[]{"el","la"})));
        checkingInAD.addQuestion(new Question("mc","______ tienda","la",
                arrayStringToList(new String[]{"el","la"})));


        Exercise checkingInNumeros = new Exercise("Checking-in/Números");
        allExercises.add(checkingInNumeros);
        checkingInNumeros.addQuestion(new Question("au","u02e03_1-La habitación de Peter es la número","26",
                arrayStringToList(new String[]{"27","26","25","20"})));
        checkingInNumeros.addQuestion(new Question("au","u02e03_2-El código postal el N ______ 5RB","29",
                arrayStringToList(new String[]{"29","19","9","1"})));
        checkingInNumeros.addQuestion(new Question("au","u02e03_3-Juana tiene _____ años","14",
                arrayStringToList(new String[]{"12","13","14","15"})));
        checkingInNumeros.addQuestion(new Question("au","u02e03_4-Hoy es es el _____ de diciembre","25",
                arrayStringToList(new String[]{"5","7","25","27"})));
        checkingInNumeros.addQuestion(new Question("au","u02e03_5-Lucas tiene _____ años","16",
                arrayStringToList(new String[]{"25","15","17","16"})));
        checkingInNumeros.addQuestion(new Question("au","u02e03_6-El ńumero de mi casa es el ____","22",
                arrayStringToList(new String[]{"12","11","22","16"})));


        Exercise checkingingInNac = new Exercise("Checking-in/Nacionalidades");
        allExercises.add(checkingingInNac);
        checkingingInNac.addQuestion(new Question("mc","Marruecos","marroquí",
                arrayStringToList(new String[]{"argentino/argentina","brasileño/brasileña","marroquí","canadiense"})));
        checkingingInNac.addQuestion(new Question("mc","Brasil","brasileño/brasileña",
                arrayStringToList(new String[]{"argentino/argentina","brasileño/brasileña","marroquí","canadiense"})));
        checkingingInNac.addQuestion(new Question("mc","Canadá","canadiense",
                arrayStringToList(new String[]{"argentino/argentina","brasileño/brasileña","marroquí","canadiense"})));
        checkingingInNac.addQuestion(new Question("mc","Argentina","argentino/argentina",
                arrayStringToList(new String[]{"argentino/argentina","brasileño/brasileña","marroquí","canadiense"})));
        checkingingInNac.addQuestion(new Question("mc","México","mexicano/mexicana",
                arrayStringToList(new String[]{"mexicano/mexicana","español/española","escocés/escocesa","irlandés/irlandesa"})));
        checkingingInNac.addQuestion(new Question("mc","Escocia","escocés/escocesa",
                arrayStringToList(new String[]{"mexicano/mexicana","español/española","escocés/escocesa","irlandés/irlandesa"})));
        checkingingInNac.addQuestion(new Question("mc","España","español/española",
                arrayStringToList(new String[]{"mexicano/mexicana","español/española","escocés/escocesa","irlandés/irlandesa"})));
        checkingingInNac.addQuestion(new Question("mc","Irlanda","irlandés/irlandesa",
                arrayStringToList(new String[]{"mexicano/mexicana","español/española","escocés/escocesa","irlandés/irlandesa"})));

        Exercise checkingingVocabulario = new Exercise("Checking-in/Vocabulario");
        allExercises.add(checkingingVocabulario);
        checkingingVocabulario.addQuestion(new Question("dd","tarjeta de crédito","u2_credit_card",
                arrayStringToList(new String[]{"u2_credit_card","u2_room","u2_suitcases","u2_bar","u2_phone","u2_hotel"})));
        checkingingVocabulario.addQuestion(new Question("dd","dirección","u2_direction",
                arrayStringToList(new String[]{"u2_receptionist","u2_room","u2_passport","u2_bar","u2_direction","u2_reservation"})));
        checkingingVocabulario.addQuestion(new Question("dd","reserva","u2_reservation",
                arrayStringToList(new String[]{"u2_reservation","u2_room","u2_suitcases","u2_bar","u2_phone","u2_hotel"})));
        checkingingVocabulario.addQuestion(new Question("dd","bar","u2_bar",
                arrayStringToList(new String[]{"u2_credit_card","u2_receptionist","u2_suitcases","u2_bar","u2_phone","u2_hotel"})));
        checkingingVocabulario.addQuestion(new Question("dd","teléfono","u2_phone",
                arrayStringToList(new String[]{"u2_reservation","u2_room","u2_suitcases","u2_bar","u2_phone","u2_hotel"})));
        checkingingVocabulario.addQuestion(new Question("dd","recepcionista","u2_receptionist",
                arrayStringToList(new String[]{"u2_credit_card","u2_room","u2_suitcases","u2_bar","u2_receptionist","u2_direction"})));
        checkingingVocabulario.addQuestion(new Question("dd","habitación","u2_room",
                arrayStringToList(new String[]{"u2_credit_card","u2_room","u2_passport","u2_reservation","u2_receptionist","u2_phone"})));
        checkingingVocabulario.addQuestion(new Question("dd","pasaporte","u2_passport",
                arrayStringToList(new String[]{"u2_credit_card","u2_room","u2_passport","u2_bar","u2_phone","u2_reservation"})));
        checkingingVocabulario.addQuestion(new Question("dd","hotel","u2_hotel",
                arrayStringToList(new String[]{"u2_credit_card","u2_room","u2_hotel","u2_reservation","u2_receptionist","u2_phone"})));
        checkingingVocabulario.addQuestion(new Question("dd","maletas","u2_suitcases",
                arrayStringToList(new String[]{"u2_credit_card","u2_room","u2_suitcases","u2_reservation","u2_hotel","u2_phone"})));


        //adding Sightseeing exercises
        Exercise sightseePre = new Exercise("Sightseeing/Preposiciones");
        allExercises.add(sightseePre);
        sightseePre.addQuestion(new Question("mc","Estoy _____ mis amigos en Madrid", "con",
                arrayStringToList(new String[]{"a","en","de","con"})));
        sightseePre.addQuestion(new Question("mc","Hay un libro _____ toda la información", "con",
                arrayStringToList(new String[]{"a","en","de","con"})));
        sightseePre.addQuestion(new Question("mc","Mi número _____ teléfono es el...", "de",
                arrayStringToList(new String[]{"a","en","de","con"})));
        sightseePre.addQuestion(new Question("mc","Marta y Julia estudian _____ la facultad de arquitectura", "en",
                arrayStringToList(new String[]{"a","en","de","con"})));
        sightseePre.addQuestion(new Question("mc","Soy estudiante _____ medicina", "de",
                arrayStringToList(new String[]{"a","en","de","con"})));
        sightseePre.addQuestion(new Question("mc","Trabajo _____ un restaurante", "en",
                arrayStringToList(new String[]{"a","en","de","con"})));
        sightseePre.addQuestion(new Question("mc","El parque abre _____ 10:00 a 11:00", "de",
                arrayStringToList(new String[]{"a","en","de","con"})));
        sightseePre.addQuestion(new Question("mc","Madrid está _____ el centro de España", "en",
                arrayStringToList(new String[]{"a","en","de","con"})));
        sightseePre.addQuestion(new Question("mc","Soy _____ Buenos Aires", "de",
                arrayStringToList(new String[]{"a","en","de","con"})));
        sightseePre.addQuestion(new Question("mc","¿ _____ qué hora abren los bancos?", "a",
                arrayStringToList(new String[]{"a","en","de","con"})));

        Exercise sightseeHay = new Exercise("Sightseeing/Hay que y Tener que");
        allExercises.add(sightseeHay);
        sightseeHay.addQuestion(new Question("mc", "The park is a long way from here. You have to take a bus."+"\n"+
                "El parque está lejos de aquí, ______ que tomar un autobús.","tienes",
                arrayStringToList(new String[]{"tienes","hay","tiene"})));
        sightseeHay.addQuestion(new Question("mc", "To learn Spanish, one must study and practise."+"\n"+
                "Para aprender español ______ que estudiar y practicar.","hay",
                arrayStringToList(new String[]{"tienes","hay","tiene"})));
        sightseeHay.addQuestion(new Question("mc", "Joseph is in Perú, he has to speak Spanish."+"\n"+
                "Joseph está en Perú, él ______ que hablar en español.","tiene",
                arrayStringToList(new String[]{"tienes","hay","tiene"})));
        sightseeHay.addQuestion(new Question("mc", "María has an exam tomorrow. She has to study a lot."+"\n"+
                "María tiene un examen mañana. Ella ______ que estudiar mucho.","tiene",
                arrayStringToList(new String[]{"tiene","hay","tengo"})));
        sightseeHay.addQuestion(new Question("mc", "If you want to pass your exams, you have to revise a lot."+"\n"+
                "Si quieres pasar los exámenes, tú ______ que repasar mucho.","tienes",
                arrayStringToList(new String[]{"tienes","hay","tengo"})));
        sightseeHay.addQuestion(new Question("mc", "To get a visa one has to fill a form."+"\n"+
                "Para obtener un visado ______ que llenar un formulario.","hay",
                arrayStringToList(new String[]{"tienes","hay","tenemos"})));

        Exercise sightseeVerbo = new Exercise("Sightseeing/Verbo Estar");
        allExercises.add(sightseeVerbo);
        sightseeVerbo.addQuestion(new Question("mc","Madrid ______ en el centro de España","está",
                arrayStringToList(new String[]{"estoy","estás","está","estamos","estáis","están"})));
        sightseeVerbo.addQuestion(new Question("mc","Yo ______ en un hotel de cuatro estrellas","estoy",
                arrayStringToList(new String[]{"estoy","estás","está","estamos","estáis","están"})));
        sightseeVerbo.addQuestion(new Question("mc","Las farmacias ______ cerradas los domingos","están",
                arrayStringToList(new String[]{"estoy","estás","está","estamos","estáis","están"})));
        sightseeVerbo.addQuestion(new Question("mc","¿A que hora ______ abierto el cine?","está",
                arrayStringToList(new String[]{"estoy","estás","está","estamos","estáis","están"})));
        sightseeVerbo.addQuestion(new Question("mc","¿Vosotras ______ en la Plaza Mayor?","estáis",
                arrayStringToList(new String[]{"estoy","estás","está","estamos","estáis","están"})));
        sightseeVerbo.addQuestion(new Question("mc","¿Cómo ______ ustedes?","están",
                arrayStringToList(new String[]{"estoy","estás","está","estamos","estáis","están"})));
        sightseeVerbo.addQuestion(new Question("mc","Mi amiga y yo ______ en el cine","estamos",
                arrayStringToList(new String[]{"estoy","estás","está","estamos","estáis","están"})));
        sightseeVerbo.addQuestion(new Question("mc","Yo ______ de vacaciones en Valencia","estoy",
                arrayStringToList(new String[]{"estoy","estás","está","estamos","estáis","están"})));
        sightseeVerbo.addQuestion(new Question("mc","El parque de atracciones ______ cerrado en invierno","está",
                arrayStringToList(new String[]{"estoy","estás","está","estamos","estáis","están"})));
        sightseeVerbo.addQuestion(new Question("mc","¿Dónde ______ el Parque del Retiro?","está",
                arrayStringToList(new String[]{"estoy","estás","está","estamos","estáis","están"})));

        Exercise sightseeVocabulario = new Exercise("Sightseeing/Vocabulario");
        allExercises.add(sightseeVocabulario);
        sightseeVocabulario.addQuestion(new Question("dd","iglesia","u3_iglesia",
                arrayStringToList(new String[]{"u3_farmacia","u3_iglesia","u3_jardin","u3_parque","u3_restaurante","u3_museo"})));
        sightseeVocabulario.addQuestion(new Question("dd","farmacia","u3_farmacia",
                arrayStringToList(new String[]{"u3_farmacia","u3_iglesia","u3_jardin","u3_parque","u3_restaurante","u3_museo"})));
        sightseeVocabulario.addQuestion(new Question("dd","restaurante","u3_restaurante",
                arrayStringToList(new String[]{"u3_farmacia","u3_iglesia","u3_jardin","u3_parque","u3_restaurante","u3_museo"})));
        sightseeVocabulario.addQuestion(new Question("dd","piscina","u3_piscina",
                arrayStringToList(new String[]{"u3_farmacia","u3_iglesia","u3_piscina","u3_parque","u3_restaurante","u3_museo"})));
        sightseeVocabulario.addQuestion(new Question("dd","tienda","u3_tienda",
                arrayStringToList(new String[]{"u3_farmacia","u3_iglesia","u3_jardin","u3_parque","u3_restaurante","u3_tienda"})));
        sightseeVocabulario.addQuestion(new Question("dd","jardín","u3_jardin",
                arrayStringToList(new String[]{"u3_farmacia","u3_iglesia","u3_jardin","u3_parque","u3_restaurante","u3_museo"})));
        sightseeVocabulario.addQuestion(new Question("dd","museo","u3_museo",
                arrayStringToList(new String[]{"u3_farmacia","u3_iglesia","u3_jardin","u3_parque","u3_restaurante","u3_museo"})));
        sightseeVocabulario.addQuestion(new Question("dd","plaza","u3_plaza",
                arrayStringToList(new String[]{"u3_plaza","u3_iglesia","u3_jardin","u3_parque","u3_restaurante","u3_museo"})));
        sightseeVocabulario.addQuestion(new Question("dd","cafetería","u3_cafeteria",
                arrayStringToList(new String[]{"u3_farmacia","u3_iglesia","u3_jardin","u3_parque","u3_restaurante","u3_cafeteria"})));
        sightseeVocabulario.addQuestion(new Question("dd","parque","u3_parque",
                arrayStringToList(new String[]{"u3_parque","u3_iglesia","u3_jardin","u3_farmacia","u3_restaurante","u3_tienda"})));


        // adding Directions Exercise
        Exercise directionImp1 = new Exercise("Directions/Imperativo 1");
        allExercises.add(directionImp1);
        directionImp1.addQuestion(new Question("mc","Ustedes _____","cojan",
                arrayStringToList(new String[]{"coja","cojamos","coge","coged","cojan"})));
        directionImp1.addQuestion(new Question("mc","Vosotros/as _____","coged",
                arrayStringToList(new String[]{"coja","cojamos","coge","coged","cojan"})));
        directionImp1.addQuestion(new Question("mc","Nosotros/as _____","cojamos",
                arrayStringToList(new String[]{"coja","cojamos","coge","coged","cojan"})));
        directionImp1.addQuestion(new Question("mc","Usted _____","coja",
                arrayStringToList(new String[]{"coja","cojamos","coge","coged","cojan"})));
        directionImp1.addQuestion(new Question("mc","Tú _____","coge",
                arrayStringToList(new String[]{"coja","cojamos","coge","coged","cojan"})));
        directionImp1.addQuestion(new Question("mc","Usted _____","tome",
                arrayStringToList(new String[]{"tomen","tome","tomemos","tomad","toma"})));
        directionImp1.addQuestion(new Question("mc","Ustedes _____","tomen",
                arrayStringToList(new String[]{"tomen","tome","tomemos","tomad","toma"})));
        directionImp1.addQuestion(new Question("mc","Nosotros/as _____","tomemos",
                arrayStringToList(new String[]{"tomen","tome","tomemos","tomad","toma"})));
        directionImp1.addQuestion(new Question("mc","Tú _____","toma",
                arrayStringToList(new String[]{"tomen","tome","tomemos","tomad","toma"})));
        directionImp1.addQuestion(new Question("mc","Vosotros/as _____","tomad",
                arrayStringToList(new String[]{"tomen","tome","tomemos","tomad","toma"})));

        Exercise directionImp2 = new Exercise("Directions/Imperativo 2");
        allExercises.add(directionImp2);
        directionImp2.addQuestion(new Question("mc","Ustedes _____","vayan",
                arrayStringToList(new String[]{"ve","vaya","id","vayamos","vayan"})));
        directionImp2.addQuestion(new Question("mc","Vosotros/as _____","id",
                arrayStringToList(new String[]{"ve","vaya","id","vayamos","vayan"})));
        directionImp2.addQuestion(new Question("mc","Nosotros/as _____","vayamos",
                arrayStringToList(new String[]{"ve","vaya","id","vayamos","vayan"})));
        directionImp2.addQuestion(new Question("mc","Usted _____","vaya",
                arrayStringToList(new String[]{"ve","vaya","id","vayamos","vayan"})));
        directionImp2.addQuestion(new Question("mc","Tú _____","ve",
                arrayStringToList(new String[]{"ve","vaya","id","vayamos","vayan"})));

        Exercise directionTu = new Exercise("Directions/Tú o Usted");
        allExercises.add(directionTu);
        directionTu.addQuestion(new Question("mc","ustedes ____","perdonen",
                arrayStringToList(new String[]{"perdonen","perdone","perdon","perdonamos","perdona"})));
        directionTu.addQuestion(new Question("mc","tú ____","perdona",
                arrayStringToList(new String[]{"perdonen","perdone","perdon","perdonad","perdona"})));
        directionTu.addQuestion(new Question("mc","vosotros ____","perdonad",
                arrayStringToList(new String[]{"perdonen","perdone","perdon","perdonad","perdona"})));
        directionTu.addQuestion(new Question("mc","ustedes ____","sigan",
                arrayStringToList(new String[]{"sigan","sigamos","sigue","siga","siguid"})));
        directionTu.addQuestion(new Question("mc","usted ____","siga",
                arrayStringToList(new String[]{"sigan","sigamos","sigue","siga","siguid"})));
        directionTu.addQuestion(new Question("mc","tú ____","sigue",
                arrayStringToList(new String[]{"sigan","sigamos","sigue","siga","siguid"})));
        directionTu.addQuestion(new Question("mc","usted ____","siga",
                arrayStringToList(new String[]{"sigan","sigamos","sigue","siga","siguid"})));
        directionTu.addQuestion(new Question("mc","tú ____","gira",
                arrayStringToList(new String[]{"gira","giramos","gire","giren","girad"})));
        directionTu.addQuestion(new Question("mc","ustedes ____","giren",
                arrayStringToList(new String[]{"gira","giramos","gire","giren","girad"})));
        directionTu.addQuestion(new Question("mc","usted ____","perdone",
                arrayStringToList(new String[]{"perdonen","perdone","perdon","perdonad","perdona"})));


        Exercise directionVocabulario = new Exercise("Directions/Vocabulario");
        allExercises.add(directionVocabulario);
        directionVocabulario.addQuestion(new Question("dd","banco","u4_bench",
                arrayStringToList(new String[] {"u4_bench","u4_man_on_bench","u4_park","u4_restaurant","u4_roundabout","u4_tree"})));
        directionVocabulario.addQuestion(new Question("dd","árbol","u4_tree",
                arrayStringToList(new String[] {"u4_bench","u4_man_on_bench","u4_park","u4_restaurant","u4_roundabout","u4_tree"})));
        directionVocabulario.addQuestion(new Question("dd","parque","u4_park",
                arrayStringToList(new String[] {"u4_bench","u4_man_on_bench","u4_park","u4_restaurant","u4_roundabout","u4_tree"})));
        directionVocabulario.addQuestion(new Question("dd","restaurante","u4_restaurant",
                arrayStringToList(new String[] {"u4_bench","u4_man_on_bench","u4_park","u4_restaurant","u4_roundabout","u4_tree"})));
        directionVocabulario.addQuestion(new Question("dd","glorieta","u4_roundabout",
                arrayStringToList(new String[] {"u4_bench","u4_man_on_bench","u4_park","u4_restaurant","u4_roundabout","u4_tree"})));
        directionVocabulario.addQuestion(new Question("dd","persona mayor","u4_man_on_bench",
                arrayStringToList(new String[] {"u4_bench","u4_man_on_bench","u4_park","u4_restaurant","u4_roundabout","u4_tree"})));


        //adding Eating exercises
        Exercise eatingVerbosQ = new Exercise("Eating/Verbos Querer y Pensar");
        allExercises.add(eatingVerbosQ);
        eatingVerbosQ.addQuestion(new Question("mc","¿Tú ______ Coca Cola?","quieres",
                arrayStringToList(new String[]{"quiero","quieres","quiere","queremos","queréis","quieren"})));
        eatingVerbosQ.addQuestion(new Question("mc","¿Ustedes ______ ir al cine?","quieren",
                arrayStringToList(new String[]{"quiero","quieres","quiere","queremos","queréis","quieren"})));
        eatingVerbosQ.addQuestion(new Question("mc","Marta ______ un café","quiere",
                arrayStringToList(new String[]{"quiero","quieres","quiere","queremos","queréis","quieren"})));
        eatingVerbosQ.addQuestion(new Question("mc","Yo no ______ postre","quiero",
                arrayStringToList(new String[]{"quiero","quieres","quiere","queremos","queréis","quieren"})));
        eatingVerbosQ.addQuestion(new Question("mc","¿Vosotros ______ una cerveza?","queréis",
                arrayStringToList(new String[]{"quiero","quieres","quiere","queremos","queréis","quieren"})));
        eatingVerbosQ.addQuestion(new Question("mc","Vosotros ______ demasiado","pensáis",
                arrayStringToList(new String[]{"piensas","piensa","pienso","pensamos","pensáis","piensan"})));
        eatingVerbosQ.addQuestion(new Question("mc","Yo ______ comprar un coche","pienso",
                arrayStringToList(new String[]{"piensas","piensa","pienso","pensamos","pensáis","piensan"})));
        eatingVerbosQ.addQuestion(new Question("mc","Nosotros ______ ir a México","pensamos",
                arrayStringToList(new String[]{"piensas","piensa","pienso","pensamos","pensáis","piensan"})));
        eatingVerbosQ.addQuestion(new Question("mc","Tú ______ ir al cine?","piensas",
                arrayStringToList(new String[]{"piensas","piensa","pienso","pensamos","pensáis","piensan"})));
        eatingVerbosQ.addQuestion(new Question("mc","Ellos ______ tomar un café","piensan",
                arrayStringToList(new String[]{"pensáis","piensas","pienso","piensan","piensa","pensamos"})));

        Exercise eatingVerboPoder = new Exercise("Eating/Verbo Poder");
        allExercises.add(eatingVerboPoder);
        eatingVerboPoder.addQuestion(new Question("mc","Vosotros ______","podéis",
                arrayStringToList(new String[]{"puedo","puedes","puede","podemos","podéis","pueden"})));
        eatingVerboPoder.addQuestion(new Question("mc","Ellos, ellas, ustedes ______","pueden",
                arrayStringToList(new String[]{"puedo","puedes","puede","podemos","podéis","pueden"})));
        eatingVerboPoder.addQuestion(new Question("mc","Él, ella, usted ______","puede",
                arrayStringToList(new String[]{"puedo","puedes","puede","podemos","podéis","pueden"})));
        eatingVerboPoder.addQuestion(new Question("mc","Yo ______","puedo",
                arrayStringToList(new String[]{"puedo","puedes","puede","podemos","podéis","pueden"})));
        eatingVerboPoder.addQuestion(new Question("mc","Nosotros ______","podemos",
                arrayStringToList(new String[]{"puedo","puedes","puede","podemos","podéis","pueden"})));
        eatingVerboPoder.addQuestion(new Question("mc","Tú ______","puedes",
                arrayStringToList(new String[]{"puedo","puedes","puede","podemos","podéis","pueden"})));

        Exercise eatingVerboJugar = new Exercise("Eating/Verbo Jugar");
        allExercises.add(eatingVerboJugar);
        eatingVerboJugar.addQuestion(new Question("mc","Tú _______","juegas",
                arrayStringToList(new String[]{"jugáis","juegas","jugamos","juego","juegan","juega"})));
        eatingVerboJugar.addQuestion(new Question("mc","Nosotros _______","jugamos",
                arrayStringToList(new String[]{"jugáis","juegas","jugamos","juego","juegan","juega"})));
        eatingVerboJugar.addQuestion(new Question("mc","Vosotros _______","jugáis",
                arrayStringToList(new String[]{"jugáis","juegas","jugamos","juego","juegan","juega"})));
        eatingVerboJugar.addQuestion(new Question("mc","Yo _______","juego",
                arrayStringToList(new String[]{"jugáis","juegas","jugamos","juego","juegan","juega"})));
        eatingVerboJugar.addQuestion(new Question("mc","Ellos, ellas, ustedes _______","juegan",
                arrayStringToList(new String[]{"jugáis","juegas","jugamos","juego","juegan","juega"})));
        eatingVerboJugar.addQuestion(new Question("mc","Él, ella, usted _______","juega",
                arrayStringToList(new String[]{"jugáis","juegas","jugamos","juego","juegan","juega"})));

        Exercise eatingVocabulario = new Exercise("Eating/Vocabulario");
        allExercises.add(eatingVocabulario);
        eatingVocabulario.addQuestion(new Question("dd","naranjas","u5_oranges",
                arrayStringToList(new String[]{"u5_apples","u5_avocado","u5_banana","u5_oranges","u5_tangerines","u5_tomatoes"})));
        eatingVocabulario.addQuestion(new Question("dd","plátano","u5_banana",
                arrayStringToList(new String[]{"u5_apples","u5_avocado","u5_banana","u5_oranges","u5_tangerines","u5_tomatoes"})));
        eatingVocabulario.addQuestion(new Question("dd","mandarinas","u5_tangerines",
                arrayStringToList(new String[]{"u5_apples","u5_avocado","u5_banana","u5_oranges","u5_tangerines","u5_tomatoes"})));
        eatingVocabulario.addQuestion(new Question("dd","manzanas","u5_apples",
                arrayStringToList(new String[]{"u5_apples","u5_avocado","u5_banana","u5_oranges","u5_tangerines","u5_tomatoes"})));
        eatingVocabulario.addQuestion(new Question("dd","aguacates","u5_avocado",
                arrayStringToList(new String[]{"u5_apples","u5_avocado","u5_banana","u5_oranges","u5_tangerines","u5_tomatoes"})));
        eatingVocabulario.addQuestion(new Question("dd","tomates","u5_tomatoes",
                arrayStringToList(new String[]{"u5_apples","u5_avocado","u5_banana","u5_oranges","u5_tangerines","u5_tomatoes"})));
        eatingVocabulario.addQuestion(new Question("dd","leche","u5_leche",
                arrayStringToList(new String[]{"u5_carne","u5_leche","u5_huevos","u5_pescado","u5_pollo","u5_vino"})));
        eatingVocabulario.addQuestion(new Question("dd","pollo","u5_pollo",
                arrayStringToList(new String[]{"u5_carne","u5_leche","u5_huevos","u5_pescado","u5_pollo","u5_vino"})));
        eatingVocabulario.addQuestion(new Question("dd","huevos","u5_huevos",
                arrayStringToList(new String[]{"u5_carne","u5_leche","u5_huevos","u5_pescado","u5_pollo","u5_vino"})));
        eatingVocabulario.addQuestion(new Question("dd","vino","u5_vino",
                arrayStringToList(new String[]{"u5_carne","u5_leche","u5_huevos","u5_pescado","u5_pollo","u5_vino"})));
        eatingVocabulario.addQuestion(new Question("dd","carne","u5_carne",
                arrayStringToList(new String[]{"u5_carne","u5_leche","u5_huevos","u5_pescado","u5_pollo","u5_vino"})));
        eatingVocabulario.addQuestion(new Question("dd","pescado","u5_pescado",
                arrayStringToList(new String[]{"u5_carne","u5_leche","u5_huevos","u5_pescado","u5_pollo","u5_vino"})));


        Exercise likesElVerboGustar = new Exercise("Likes/El Verbo Gustar");
        allExercises.add(likesElVerboGustar);
        likesElVerboGustar.addQuestion(new Question("mc","Me ______ los museos","gustan",
                arrayStringToList(new String[]{"gusta","gustan"})));
        likesElVerboGustar.addQuestion(new Question("mc","¿Te ______ el arte?","gusta",
                arrayStringToList(new String[]{"gusta","gustan"})));
        likesElVerboGustar.addQuestion(new Question("mc","Nos ______ jugar al fútbol?","gusta",
                arrayStringToList(new String[]{"gusta","gustan"})));
        likesElVerboGustar.addQuestion(new Question("mc","A Peter no le ______ ver la tele","gusta",
                arrayStringToList(new String[]{"gusta","gustan"})));
        likesElVerboGustar.addQuestion(new Question("mc","A Marta le ______ hablar por teléfono","gusta",
                arrayStringToList(new String[]{"gusta","gustan"})));
        likesElVerboGustar.addQuestion(new Question("mc","Nos ______ las motos","gustan",
                arrayStringToList(new String[]{"gusta","gustan"})));
        likesElVerboGustar.addQuestion(new Question("mc","Os ______ los libros","gustan",
                arrayStringToList(new String[]{"gusta","gustan"})));
        likesElVerboGustar.addQuestion(new Question("mc","Les ______ las pinturas","gustan",
                arrayStringToList(new String[]{"gusta","gustan"})));

        Exercise likesLosPronombres = new Exercise("Likes/Los Pronombres");
        allExercises.add(likesLosPronombres);
        likesLosPronombres.addQuestion(new Question("mc","A mí no ___ gustan los museos","me",
                arrayStringToList(new String[]{"me","te","le","nos","os","les"})));
        likesLosPronombres.addQuestion(new Question("mc","A ustedes no ___ gustan los bailes","les",
                arrayStringToList(new String[]{"me","te","le","nos","os","les"})));
        likesLosPronombres.addQuestion(new Question("mc","A él ___ gustan los autos","le",
                arrayStringToList(new String[]{"me","te","le","nos","os","les"})));
        likesLosPronombres.addQuestion(new Question("mc","A ella no ___ gustan las","le",
                arrayStringToList(new String[]{"me","te","le","nos","os","les"})));
        likesLosPronombres.addQuestion(new Question("mc","A nosotros ___ gustan las esculturas","nos",
                arrayStringToList(new String[]{"me","te","le","nos","os","les"})));
        likesLosPronombres.addQuestion(new Question("mc","A vosotros ___ gusta el arte","os",
                arrayStringToList(new String[]{"me","te","le","nos","os","les"})));

        Exercise likesGustos = new Exercise("Likes/Gustos");
        allExercises.add(likesGustos);
        likesGustos.addQuestion(new Question("mc","A mí no me gustan los museos :)","A mí tampoco",
                arrayStringToList(new String[]{"A mí también","A mí tampoco","A mí sí","A mí no"})));
        likesGustos.addQuestion(new Question("mc","Me gustan las películas de terror :)","A mí también",
                arrayStringToList(new String[]{"A mí también","A mí tampoco","A mí sí","A mí no"})));
        likesGustos.addQuestion(new Question("mc","No me gusta el pescado :(","A mí sí",
                arrayStringToList(new String[]{"A mí también","A mí tampoco","A mí sí","A mí no"})));
        likesGustos.addQuestion(new Question("mc","Me gusta mucho el chocolate :)","A mí también",
                arrayStringToList(new String[]{"A mí también","A mí tampoco","A mí sí","A mí no"})));
        likesGustos.addQuestion(new Question("mc","Me gusta ir de compras :)","A mí también",
                arrayStringToList(new String[]{"A mí también","A mí tampoco","A mí sí","A mí no"})));
        likesGustos.addQuestion(new Question("mc","No me gusta viajar :(","A mí sí",
                arrayStringToList(new String[]{"A mí también","A mí tampoco","A mí sí","A mí no"})));
        likesGustos.addQuestion(new Question("mc","Me gusta la música clásica :(","A mí no",
                arrayStringToList(new String[]{"A mí también","A mí tampoco","A mí sí","A mí no"})));
        likesGustos.addQuestion(new Question("mc","No me gusta leer el periódico :)","A mí tampoco",
                arrayStringToList(new String[]{"A mí también","A mí tampoco","A mí sí","A mí no"})));


        Exercise likesVocabulario = new Exercise("Likes/Vocabulario");
        allExercises.add(likesVocabulario);
        likesVocabulario.addQuestion(new Question("dd","escultura","u6_escultura",
                arrayStringToList(new String[]{"u6_arte_moderno","u6_cuadro","u6_deporte","u6_entrada","u6_escultura","u6_obra_de_arte"})));
        likesVocabulario.addQuestion(new Question("dd","cuadro","u6_cuadro",
                arrayStringToList(new String[]{"u6_arte_moderno","u6_cuadro","u6_deporte","u6_entrada","u6_escultura","u6_obra_de_arte"})));
        likesVocabulario.addQuestion(new Question("dd","deporte","u6_deporte",
                arrayStringToList(new String[]{"u6_arte_moderno","u6_cuadro","u6_deporte","u6_entrada","u6_escultura","u6_obra_de_arte"})));
        likesVocabulario.addQuestion(new Question("dd","arte moderno","u6_arte_moderno",
                arrayStringToList(new String[]{"u6_arte_moderno","u6_cuadro","u6_deporte","u6_entrada","u6_escultura","u6_obra_de_arte"})));
        likesVocabulario.addQuestion(new Question("dd","entrada","u6_entrada",
                arrayStringToList(new String[]{"u6_arte_moderno","u6_cuadro","u6_deporte","u6_entrada","u6_escultura","u6_obra_de_arte"})));
        likesVocabulario.addQuestion(new Question("dd","obra de arte","u6_obra_de_arte",
                arrayStringToList(new String[]{"u6_arte_moderno","u6_cuadro","u6_deporte","u6_entrada","u6_escultura","u6_obra_de_arte"})));

        //adding Planning exercises
        Exercise planningFuturo = new Exercise("Planning/Futuro con Ir");
        allExercises.add(planningFuturo);
        planningFuturo.addQuestion(new Question("mc","Yo ______ un vestido nuevo.","voy a comprar",
                arrayStringToList(new String[]{"vas a tener", "voy a comprar","vamos a ir","van a ver"})));
        planningFuturo.addQuestion(new Question("mc","Nosotros ______ a Madrid.","vamos a ir",
                arrayStringToList(new String[]{"vas a salir", "va a hablar","vamos a ir","vais a visitar"})));
        planningFuturo.addQuestion(new Question("mc","Vosotros ______ al bebé de Mar.","vais a ver",
                arrayStringToList(new String[]{"vais a ver", "voy a comprar","va a ver","vais a beber"})));
        planningFuturo.addQuestion(new Question("mc","Yo ______ en la biblioteca.","voy a estudiar",
                arrayStringToList(new String[]{"vas a tener", "voy a comprar","voy a estudiar","vais a presentar "})));
        planningFuturo.addQuestion(new Question("mc","El viernes Pablo ______ con sus amigos.","va a comer",
                arrayStringToList(new String[]{"va a comer", "voy a salir","vamos a estudiar","va a pasar"})));
        planningFuturo.addQuestion(new Question("mc","Yo te ______ un poquito de español.","voy a enseñar",
                arrayStringToList(new String[]{"vas a tener", "voy a enseñar","vamos a ir","voy a ver"})));
        planningFuturo.addQuestion(new Question("mc","Paz ______ a las 8:30 de la noche.","va a salir",
                arrayStringToList(new String[]{"va a salir", "voy a enseñar","vamos a ir","voy a ver"})));
        planningFuturo.addQuestion(new Question("mc","Ellos ______ en un restaurante.","van a comer",
                arrayStringToList(new String[]{"vas a salir", "voy a enseñar","van a comer","voy a ver"})));
        planningFuturo.addQuestion(new Question("mc","Yo ______ el periódico.","voy a leer",
                arrayStringToList(new String[]{"vas a salir", "voy a enseñar","vamos a ir","voy a leer"})));

        Exercise planningElVerbo = new Exercise("Planning/El Verbo Doler");
        allExercises.add(planningElVerbo);
        planningElVerbo.addQuestion(new Question("mc","Me ______ los pies.","duelen",
                arrayStringToList(new String[]{"duele","duelen"})));
        planningElVerbo.addQuestion(new Question("mc","¿Te ______ la cabeza?","duele",
                arrayStringToList(new String[]{"duele","duelen"})));
        planningElVerbo.addQuestion(new Question("mc","Nos ______ el cuerpo.","duele",
                arrayStringToList(new String[]{"duele","duelen"})));
        planningElVerbo.addQuestion(new Question("mc","A Peter no le ______ nada.","duele",
                arrayStringToList(new String[]{"duele","duelen"})));
        planningElVerbo.addQuestion(new Question("mc","A Marta le ______ el estómago.","duele",
                arrayStringToList(new String[]{"duele","duelen"})));
        planningElVerbo.addQuestion(new Question("mc","Nos ______ las manos.","duelen",
                arrayStringToList(new String[]{"duele","duelen"})));
        planningElVerbo.addQuestion(new Question("mc","¿Os ______ los brazos?","duelen",
                arrayStringToList(new String[]{"duele","duelen"})));
        planningElVerbo.addQuestion(new Question("mc","A Diana le ______ las piernas.","duelen",
                arrayStringToList(new String[]{"duele","duelen"})));


        Exercise planningPractla = new Exercise("Planning/Practica la Hora");
        allExercises.add(planningPractla);
        planningPractla.addQuestion(new Question("mc","¿Cómo se dice 5:30?","cinco y media",
                arrayStringToList(new String[]{"cinco y media","cinco menos media","seis y media"})));
        planningPractla.addQuestion(new Question("mc","¿Cómo se dice 6:10?","seis y deiz",
                arrayStringToList(new String[]{"seis menos diez","siete y diez","seis y deiz"})));
        planningPractla.addQuestion(new Question("mc","¿Cómo se dice 11:15?","once y cuarto",
                arrayStringToList(new String[]{"once y cuarto","once menos quince","quince para las once"})));
        planningPractla.addQuestion(new Question("mc","¿Cómo se dice 14:35?","catorce treinta y cinco",
                arrayStringToList(new String[]{"catorce treinta y cinco","tres y treinta y cinco","dos menos veinticinco"})));
        planningPractla.addQuestion(new Question("mc","¿Cómo se dice 12:45?","es la una menos cuarto",
                arrayStringToList(new String[]{"es la una menos cuarto","son la una menos cuarto","es las doce tres cuartos"})));
        planningPractla.addQuestion(new Question("mc","¿Cómo se dice 10:00?","diez en punto",
                arrayStringToList(new String[]{"doce en punto","diez en punto","once en punto"})));
        planningPractla.addQuestion(new Question("mc","¿Cómo se dice 7:15?","siete y cuarto",
                arrayStringToList(new String[]{"tres cuartos para las ocho","siete y cuarto","siete menos cuarto"})));
        planningPractla.addQuestion(new Question("mc","¿Cómo se dice 8:25?","son las ocho y veinticinco",
                arrayStringToList(new String[]{"son la ocho veinte","son las ocho y veinticinco","es las ocho veinticinco"})));
        planningPractla.addQuestion(new Question("mc","¿Cómo se dice 2:40?","son las dos cuarenta",
                arrayStringToList(new String[]{"es las dos cuarenta","es la una menos veinte","son las dos cuarenta"})));
        planningPractla.addQuestion(new Question("mc","¿Cómo se dice 4:15?","son las cuatro y cuarto",
                arrayStringToList(new String[]{"son las cuatro y cuarto","son las tres menos cuarto","son las cuatro menos cuarto"})));

        Exercise planningVocabulario = new Exercise("Planning/Vocabulario");
        allExercises.add(planningVocabulario);
        planningVocabulario.addQuestion(new Question("dd","zapatos","u7_zapatos",
                arrayStringToList(new String[]{"u7_centro_comercial","u7_escaleras_mecanicas","u7_zapateria","u7_zapatos","u7_supermercado","u7_quisco"})));
        planningVocabulario.addQuestion(new Question("dd","escaleras mecánicas","u7_escaleras_mecanicas",
                arrayStringToList(new String[]{"u7_centro_comercial","u7_escaleras_mecanicas","u7_zapateria","u7_sandalis","u7_supermercado","u7_quisco"})));
        planningVocabulario.addQuestion(new Question("dd","centro comercial","u7_centro_comercial",
                arrayStringToList(new String[]{"u7_centro_comercial","u7_escaleras_mecanicas","u7_zapateria","u7_zapatos","u7_supermercado","u7_quisco"})));
        planningVocabulario.addQuestion(new Question("dd","quiosco","u7_quisco",
                arrayStringToList(new String[]{"u7_centro_comercial","u7_escaleras_mecanicas","u7_zapateria","u7_zapatos","u7_supermercado","u7_quisco"})));
        planningVocabulario.addQuestion(new Question("dd","sandalias","u7_sandalis",
                arrayStringToList(new String[]{"u7_centro_comercial","u7_escaleras_mecanicas","u7_zapateria","u7_sandalis","u7_supermercado","u7_quisco"})));
        planningVocabulario.addQuestion(new Question("dd","supermercado","u7_supermercado",
                arrayStringToList(new String[]{"u7_centro_comercial","u7_escaleras_mecanicas","u7_zapateria","u7_sandalis","u7_supermercado","u7_quisco"})));
        planningVocabulario.addQuestion(new Question("dd","zapatería","u7_zapateria",
                arrayStringToList(new String[]{"u7_centro_comercial","u7_escaleras_mecanicas","u7_zapateria","u7_zapatos","u7_supermercado","u7_quisco"})));


        //adding Shopping exercises
        Exercise shoppingLosAdj = new Exercise("Shopping/Los Adjetivos");
        allExercises.add(shoppingLosAdj);
        shoppingLosAdj.addQuestion(new Question("mc","Una canción ______.","nueva",
                arrayStringToList(new String[]{"nueva","nuevas","largas","cortas"})));
        shoppingLosAdj.addQuestion(new Question("mc","Unas sandalias ______.","bonitas",
                arrayStringToList(new String[]{"bonitas","cómoda","nuevos","fea"})));
        shoppingLosAdj.addQuestion(new Question("mc","Un niño ______.","guapo",
                arrayStringToList(new String[]{"guapo","llorona","bonita","inteligentes"})));
        shoppingLosAdj.addQuestion(new Question("mc","Un vestido ______.","verde",
                arrayStringToList(new String[]{"bonita","rojas","verde","amarilla"})));
        shoppingLosAdj.addQuestion(new Question("mc","Unas camisetas ______.","moradas",
                arrayStringToList(new String[]{"moradas","verde","azul","rojos"})));
        shoppingLosAdj.addQuestion(new Question("mc","Una amiga ______.","inglesa",
                arrayStringToList(new String[]{"estadounidenses","mexicano","irlandés","inglesa"})));
        shoppingLosAdj.addQuestion(new Question("mc","Las tiendas son ______.","grandes",
                arrayStringToList(new String[]{"bonita","grandes","pequeña","fea"})));
        shoppingLosAdj.addQuestion(new Question("mc","El abrigo es ______.","blanco",
                arrayStringToList(new String[]{"blanco","negra","bonitas","grandes"})));

        Exercise shoppingObjDire = new Exercise("Shopping/Objeto Directo");
        allExercises.add(shoppingObjDire);
        shoppingObjDire.addQuestion(new Question("mc","Julia compra unas  sandalias, ______ compra en color negro.","las",
                arrayStringToList(new String[]{"les","las","los"})));
        shoppingObjDire.addQuestion(new Question("mc","Voy a comprar el vestido, ______ voy a pagar en efectivo.","lo",
                arrayStringToList(new String[]{"le","lo","la"})));
        shoppingObjDire.addQuestion(new Question("mc","Necesito comprar fruta ______ necesito comprar antes de ir a casa.","la",
                arrayStringToList(new String[]{"les","la","los"})));
        shoppingObjDire.addQuestion(new Question("mc","Quiero ver la película de Almodóvar ______ quiero ver esta tarde.","la",
                arrayStringToList(new String[]{"las","los","la"})));
        shoppingObjDire.addQuestion(new Question("mc","Me gusta ver la televisión, me gusta ______ por la tarde.","verla",
                arrayStringToList(new String[]{"verle","verlo","verla"})));
        shoppingObjDire.addQuestion(new Question("mc","Quiero leer un libro, quiero ______ en la biblioteca.","leerlo",
                arrayStringToList(new String[]{"leerte","leerlo","leerlaa"})));
        shoppingObjDire.addQuestion(new Question("mc","Ella necesita un vestido, ______ necesita para ir a la disco.","lo",
                arrayStringToList(new String[]{"la","lo","los"})));
        shoppingObjDire.addQuestion(new Question("mc","Nosotros vemos el fútbol todos los sábados, ¿ ______ quieres ver con nostros?","lo",
                arrayStringToList(new String[]{"nos","lo","los"})));
        shoppingObjDire.addQuestion(new Question("mc","Quiero unos zapatos, ______ quiero en talla 38.","los",
                arrayStringToList(new String[]{"le","las","los"})));
        shoppingObjDire.addQuestion(new Question("mc","Quiero una chaqueta, ______ quiero en talla mediana.","la",
                arrayStringToList(new String[]{"lo","le","la"})));

        Exercise shoppingQuisiera = new Exercise("Shopping/Quisiera");
        allExercises.add(shoppingQuisiera);
        shoppingQuisiera.addQuestion(new Question("mc","Yo _____ ir a pasear esta noche.","quisiera",
                arrayStringToList(new String[]{"quisiera","quisieras","quisiéramos","quisierais","quisieran"})));
        shoppingQuisiera.addQuestion(new Question("mc","¿Vosotros _____ ir al museo.","quisierais",
                arrayStringToList(new String[]{"quisiera","quisieras","quisiéramos","quisierais","quisieran"})));
        shoppingQuisiera.addQuestion(new Question("mc","Ellos _____ ir a descansar al hotel.","quisieran",
                arrayStringToList(new String[]{"quisiera","quisieras","quisiéramos","quisierais","quisieran"})));
        shoppingQuisiera.addQuestion(new Question("mc","Nosotros _____ ir al parque.","quisiéramos",
                arrayStringToList(new String[]{"quisiera","quisieras","quisiéramos","quisierais","quisieran"})));
        shoppingQuisiera.addQuestion(new Question("mc","¿Tú _____ ir de compras al centro?.","quisieras",
                arrayStringToList(new String[]{"quisiera","quisieras","quisiéramos","quisierais","quisieran"})));
        shoppingQuisiera.addQuestion(new Question("mc","Ella _____ comprar el jersey amarillo pero es muy caro.","quisiera",
                arrayStringToList(new String[]{"quisiera","quisieras","quisiéramos","quisierais","quisieran"})));


        Exercise shoppingVocabulario = new Exercise("Shopping/Vocabulario");
        allExercises.add(shoppingVocabulario);
        shoppingVocabulario.addQuestion(new Question("dd","vaqueros","u8_vaqueros",
                arrayStringToList(new String[]{"u8_camisa","u8_chaqueta","u8_efectivo","u8_jersey","u8_vaqueros","u8_vestido"})));
        shoppingVocabulario.addQuestion(new Question("dd","jersey","u8_jersey",
                arrayStringToList(new String[]{"u8_camisa","u8_chaqueta","u8_efectivo","u8_jersey","u8_vaqueros","u8_vestido"})));
        shoppingVocabulario.addQuestion(new Question("dd","vestido","u8_vestido",
                arrayStringToList(new String[]{"u8_camisa","u8_chaqueta","u8_efectivo","u8_jersey","u8_vaqueros","u8_vestido"})));
        shoppingVocabulario.addQuestion(new Question("dd","chaqueta","u8_chaqueta",
                arrayStringToList(new String[]{"u8_camisa","u8_chaqueta","u8_efectivo","u8_jersey","u8_vaqueros","u8_vestido"})));
        shoppingVocabulario.addQuestion(new Question("dd","camisa","u8_camisa",
                arrayStringToList(new String[]{"u8_camisa","u8_chaqueta","u8_efectivo","u8_jersey","u8_vaqueros","u8_vestido"})));
        shoppingVocabulario.addQuestion(new Question("dd","efectivo","u8_efectivo",
                arrayStringToList(new String[]{"u8_camisa","u8_chaqueta","u8_efectivo","u8_jersey","u8_vaqueros","u8_vestido"})));


        //adding Dating exercises
        Exercise datingInterro = new Exercise("Dating/Interrogativos");
        allExercises.add(datingInterro);
        datingInterro.addQuestion(new Question("mc","__________ quieres beber?","¿Qué",
                arrayStringToList(new String[]{"¿Cuánto","¿Cómo","¿Cuándo","¿Qué"})));
        datingInterro.addQuestion(new Question("mc","__________ quedamos para ir a la disco?","¿Cómo",
                arrayStringToList(new String[]{"¿Cuánto","¿Cómo","¿Cuándo","¿Qué"})));
        datingInterro.addQuestion(new Question("mc","__________ pasó por ti al hotel?","¿Cuándo",
                arrayStringToList(new String[]{"¿Cuánto","¿Cómo","¿Cuándo","¿Qué"})));
        datingInterro.addQuestion(new Question("mc","__________ es tu teléfono?","¿Cuál",
                arrayStringToList(new String[]{"¿Dónde","¿Cuál","¿Por qué","¿Quién"})));
        datingInterro.addQuestion(new Question("mc","__________ está tu hotel?","¿Dónde",
                arrayStringToList(new String[]{"¿Dónde","¿Cuál","¿Por qué","¿Quién"})));
        datingInterro.addQuestion(new Question("mc","__________ viene con nosotros esta noche?","¿Quién",
                arrayStringToList(new String[]{"¿Dónde","¿Cuál","¿Por qué","¿Quién"})));
        datingInterro.addQuestion(new Question("mc","__________ no nos vemos primero para tomar algo?","¿Por qué",
                arrayStringToList(new String[]{"¿Dónde","¿Cuál","¿Por qué","¿Quién"})));


        Exercise datingPrPo = new Exercise("Dating/Pronombres Posesivos");
        allExercises.add(datingPrPo);
        datingPrPo.addQuestion(new Question("mc","Voy a darte mi número de teléfono.","Voy a darte el mío",
                arrayStringToList(new String[]{"Voy a darte el suyo","Voy a darte el mío","Voy a darte el nuestro"})));
        datingPrPo.addQuestion(new Question("mc","Voy a darte nuestro número de teléfono.","Voy a darte el nuestro",
                arrayStringToList(new String[]{"Voy a darte el suyo","Voy a darte el mío","Voy a darte el nuestro"})));
        datingPrPo.addQuestion(new Question("mc","Voy a darte el número de teléfono de mi amigo.","Voy a darte el suyo",
                arrayStringToList(new String[]{"Voy a darte el suyo","Voy a darte el mío","Voy a darte el nuestro"})));
        datingPrPo.addQuestion(new Question("mc","Esta es mi dirección.","Esta es la mía",
                arrayStringToList(new String[]{"Esta es la suya","Esta es la mía","Esta es la nuestra"})));
        datingPrPo.addQuestion(new Question("mc","Esta es la chaqueta de Julia.","Esta es la suya",
                arrayStringToList(new String[]{"Esta es la suya","Esta son las suyas","Este es el suyo"})));
        datingPrPo.addQuestion(new Question("mc","Prefiero ir a mi casa.","Prefiero ir a la mia",
                arrayStringToList(new String[]{"Prefiero ir al mio","Prefiero ir a la mia","Prefiero ir a la tuya"})));


        Exercise datingAdjPos = new Exercise("Dating/Adjetivos Posesivos");
        allExercises.add(datingAdjPos);
        datingAdjPos.addQuestion(new Question("mc","Hola, te presento a ______ amiga Carla.","mi",
                arrayStringToList(new String[]{"mi","tu","su","nuestro","vuestro","sus"})));
        datingAdjPos.addQuestion(new Question("mc","Carla, ¿a qué hora sale ______ tren?","tu",
                arrayStringToList(new String[]{"mi","tu","su","nuestro","vuestro","sus"})));
        datingAdjPos.addQuestion(new Question("mc","Peter vuelve a Londres hoy, ______ avión sale a las 3:00.","su",
                arrayStringToList(new String[]{"mi","tu","su","nuestro","vuestro","sus"})));
        datingAdjPos.addQuestion(new Question("mc","Hola chicos, ¿a qué hora sale ______ autobús?","vuestro",
                arrayStringToList(new String[]{"mi","tu","su","nuestro","vuestro","sus"})));
        datingAdjPos.addQuestion(new Question("mc","Marta y yo trabajamos juntas ______ trabajo es un poco aburrido.","nuestro",
                arrayStringToList(new String[]{"mi","tu","su","nuestro","vuestro","sus"})));
        datingAdjPos.addQuestion(new Question("mc","Marta va a bailar mucho en la disco, por eso tiene que usar ______ zapatos cómodos.","sus",
                arrayStringToList(new String[]{"mi","tu","su","nuestro","vuestro","sus"})));


        Exercise datingVocabulario = new Exercise("Dating/Vocabulario");
        allExercises.add(datingVocabulario);
        datingVocabulario.addQuestion(new Question("dd","café","u9_cafe",
                arrayStringToList(new String[]{"u9_cafe","u9_pastel","u9_hielos","u9_galleta","u9_refresco","u9_zumo_de_naranja"})));
        datingVocabulario.addQuestion(new Question("dd","zumo de naranja","u9_zumo_de_naranja",
                arrayStringToList(new String[]{"u9_cafe","u9_pastel","u9_hielos","u9_galleta","u9_refresco","u9_zumo_de_naranja"})));
        datingVocabulario.addQuestion(new Question("dd","pastel","u9_galleta",
                arrayStringToList(new String[]{"u9_cafe","u9_pastel","u9_hielos","u9_galleta","u9_refresco","u9_zumo_de_naranja"})));
        datingVocabulario.addQuestion(new Question("dd","hielos","u9_hielos",
                arrayStringToList(new String[]{"u9_cafe","u9_pastel","u9_hielos","u9_galleta","u9_refresco","u9_zumo_de_naranja"})));
        datingVocabulario.addQuestion(new Question("dd","galleta","u9_pastel",
                arrayStringToList(new String[]{"u9_cafe","u9_pastel","u9_hielos","u9_galleta","u9_refresco","u9_zumo_de_naranja"})));
        datingVocabulario.addQuestion(new Question("dd","refresco","u9_refresco",
                arrayStringToList(new String[]{"u9_cafe","u9_pastel","u9_hielos","u9_galleta","u9_refresco","u9_zumo_de_naranja"})));
    }

    /**
     * @return all the exercises
     */
    ArrayList<Exercise> getExercisesList(){
        return allExercises;
    }
    // just some method needed...passing by
    private ArrayList<String> arrayStringToList(String[] input){
        ArrayList<String> result= new ArrayList<>();
        Collections.addAll(result, input);
        return result;
    }
}
