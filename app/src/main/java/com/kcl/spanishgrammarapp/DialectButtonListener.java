package com.kcl.spanishgrammarapp;

import android.view.View;
import android.widget.Button;

import com.kcl.spanishgrammarapp.resources.data.Region;

/**
 * Created by janaldoustorres on 18/12/2016.
 */

public class DialectButtonListener implements View.OnClickListener {
    private ParagonCustomButton spa, mex;

    public DialectButtonListener(ParagonCustomButton s, ParagonCustomButton m) {
        spa = s;
        mex = m;
    }

    @Override
    public void onClick(View v) {
        if(v == spa) {
            spa.changeToDefaultColor();
            mex.changeToPressedColor();
        } else {
            spa.changeToPressedColor();
            mex.changeToDefaultColor();
        }
        ResourcesActivity.DIALECT = Region.toRegion(((Button) v).getText().toString());
    }
}
