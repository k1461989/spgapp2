package com.kcl.spanishgrammarapp;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class is a representation of the Exercises that are available in the online Spanish Grammar Learning app.
 * Each object of this class contains several useful attributes that describe the Exercise, and it also holds
 * a collection of all the Questions that make up the exercise. The primary function of this class is to organise
 * the Questions and store some metadata about the collection.
 */
class Exercise implements Serializable {

    /**The collection of Questions for this Exercise.*/
    private ArrayList<Question> questions = new ArrayList<>();

    /**This int describes the number of questions within this exercise that have been answered correctly
     * regardless of whether or not the exercise has been finished yet.*/
    private int numCorrectlyAnswered = 0;

    /**A String unique to each exercise, used to identify and separate exercises from themselves.
    * This string describes where the Exercise belongs. Each Topic has a collection of Subtopics, and each
    * of those has its own exercise. This identifier is very important for loading user progress from file
    * because it allows us to use the particular instance with the right subtopic.*/
    private String identifier;

    /**Public constructor for this class. It only needs one argument, which is its identifier.
    * @param identifier is the "Topic/Subtopic" string that is used to identify where the Exercise belongs*/
    Exercise(String identifier){
        this.identifier = identifier;
    }

    /**Adds the given question object to the collection of Questions for this Exercise.
    * @param q Question type object to be added to this Exercise.*/
    void addQuestion(Question q){
        questions.add(q);
    }

    /**@return the collection of Questions for this Exercise.*/
    ArrayList<Question> getQuestions() {
        return questions;
    }

    /**@return the identifier of this exercise (i.e. "Topic/Subtopic")*/
    String getIdentifier(){
        return identifier;
    }

    /**Sets the number of questions answered correctly to the given input.
    * This is mainly used for resetting the value to 0 when resetting the exercise.
     * @param i the number of questions answered correctly.*/
    void setNumCorrectlyAnswered(int i){
        numCorrectlyAnswered = i;
    }

    /**Increases the value of the number of questions answered correctly by one.*/
    void incrementCorrectlyAnswered(){
        ++numCorrectlyAnswered;
    }

    private double correctnessRating;

    /**Returns the correctness rating for the exercise (a percentage as decimal)*/
    double getCorrectnessRating(){
        correctnessRating = numCorrectlyAnswered/ (double) questions.size();
        return correctnessRating;
    }

    void setCorrectnessRating(double correctnessRating){
        this.correctnessRating = correctnessRating;
    }

    /**
     * for creating a string output for Exercise
     * @return a String containing, topic, subtopic and rating
     */
    @Override
    public String toString(){
        String[] order = getIdentifier().split("/");
        return order[0]+" - "+order[1]+" - "+((Math.round(getCorrectnessRating()*100)))+"%";
    }
}
