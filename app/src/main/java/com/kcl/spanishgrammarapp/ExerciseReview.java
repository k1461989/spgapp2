package com.kcl.spanishgrammarapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * This class is responsible for giving the user a review of their performance for the exercise they've
 * just completed.*/
public class ExerciseReview extends Activity {

    private Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_exercise_review);
        createGUI();
    }

    /**
     * Creates the GUI elements for this activity*/
    private void createGUI(){
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        int screenWidth = size.x;

        TableLayout tableLayout = (TableLayout) findViewById(R.id.tl_exercise_review);

        TableRow tableRow = new TableRow(this);
        TableLayout.LayoutParams rowParams =
                new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
        rowParams.width=screenWidth;
        tableRow.setLayoutParams(rowParams);

        int questionNumber = 1;
        TextView header1 = new TextView(this); //1st header
        header1.setText("#");
        header1.setBackgroundColor(Color.parseColor("#739DA5A8"));
        header1.setTextSize(20);
        header1.setTypeface(font);
        tableRow.addView(header1);
        TextView header2 = new TextView(this); //2nd header
        header2.setText("Question");
        header2.setBackgroundColor(Color.parseColor("#73DAE6EB"));
        header2.setTextSize(20);
        header2.setTypeface(font);
        tableRow.addView(header2);
        TextView header3 = new TextView(this); //3rd header
        header3.setText("Correct");
        header3.setTextSize(20);
        header3.setTypeface(font);
        tableRow.addView(header3);
        tableLayout.addView(tableRow);
        for(Question q : getExercise().getQuestions()){
            TableRow tableRowQ = new TableRow(this);
            tableRowQ.setLayoutParams(rowParams);

            TextView number = new TextView(this);
            number.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            number.setText(questionNumber + ". ");
            number.setTextSize(20);
            number.setTypeface(font);
            tableRowQ.addView(number);

            TextView question = new TextView(this);
            if(questionNumber%2==0){ //alternate colours for rows
                question.setBackgroundColor(Color.parseColor("#73DAE6EB"));
            }else{
                question.setBackgroundColor(Color.parseColor("#739DA5A8"));
            }
            question.setLayoutParams(new TableRow.LayoutParams((int) (screenWidth * 0.65), TableRow.LayoutParams.WRAP_CONTENT));
            question.setTextSize(20);
            question.setTypeface(font);
            question.setText(q.getQuestion());
            tableRowQ.addView(question);

            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setGravity(Gravity.CENTER);
            ImageView imageView = new ImageView(this);
            TableRow.LayoutParams imageParams = new TableRow.LayoutParams(70,70);
            imageView.setLayoutParams(imageParams);
            if(q.isCompleted()){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    imageView.setBackground(getDrawable(R.drawable.check));
                } else {
                    imageView.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.check));
                }
            }else{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    imageView.setBackground(getDrawable(R.drawable.cross));
                } else {
                    imageView.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.cross));
                }
            }
            linearLayout.addView(imageView);
            tableRowQ.addView(linearLayout);

            tableLayout.addView(tableRowQ);
            ++questionNumber;
        }
    }

    /**
     * Finds the exercise the user has just completed*/
    private Exercise getExercise(){
        return (Exercise) getIntent().getExtras().getSerializable("EXERCISE");
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, SubtopicsActivity.class);
        intent.putExtra(ExercisesActivity.IDENTIFIER, getExercise().getIdentifier());
        intent.putExtra(MainActivity.TOPIC, getIntent().getStringExtra(MainActivity.TOPIC));
        startActivity(intent);
    }
}
