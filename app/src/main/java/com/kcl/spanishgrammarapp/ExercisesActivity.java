package com.kcl.spanishgrammarapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

@SuppressWarnings("ResourceType")
/**This class is responsible for the creation of the GUI elements and their functionality for Exercises.
 * It constructs the exercises (conceptually, what the user is doing to improve their language skills)
 * by using an Exercise object that has a collection of questions (ArrayList of Question objects).
 * This class can create Exercises of four types: Multiple Choice, Drag&Drop, Typing, True/False.*/
public class ExercisesActivity extends Activity {
    // Member variables for question types, to prevent bugs due to typos.
    public static final String multipleChoice = "mc";
    public static final String trueFalse = "tf";
    public static final String dragAndDrop = "dd";
    public static final String typing = "ty";
    public static final String audio = "au";
    public static final String IDENTIFIER = "IDENTIFIER";
    private RelativeLayout relativeLayout; //accessed multiple times throughout the code in various methods.
    private String cAnswer; //removing this would mean it features in 4 new places.
    private ArrayList<String> answers; //accessed in multiple places (10 in the class)
    private TextView question;//This was previously declared in onCreate(), but it needs to be private for DragAndDrop. Features in 3 methods.
    private ArrayList<Question> exerciseQuestions; //accessed in multiple methods (4).
    private int currentQuestionIndex = 0;  //used in 3 different methods
    private Exercise currentExercise;
    private LinearLayout llChoices;
    private GridLayout glChoices;
    private TextView tvInstructions;
    private boolean exerciseStarted = false;

    /**This String describes if the exercises is in-progress, completed or a new one (not previously started).
    * It saves a lot of redundant work once we have identified the category of the Exercise.*/
    private String exerciseState = "NEW";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercises);
        llChoices = (LinearLayout) findViewById(R.id.ll_exercises);
        glChoices = (GridLayout) findViewById(R.id.gl_exercises_choices);
        tvInstructions = (TextView) findViewById(R.id.tv_exercises_instructions);
        tvInstructions.setTypeface(Typeface.createFromAsset(getAssets(), "font2.ttf"));

        Intent intent = getIntent(); //the intent we used to make this activity...
        String identifier = intent.getStringExtra(ExercisesActivity.IDENTIFIER);

        //set title
        //getActionBar().setTitle(intent.getStringExtra(SubtopicsActivity.SUBTOPIC));

        /*Here's where some magic happens. We get the exercise using the getOrMakeExercise method. If the exercise for this
        * Topic/Subtopic combination has been successfully loaded from the UserProgress.ser file then the currentExercise is
        * just a new reference to that object. If it did not exist in the file, then a new exercise is made.*/
        currentExercise = getOrMakeExercise(identifier);
        try{
        exerciseQuestions = currentExercise.getQuestions();
        } catch (Exception e) {
            startActivity(new Intent(this, ErrorActivity.class));
        }
        if(exerciseQuestions.size()==0){ //spares us a crash if there are no questions available for the selected exercise
            onBackPressed();
            return;
        }

        /*There's one container used to hold all the questions of all types, and it is the relativeLayout variable.
        * This block of code initializes it, gives it layout parameters and adds it as the content view for the activity*/
        relativeLayout = (RelativeLayout) findViewById(R.id.rl_exercises);
        relativeLayout.setFocusableInTouchMode(true);
        /*Regardless of question type, all have a TextView. This block of code initializes the said TextView but does not assign
        * any text to it. It sets the font size, because we want this to be big and clear, and an arbitrary ID.*/
        question = (TextView) findViewById(R.id.tv_exercises_question);

        /*This is where we attempt to resume user's progress for this exercise. It may be the case that they have no progress
        * for this exercise, in which case this method does nothing. If it has been started, the user can resume, if it has been
        * completed the user can choose to restart the exercise.*/
        if(intent.getStringExtra("RESET")!=null) { //for the purpose of revise section
            restartExercise(identifier);
        }else {
            getExerciseFromSaved(identifier);
            resumeUserProgress(identifier);
        }
        reconstructGUI();
    }

    /**This method will return an exercise if it finds it in the collection of either completed or in-progress
    * exercises, and if not found in there then it will make and populate a new exercise for this topic/subtopic
    * @param identifier the unique identifying String for the exercise
    * @return Exercise for this configuration of topic/subtopic*/
    private Exercise getOrMakeExercise(String identifier){
        Exercise exercise = null;
        DataActivity dataActivity = new DataActivity();
        ArrayList<Exercise> allExercises = dataActivity.getExercisesList();
        for (Exercise e : allExercises) {
            if (e.getIdentifier().equals(identifier)) {
                exercise = e;
            }
        }
        return exercise; //create a new Exercise, a set of questions (empty)
    }

    /**This method searches the saved exercises collection for one with a matching identifier
     * @return an exercises from either the completedExercises or exercisesInProgress collections, or null if featured in neither.
     * @param identifier the identifier string of the exercise to search for in the exercises saved to file*/
    private Exercise getExerciseFromSaved(String identifier){
        if(UserProgress.completedExercises.size()>0) {
            for (Exercise e : UserProgress.completedExercises) {
                if (e.getIdentifier().equals(identifier)) {
                    exerciseState = "COMPLETED";
                    return e;
                }
            }
        }
        getExerciseInProgress(identifier);
        return null; //unlucky, didn't find the exercise in either collection
    }

    private Exercise getExerciseInProgress(String identifier){
        if(UserProgress.exercisesInProgress.size()>0) {
            for (Exercise e : UserProgress.exercisesInProgress) {
                if (e.getIdentifier().equals(identifier)) {
                    exerciseState = "PROGRESS";
                    return e;
                }
            }
        }
        return null;
    }

    /**This method is responsible for constructing the GUI based on the Question object that we
    * are currently referring to from the exerciseQuestions variable at the index of currentQuestionIndex.
    * It achieves this by calling the right method for each question type. Each question type has its own
    * method for constructing the correct GUI. Each of the methods adds some vital Tag data that is used in
    * determining if the user has the correct answer.*/
    private void reconstructGUI(){
        llChoices.removeAllViews();
        glChoices.removeAllViews();
        tvInstructions.setText("");
        question.setText(exerciseQuestions.get(currentQuestionIndex).getQuestion());
        question.setOnClickListener(null);
        cAnswer = exerciseQuestions.get(currentQuestionIndex).getCorrectAnswer();
        answers = exerciseQuestions.get(currentQuestionIndex).getAnswers();

        switch (exerciseQuestions.get(currentQuestionIndex).getQuestionType()){
            case multipleChoice:
                constructMultipleChoice();
                break;
            case trueFalse:
                constructTrueFalse();
                break;
            case dragAndDrop:
                constructDragAndDrop();
                break;
            case typing:
                constructTypingActivity(relativeLayout);
                break;
            case audio:
                constructAudioActivity();
                break;
            default:
                constructMultipleChoice();
        }
    }

    private void constructAudioActivity() {
        //play audio from start
        final MediaPlayer player;
        String audioName ="";
        Log.d("THE ID",question.getText().toString());

        if (question.getText().subSequence(0,6).equals("u02e03")){
            Log.d("The first letters",question.getText().subSequence(0,6).toString());
            String[] audioAndQuestion = question.getText().toString().split("-");
            audioName = audioAndQuestion[0];
            Log.d("AU::AUDIO NAME",audioName);
            String questionName = audioAndQuestion[1];
            Log.d("Audio Activity Name::",questionName);
            question.setText(questionName);
            Log.d("IF CONDITION","TRUE");

        }else{
            Log.d("ELSE CONDITION","TRUE");
            audioName = question.getText().toString();
            question.setText("Click to Play");
            Log.d("AU::AUDIO NAME",audioName);
        }


        player = MediaPlayer.create(this, this.getResources().getIdentifier(audioName,"raw",this.getPackageName()));
        player.start();

        //set question to play audio
        question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(player.isPlaying()) {
                    player.stop();
                }
                player.start();
                Log.d("clicked question","playing sound now");
            }
        });

        constructMultipleChoice();
    }

    /**This method constructs the GUI of Multiple Choice type questions.*/
    private void constructMultipleChoice(){
        LinearLayout.LayoutParams linearLayoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayoutParams.setMargins(5,10,5,10);
        for (int i = 0; i < answers.size(); i++) {
            Button btnChoice = new ParagonCustomButton(this);btnChoice.setLayoutParams(linearLayoutParams);
            btnChoice.setText(answers.get(i));
            btnChoice.setTag(answers.get(i));
            llChoices.addView(btnChoice);
            btnChoice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    results(multipleChoice, v);
                }
            });
        }

        //adding the instruction
        String theQuestionText = question.getText().subSequence(question.getText().length()-2,question.getText().length()).toString();
        Log.d("THE QUESTION TEXT::",theQuestionText);
        if (theQuestionText.equals(":)")||theQuestionText.equals(":(")){
            tvInstructions.setText("Happy face = same likes | Unhappy face = different likes");
        }
    }

    /**This method constructs the GUI of True or False type questions*/
    private void constructTrueFalse(){
        final Button trueButton = new Button(this);
        trueButton.setText("True");
        trueButton.setId(1010);
        trueButton.setTag("true");
        trueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(trueButton.getTag() + "");
                results(trueFalse, trueButton);
            }
        });
        final Button falseButton = new Button(this);
        falseButton.setText("False");
        falseButton.setId(1020);
        falseButton.setTag("false");
        falseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                results(trueFalse, falseButton);
            }
        });
        RelativeLayout.LayoutParams trueBtnParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        trueBtnParams.addRule(RelativeLayout.ABOVE, falseButton.getId());

        RelativeLayout.LayoutParams falseBtnParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        falseBtnParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        relativeLayout.addView(trueButton, trueBtnParams);
        relativeLayout.addView(falseButton, falseBtnParams);
    }


    /**This method constructs the GUI of Drag & Drop type questions*/
    private void constructDragAndDrop(){
        tvInstructions.setText("Drag the word to the picture");

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            glChoices.setRowCount(2);
            glChoices.setColumnCount(3);
        }else{
            glChoices.setRowCount(3);
            glChoices.setColumnCount(2);
        }

        question.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ClipData data = ClipData.newPlainText("", "");
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                    v.startDrag(data, shadowBuilder, v, 0);
                    return true;
                } else {
                    return false;
                }
            }
        });

        Display screen = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        screen.getSize(size);
        int imageWidth = size.x / 2 - 30;
        int imageHeight = size.y / 4 - 30;
        ViewGroup.LayoutParams glParams = new ViewGroup.LayoutParams(GridLayout.LayoutParams.WRAP_CONTENT, GridLayout.LayoutParams.WRAP_CONTENT);
        glParams.height = imageHeight;
        glParams.width = imageWidth;

        for (int i = 0; i < answers.size(); i++) {
            ImageView imageView = new ImageView(this);//
            imageView.setTag(answers.get(i));
            if (!answers.get(i).contains("null")) {
                try {
                    imageView.setImageResource(this.getResources().getIdentifier(answers.get(i), "drawable", this.getPackageName()));
                } catch (Exception e) {
                    startActivity(new Intent(this, ErrorActivity.class));
                }
                imageView.setOnDragListener(new View.OnDragListener() {
                    @Override
                    public boolean onDrag(View v, DragEvent event) {
                        switch (event.getAction()) {
                            case DragEvent.ACTION_DRAG_STARTED:
                                break;
                            case DragEvent.ACTION_DRAG_ENTERED:
                                break;
                            case DragEvent.ACTION_DRAG_EXITED:
                                break;
                            case DragEvent.ACTION_DRAG_LOCATION:
                                break;
                            case DragEvent.ACTION_DRAG_ENDED:
                                break;
                            case DragEvent.ACTION_DROP:
                                results(dragAndDrop, v);
                                break;
                            default:
                                break;
                        }
                        return true;
                    }
                });
                imageView.setLayoutParams(glParams);
                glChoices.addView(imageView);
            }
        }

    }

    /**This method constructs the GUI of Typing type questions*/
    private void constructTypingActivity(final RelativeLayout relativeLayout){
        final EditText userInput = new EditText(this);
        userInput.setId(10100);
        userInput.setInputType(InputType.TYPE_CLASS_TEXT);
        userInput.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    results(typing, userInput);
                } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                    System.out.println("TEST12345678910");
                    relativeLayout.requestFocus();
                }
                return true;
            }
        });
        RelativeLayout.LayoutParams editTextParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        editTextParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        relativeLayout.addView(userInput, editTextParams);
    }

    /**This method creates the dialog showing the result of the user's chosen answer (e.g. if it is right or wrong).
    * It also does a few things to facilitate and do save the user progress.
    * @param qType is the type of Question that this method is called for.
    * @param view is the view that was used to select the answer (e.g. a Button)*/
    public void results(String qType, View view){
        if (!exerciseStarted){
            exerciseStarted(); //mark this exercise as started after completing first question (i.e. it not counts as in-progress)
            exerciseStarted = true;
        }
        boolean correct = false; //we assume the user is wrong unless proven to be correct below
        String userAnswer; //this String is recorded in the Question object, and is useful for tracking user's progress. Initially it had more functionality intended.
        if (qType.equals(typing)){ //typing questions need a little different modification to the userAnswer
            userAnswer = ((EditText) view).getText().toString().trim();
        }else{
            userAnswer = view.getTag().toString(); //initialization of userAnswer in the general case. The GUI elements constructed have Tag data added, which is now read.
        }
        if (userAnswer.equals(cAnswer)){
            correct=true; //we have proved the user has answered the question correctly
            currentExercise.getQuestions().get(currentQuestionIndex).setCompleted(true);
            currentExercise.incrementCorrectlyAnswered(); //increment the number of correctly answered questions for the Exercise object
        }
        if(qType.equals(dragAndDrop)){
            correct = view.getTag().equals(cAnswer);
        }
        exerciseQuestions.get(currentQuestionIndex).userGivenAnswer = userAnswer; //record the user answer in the Exercise object
        exerciseQuestions.get(currentQuestionIndex).addAttempt(); //the user has attempted the exercise

        final AlertDialog.Builder alert = new AlertDialog.Builder(this); //this Alert Dialog Builder allows flexible creation of the content depending on user answer (e.g. right/wrong)
        if(correct) {
            alert.setTitle("Well done!");
            alert.setMessage("This is the Correct Answer!");
            exerciseQuestions.get(currentQuestionIndex).setAnswered(true);
        }else{
            alert.setTitle("Try again later");
            alert.setMessage("Incorrect Answer!");
            exerciseQuestions.get(currentQuestionIndex).setAnswered(true); //the definition of "Completed" has changed.
        }
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (currentQuestionIndex == exerciseQuestions.size() - 1) { //we have reached the end of the exercise
                    endOfExercise();
                } else {
                    ++currentQuestionIndex;
                    reconstructGUI(); //goes to next question
                }
            }
        });
        alert.setCancelable(false);
        exerciseState="PROGRESS";
        MainActivity.userProgress.saveProgress(); //save the user progress
        alert.create();
        alert.show();
    }

    /**This method is used to identify an exercise as in-progress. If it did not yet feature in the
    * exercisesInProgress collection in UserProgress then it is added.*/
    private void exerciseStarted(){
        if(!exerciseState.equals("PROGRESS")){
            UserProgress.exercisesInProgress.add(currentExercise);
        }
    }

    /**This method does a couple of necessary things when we reach the end of the exercise.*/
    private void endOfExercise(){
        Exercise exercise = getExerciseInProgress(currentExercise.getIdentifier());
        if(!retainHighestCorrectnessRating()){ //it can be that retainHighestRating returns false if there was no saved exercise found
            if(getExerciseFromSaved(currentExercise.getIdentifier())!=null) {
                UserProgress.completedExercises.remove(getExerciseFromSaved(currentExercise.getIdentifier())); //if there was in fact an exercise like this completed before, then remove it
            }
            UserProgress.completedExercises.add(exercise); //if we had an exercise, then at this point we removed it and need to add it back in. Or we didn't have it added to begin with.
        }
        UserProgress.exercisesInProgress.remove(currentExercise); //we've completed the exercise, it's no longer in progress
        MainActivity.userProgress.saveProgress();

        ratingDialog(currentExercise); //this will display current result, not best result, as intended
    }

    /**
     * This method ensures that the previous correctness rating does not get overwritten if
     * it was higher than in the current attempt
     * @return boolean value that is true is the saved exercise had a higher score*/
    private boolean retainHighestCorrectnessRating() {
        //This is a bit hard to get your head around, it has been simplified is why.
        return getExerciseFromSaved(currentExercise.getIdentifier()) != null
                && getExerciseFromSaved(currentExercise.getIdentifier()).getCorrectnessRating() > currentExercise.getCorrectnessRating();
    }

    /** This method displays the rating for the exercises with the appropriate pictures*/
    private void ratingDialog(final Exercise exercise){
        Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams((int) (getScreenWidth()*0.6), (int) (getScreenWidth()*0.18));
        rlp.setMargins(10,20,10,10);
        ImageView rating = new ImageView(getBaseContext());
        Double correctnessRating= currentExercise.getCorrectnessRating();
        if(correctnessRating<0.4) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                rating.setBackground(getDrawable(R.drawable.star0));
            } else {
                rating.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.star0));
            }
        }else if(correctnessRating<0.6){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                rating.setBackground(getDrawable(R.drawable.star1));
            } else {
                rating.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.star1));
            }
        }else if(correctnessRating<0.8){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                rating.setBackground(getDrawable(R.drawable.star2));
            } else {
                rating.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.star2));
            }
        }else if(correctnessRating>=0.8){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                rating.setBackground(getDrawable(R.drawable.star3));
            } else {
                rating.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.star2));
            }
        }
        dialog.setTitle("Exercise complete");
        dialog.addContentView(rating, rlp);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                reviewExercise(exercise);
            }
        });
        dialog.show();
    }

    private int getScreenWidth(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int screenWidth = displayMetrics.widthPixels;
        return screenWidth;
    }

    /**This method creates a dialog to communicate with the user and based on their input it
    * can resume their progress for a given Exercise or it will allow them to restart it.*/
    private void resumeUserProgress(final String identifier){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        if (exerciseState.equals("PROGRESS")) {
            currentExercise = getExerciseInProgress(identifier);
            exerciseQuestions = currentExercise.getQuestions();
            System.out.println("User has previously started this exercise");
            dialogBuilder.setTitle("You have previously started this exercise");
            dialogBuilder.setMessage("Would you like to resume from where you have finished?\n" +
                    "Pressing \"No\" will restart this exercise.");
        }else if (exerciseState.equals("COMPLETED")) {
            System.out.println("User has completed this exercise");
            dialogBuilder.setTitle("You have already completed this exercise");
            dialogBuilder.setMessage("Would you like to restart this exercise?");
        }else {
            return; //if there is no Exercise with the given identifier, then stop executing
        }
            dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (exerciseState.equals("COMPLETED")) {
                        restartExercise(identifier);
                    }
                    if(exerciseState.equals("PROGRESS")){
                        //resume from first uncompleted question
                        while (exerciseQuestions.get(currentQuestionIndex).isAnswered()) {
                            System.out.println("Completed question detected");
                            ++currentQuestionIndex;
                        }
                        reconstructGUI(); //redraw the new question
                    }
                }
            });
            dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(exerciseState.equals("PROGRESS")){
                        restartExercise(identifier);
                    }else{
                        onBackPressed();
                    }
                }
            });
        dialogBuilder.setCancelable(false);
        dialogBuilder.create();
        dialogBuilder.show();
    }

    /**Like you might expect it, this method restarts the exercise. It defaults some values.
     * @param identifier the identifier of the exercise to be restarted*/
    private void restartExercise(String identifier){
        /* Restart the exercise:
         * Remove from the list of completed exercises, set all questions to be not completed,
         * set all the user answers to null
         * start constructing the questions from scratch
         * add new empty question to "inProgress"*/
        for(Question q : getExerciseFromSaved(identifier).getQuestions()){
            q.setCompleted(false);
            q.setAnswered(false);
            q.userGivenAnswer = null;
        }
        currentExercise.setNumCorrectlyAnswered(0);
       if(exerciseState.equals("PROGRESS")){
            UserProgress.exercisesInProgress.remove(getExerciseFromSaved(identifier));
        }
    }

    private void reviewExercise(Exercise exercise){
        currentQuestionIndex = 0;
        Intent intent = new Intent(this, ExerciseReview.class);
        intent.putExtra(IDENTIFIER , exercise.getIdentifier());
        intent.putExtra(MainActivity.TOPIC, getIntent().getStringExtra(MainActivity.TOPIC));
        Bundle b = new Bundle();
        b.putSerializable("EXERCISE", exercise);
        intent.putExtras(b);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void instructions(){
        String instruction = "";
        switch (currentExercise.getQuestions().get(currentQuestionIndex).getQuestionType()){
            case multipleChoice:
                instruction = "Press the button with the correct answer to proceed. You only choose once.";
                break;
            case typing:
                instruction = "Tap the screen and a keyboard will appear. Type your answer and submit.";
                break;
            case trueFalse:
                instruction = "Choose whether or not the statement is true or false using the buttons.";
                break;
            case dragAndDrop:
                instruction = "Drag the word onto the picture that it matches and drop it there.";
                break;
            case audio:
                instruction = "Click the question/title to play the audio again";
        }
        //Toast toast = Toast.makeText(this, instruction, Toast.LENGTH_LONG);
        //toast.show();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Instructions");
        dialogBuilder.setMessage(instruction);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_exercises, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_instructions) {
            instructions();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
