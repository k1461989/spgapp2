package com.kcl.spanishgrammarapp;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.widget.CompoundButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;

public class ExtraActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extra);

        final TextView title = (TextView) findViewById(R.id.textViewTitle);
        title.setTypeface(Typeface.createFromAsset(getAssets(), "font2.ttf"));

        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollViewExtra);
        scrollView.removeAllViews();
        scrollView.addView(credits());

        ToggleButton toggleButton = (ToggleButton) findViewById(R.id.toggleButtonExtra);
        toggleButton.setTypeface(Typeface.createFromAsset(getAssets(), "font2.ttf"));
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    scrollView.removeAllViews();
                    scrollView.addView(credits());
                    title.setText("Credits");
                }else{
                    scrollView.removeAllViews();
                    scrollView.addView(help());
                    title.setText("Help");
                }
            }
        });
    }

    private TextView credits(){
        TextView credits = new TextView(this);
        credits.setGravity(Gravity.CENTER_HORIZONTAL);
        credits.setText(
                        "King’s Spanish \n" +
                        "Grammar, Vocabulary and more\n" +
                        "\n" +
                        "Copyright \n" +
                        "Modern Language Centre\n" +
                        "King’s College London 2016\n" +
                        "\n" +
                        "Project Board:\n" +
                        "Initial Stage: Dominique Borel and Catherine Brossard.\n" +
                        "Final Stage:  Dr. Ana María Sousa Aguiar de Medeiros and Anette Schroeder-Rossell.\n" +
                        "\n" +
                        "Project manager:\n" +
                        "Cecilia Trevino\n" +
                        "\n" +
                        "Instructiona Design: \n" +
                        "Cecilia Trevino\n" +
                        "\n" +
                        "Finance: Catherine Brossard\n" +
                        "\n" +
                        "Android Application Development \n" +
                        "Project Manager: Oskar Slyk. \n" +
                        "\n" +
                        "Content written by Cecilia Trevino with the help of Diego Iudicissa, Victoria Montesinos Gasser and Beatriz Sanitago.\n" +
                        "\n" +
                        "Language Consultancy: Alejandra Lopez Vazquez and Marta Nunez.\n" +
                        "Technical Consultancy: Philip Wallace and Vanessa Skiadelli and Oskar Slyk.\n" +
                        "\n" +
                        "Video Scripts written by Cecilia Trevino\n" +
                        "Video Production: Vanessa Skiadelli and Cecilia Trevino\n" +
                        "\n" +
                        "Android Application:\n"+
                        "Project manager: Oskar Slyk\n" +
                        "Front-end development: Jan Aldous Torres\n"+
                        "Back-end development: Tharshan Srikaran, Oskar Slyk\n" +
                        "In-app icons: Yun Zeng (Tracy)\n" +
                        "Logo: Arne Flach\n" +
                        "\n"+
                        "First iteration of the project:\n"+
                        "Project manager: Oskar Slyk\n"+
                        "Android front-end resources: Yun Zeng (Tracy)\n" +
                        "Android Back-end development: Tharshan Srikaran, Oskar Slyk\n"+
                        "Android recording tool: Nitin Kapoor\n" +
                        "Android video streaming: Shubham Goel\n" +
                        "CMS front-end (Heroku App): Jan Aldous Torres\n" +
                        "CMS back-end (Database): Jaesok An, Gun Park\n" +
                        "Logo: Shubham Goel\n" +
                        "\n" +
                        "Voices:\n" +
                        "James Brillantes\n" +
                        "Cecilia Trevino\n" +
                        "Adrian Gordaliza-Vega\n" +
                        "Beatriz Santiago\n" +
                        "Lucia S. Moraga\n" +
                        "Alejandra Coutts-Lopez\n" +
                        "Diego Flores-Jaime\n" +
                        "Audio recording and editing: James Brillantes and Cecilia Trevino\n" +
                        "\n" +
                        "Pictures selection and editing Victoria Montesinos Gasser and Cecilia Trevino\n" +
                        "\n" +
                        "Proofreading: Beatriz Santiago, Victoria Montesinos Gasser, Nick Shepherd and Tony Thorne.\n" +
                        "\n" +
                        "This project was funded by King’s College London Technology Enhanced Learning Fund.\n" +
                        "\n" +
                        "Special thanks to: \n" +
                        "\n" +
                        "Dr. Jeroen Keppens from the Department of Informatics. Faculty of Natural and Mathematical Sciences.\n" +
                        "\n" +
                        "Nelly Mars and Teresa García from the Modern Language Centre. Faculty of Arts and Humanities.\n" +
                        "\n" +
                        "Daniel Barrington and Dr. Ian Barrett from the Faculty of Arts and Humanities.\n" +
                        "Elena Hernandez-Martin, and Vanessa Skiadelli from the Centre of Technology Enhanced Learning. \n" +
                        "\n" +
                        "\n"
        );

        return credits;
    }

    private TextView help(){
        TextView help = new TextView(this);
        help.setGravity(Gravity.CENTER_HORIZONTAL);
        help.setText(
                "Welcome to the help section of the King's Spanish Grammar and Vocabulary App. This section" +
                        " outlines the key features of this app and explains how to use them." +
                        "\n\nThe first thing you will see is the Main Menu screen which contains 9 topics and three other buttons at the bottom." +
                        " The 9 topics are the core of the app. Within each topic are three sections outlined below." +
                "\n\nObserve section: This section contains situational videos. These show practical application of the Spanish grammar and vocabulary" +
                "\n\nReflect section: This section contains lecture videos. These focus on building your understanding of the grammar." +
                "\n\nExperiment section: This section is filled with exercises. This is your chance to practise what you have learned. " +
                        "There is a series of exercises for each Topic. We have several types of exercises, such as drag and drop, " +
                        "multiple choice, audio exercises and others.\n\n\n" +
                        "After completing an exercise, you will see a review page which tells you which questions you have answered correctly." +
                        "\n\nIn the Revise section of the App you can see all your previously completed exercises. These are sorted by the lowest scores first" +
                        " so you can prioritise your weakest areas easily. If you would like to retry an exercise you see in the" +
                        " Revise section, simply tap on it in the list, and you will be prompted to retry." +
                        "\n\n" +
                        "The Resources section of the app contains many useful and common Spanish words and phrases such as" +
                        " numbers, festivals, or times of the day. These are available in two accents, Spanish and Mexican." +
                        " Switch between them in the main menu of the Resources section." +
                        "\n\n" +
                        "The recording tool offers an opportunity to check your pronunciation." +
                        " You have access to the recording tool itself as well as all the audio resources" +
                        " from the Resources section. Listen to the Resources carefully and record your own attempt" +
                        " at pronouncing the words. You are your own judge in this one, and you can perfect your pronunciation" +
                        " using this tool at your convenience. The audio files are saved on your device so you can replay them" +
                        " at any time you want. If you would like to delete your own recordings, simply choose the one you wish to delete" +
                        " and press the bin icon." +
                        "\n\nThe glossary section is a mini-dictionary of the words used throughout this app. You can look up definitions" +
                        " or browse by letters of the alphabet. The list of letters is scrollable and you can use it to display all" +
                        " the words beginning with that letter.");
        return help;
    }
}
