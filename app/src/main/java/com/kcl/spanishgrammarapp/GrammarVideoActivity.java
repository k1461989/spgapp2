package com.kcl.spanishgrammarapp;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import com.kcl.spanishgrammarapp.CMS.CMS;

import static android.os.Environment.getExternalStorageDirectory;

/**
 * Created by janaldoustorres on 06/03/2016.
 */
public class GrammarVideoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grammar_video);

        final String AUTHORITY = "com.kcl.spanishgrammarapp.downloader.ZipFileContentProvider";
        final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

        //get intents
        String topicTitle = getIntent().getStringExtra(ReflectActivity.CURRENT_TOPIC);
        String subtopicNo = getIntent().getStringExtra(ReflectActivity.SUBTOPIC_NO);

        Log.d("SUBTOPIC NO", subtopicNo +"");

        int topicNo = CMS.getTopicNo(topicTitle);

        try {
//            Uri uri_video = Uri.parse("android.resource://" + getPackageName() + "/raw/u0" + topicNo + "_" + subtopicNo);
//            String video_path = getExternalStorageDirectory()+"/Android/obb/"+getPackageName()+"/u0" + topicNo + "_" + subtopicNo+".mp4";

            Uri video_uri = Uri.parse(CONTENT_URI +"/u0" + topicNo + "_" + subtopicNo+".mp4");

            //setup
            VideoView video_player_view = (VideoView) findViewById(R.id.vv_grammar_video);

            video_player_view.setVideoURI(video_uri);
            video_player_view.requestFocus();
            video_player_view.start();

            MediaController vidControl = new MediaController(this);
            vidControl.setAnchorView(video_player_view);
            video_player_view.setMediaController(vidControl);

        } catch (Exception e) {
            Log.e("Video not working", "ERROR");
        }
    }
}
