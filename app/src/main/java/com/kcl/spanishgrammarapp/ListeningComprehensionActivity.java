package com.kcl.spanishgrammarapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kcl.spanishgrammarapp.CMS.CMS;
import com.kcl.spanishgrammarapp.resources.data.ListeningComprehension;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Sets up a Listening Comprehension Activity
 */
public class ListeningComprehensionActivity extends Activity implements View.OnClickListener{

    private Uri uri;//uri of the audio
    private ArrayList<String> choices;
    private ArrayList<Integer> answers;// the correct answers
    private Button btnPlay;// to btnPlay and pause the audio
    private Button stop;//to stop the audio
    private MediaPlayer player;// used to ready the audio to execute
    private String status;// the status of the audio (PLAYED,PAUSED OR STOPPED)
    private SeekBar seekBar;// controls the current position of the audio
    private final Handler handler = new Handler();
    private ArrayList<CheckBox> checkBoxes;// the checkboxes containing all the answers
    private TextView currentTime;//shows current time position of the audio

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listening_comprehension);

        //gets topic no
        String topicTitle = getIntent().getStringExtra(ReflectActivity.CURRENT_TOPIC);
        int topicNo = CMS.getTopicNo(topicTitle);

        //get video file
        uri = Uri.parse("android.resource://" + getPackageName() + "/raw/lc_u0"+topicNo+"_02");

        //get data
        ListeningComprehension listeningComprehension = CMS.getListCompQuestion(getApplicationContext(), topicNo);
        choices = listeningComprehension.getChoices();
        answers = listeningComprehension.getAnswers();

        // makes the audio ready to be played
        readyAudio();

        //sets up the btnPlay/pause button
        btnPlay = (Button) findViewById(R.id.playPauseBtn);
        btnPlay.setOnClickListener(this);

        //sets up the stop button
        stop = (Button) findViewById(R.id.stopBtn);
        stop.setOnClickListener(this);

        //sets up the seekbar
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setMax(player.getDuration());
        //enables the seekbar to give the current progress of the audio
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                currentTime.setText(convertTime(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //sets the audio to seek when the seekbar is moved
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                seekPlayer(v);
                return false;
            }
        });

        //sets up the current time of teh audio
        currentTime = (TextView) findViewById(R.id.currentTime);
        currentTime.setText("0:00");

        //sets up the total time of the audio
        TextView endTime = (TextView) findViewById(R.id.endTime);
        endTime.setText(convertTime(player.getDuration()));

        LinearLayout optionsView = (LinearLayout) findViewById(R.id.optionsView);

        //sets up the checkboxes containing all the choices
        Log.d("choices", choices.toString());
        checkBoxes = new ArrayList<>();
        for (int i =0;i< choices.size();i++){

            checkBoxes.add(new CheckBox(this));
            checkBoxes.get(i).setText(choices.get(i));
            checkBoxes.get(i).setTextSize(20);
            checkBoxes.get(i).setTag(i + 1);
            checkBoxes.get(i).setTextColor(Color.WHITE);
            optionsView.addView(checkBoxes.get(i));

        }

        //RelativeLayout bottomView = (RelativeLayout) findViewById(R.id.questionView);

        //sets up the check button
        Button checkButton = (Button) findViewById(R.id.checkBtn);
        checkButton.setText("Check!");
        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer();
            }
        });

        TextView instructionsTextView = (TextView) findViewById(R.id.instructionsText);
        instructionsTextView.setTypeface(Typeface.createFromAsset(getAssets(), "font2.ttf"));
        instructionsTextView.setGravity(Gravity.CENTER);
    }

    /**
     * This listener method functions the btnPlay/pause and stop buttons.
     * @param view the button being clicked
     */
    @Override
    public void onClick(View view) {

        if (view == btnPlay){

            if(status=="stopped"){
                readyAudio();
                player.start();
                seekBarUpdater();
                status = "played";
                btnPlay.setBackgroundResource(R.drawable.pausebtn);
            }else if(status == "played"){
                player.pause();
                seekBarUpdater();
                status = "paused";
                btnPlay.setBackgroundResource(R.drawable.playbtn);
            }else{
                player.start();
                seekBarUpdater();
                status = "played";
                btnPlay.setBackgroundResource(R.drawable.pausebtn);
            }

        }else if(view == stop){
            player.pause();
            player.reset();
            status = "stopped";
            btnPlay.setBackgroundResource(R.drawable.playbtn);
        }
    }


    /**
     * This class loads the audio file from uri and makes it ready to btnPlay
     */
    public void readyAudio(){

        try {
            player = new MediaPlayer();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(getApplicationContext(), uri);
            player.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * This class enables the SeekBar to control the time position of the audio
     * @param view the seekbar being updated
     */
    public void seekPlayer(View view){
        if (player.isPlaying()){
            SeekBar sB = (SeekBar)view;
            player.seekTo(sB.getProgress());
        }

    }


    /**
     * Updates the SeekBar based on the time position of the audio
     */
    public void seekBarUpdater(){
        seekBar.setProgress(player.getCurrentPosition());

        if(player.isPlaying()){
            Runnable notification = new Runnable() {
                @Override
                public void run() {
                    seekBarUpdater();
                }
            };
            handler.postDelayed(notification, 1000);
        }else{
            player.pause();
            seekBar.setProgress(0);

        }
    }


    /**
     * Checks the validity of the chosen answers
     */
    public void checkAnswer() {

        ArrayList<CheckBox> list = new ArrayList<>();
        for (int i =0; i<checkBoxes.size();i++){

            if(checkBoxes.get(i).isChecked()){
                list.add(checkBoxes.get(i));
            }
        }


        Integer listSize = list.size();
        if (listSize== answers.size()){
            for (int i = 0; i< answers.size(); i++){
                for (int j =0;j<list.size();j++){
                    if (list.isEmpty()){
                        break;
                    }
                    if(list.get(j).getTag()== answers.get(i)){
                        list.remove(j);
                    }


                }
            }
        }



        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        if (list.isEmpty()&&listSize== answers.size()) {

            //do something when correct

            dialogBuilder.setTitle("Well Done");
            dialogBuilder.setMessage("This is the Correct Answer!");
            dialogBuilder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onBackPressed();
                }
            });
        } else {

            //do something when incorrect

            dialogBuilder.setTitle("Try Again");
            dialogBuilder.setMessage("Incorrect Answer!");
            dialogBuilder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //do something
                }
            });
            dialogBuilder.setNegativeButton("See Answer", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    showAnswer();
                }
            });
        }
        dialogBuilder.show();

    }

    private void showAnswer() {
        //make text green for correct answers
        for(Integer ans: answers) {
            checkBoxes.get(ans).setTextColor(Color.GREEN);
        }
    }


    /**
     * converts milliseconds into an appropriate format for minutes
     * @param value the milliseconds value
     * @return minutes format for milliseconds
     */
    public String convertTime(int value){

        Integer int1 = (int)(TimeUnit.MILLISECONDS.toSeconds(value));
        Integer mins = int1 /60;
        Integer secs = int1-(mins*60);
        if(secs<10){return mins +":"+"0"+secs;
        }else{return mins +":"+secs;}

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lc, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.listen_instructions) {
            String instruction = "Play the audio file and tick the boxes accordingly";
            Toast toast = Toast.makeText(this, instruction, Toast.LENGTH_LONG);
            toast.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
