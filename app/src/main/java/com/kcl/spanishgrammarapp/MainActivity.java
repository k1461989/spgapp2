package com.kcl.spanishgrammarapp;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity{

    public final static String TOPIC = "TOPIC";
    public final static String DIALECT = "DIALECT";

    public static UserProgress userProgress;

    private boolean permissionToRecordAccepted = false;
    private boolean permissionToWriteAccepted = false;
    private String [] permissions = {"android.permission.RECORD_AUDIO", "android.permission.WRITE_EXTERNAL_STORAGE"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userProgress = new UserProgress(this.getBaseContext());
        userProgress.loadProgress();

        TextView title = (TextView) findViewById(R.id.tv_mainActivity_title);
        title.setText(getIntent().getStringExtra(TOPIC));

        // setting permissions for recordingbtn
        int requestCode = 200;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);

        // ---------------------------------------modified
        setContentView(R.layout.activity_main);
    }

    public void learnActivity(View view){
        Intent intent = new Intent(this, ReflectActivity.class);
        intent.putExtra(TOPIC, getIntent().getStringExtra(TOPIC));
        startActivity(intent);
    }

    public void practiceActivity(View view){
        Intent intent = new Intent(this, SubtopicsActivity.class);
        intent.putExtra(TOPIC, getIntent().getStringExtra(TOPIC));
        startActivity(intent);
    }

    public void reviseActivity(View view){
        Intent intent = new Intent(this, ReviseActivity.class);
        intent.putExtra(TOPIC, getIntent().getStringExtra(TOPIC));
        startActivity(intent);
    }

    public void recordingTool(View view){
        Intent intent = new Intent(this,RecordingActivity.class);
        intent.putExtra(TOPIC, getIntent().getStringExtra(TOPIC));
        startActivity(intent);

    }

    /**
     * Run when subtopic button is clicked
     * @param view clicked button
     */
    public void enterSituationalVideo(View view){
        Intent intent = new Intent(this, SituationalVideoActivity.class);
        intent.putExtra(TOPIC, getIntent().getStringExtra(TOPIC));
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, TopicsActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Sets up the user permission requests for Recording
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 200:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                permissionToWriteAccepted  = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted ) MainActivity.super.finish();
        if (!permissionToWriteAccepted ) MainActivity.super.finish();
    }
}
