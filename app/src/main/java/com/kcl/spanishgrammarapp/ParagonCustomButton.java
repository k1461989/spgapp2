package com.kcl.spanishgrammarapp;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by janaldoustorres on 04/12/2016.
 */

public class ParagonCustomButton extends Button {
    public ParagonCustomButton(Context context) {
        super(context);
        setEverything(context);
    }

    public ParagonCustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setEverything(context);
    }

    private void setEverything(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "font2.ttf"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.setElevation(9);
        }
        this.setBackgroundResource(R.drawable.paragoncustombutton);
        this.setTextColor(ContextCompat.getColor(context, R.color.tracys_orange));
    }

    public void changeToPressedColor() {
        this.setBackgroundResource(R.drawable.mybuttonpressed);
    }

    public void changeToDefaultColor() {
        this.setBackgroundResource(R.drawable.mybutton);
    }
}
