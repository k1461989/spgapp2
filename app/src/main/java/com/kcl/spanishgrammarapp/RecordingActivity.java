package com.kcl.spanishgrammarapp;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kcl.spanishgrammarapp.recordings.RecordingsCustomAdapter;
import com.kcl.spanishgrammarapp.recordings.RecordingsFragment;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * This Activity sets up the Recording tool for the app.
 */
public class RecordingActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private Button record_Butn;
    private Button stop_Butn;
    private Button play_Butn;
    private Button pause_Butn;
    private Button delete_Butn;

    private ListView userRecsLV;
    private ArrayList<String> userRecsList = new ArrayList<>();
    private ArrayAdapter userRecsAdapter;

    private MediaRecorder myRecorder;
    private String outputFile = null;
    private MediaPlayer mediaPlayer = null;

    private boolean isRec=false;
    private boolean isPlay=false;
    private boolean isStart=false;
    private boolean isUserView = false;
    private boolean isSavedView = false;

    private String spgFolderLoc;
    private String selectedAudioPath;
    private String selectedAudioName;

    private TextView txtv;
    private ViewPager vpRecordings;
    private TabLayout tlRecordings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // create a folder for the app audio files
        final File spgDir = new File(Environment.getExternalStorageDirectory(),"SPG_App");
        if (!spgDir.exists())
        {
            spgDir.mkdirs();
            Log.d("FILE LOCATION:",spgDir.getAbsolutePath());
        }
        spgFolderLoc = spgDir.getAbsolutePath()+"/";

        setContentView(R.layout.activity_recording);

        //set up media controller buttons
        record_Butn = (Button) findViewById(R.id.btn_record);
        record_Butn.setOnClickListener(recordListener);
        record_Butn.setEnabled(true);

        stop_Butn = (Button) findViewById(R.id.btn_stop);
        stop_Butn.setEnabled(false);
        stop_Butn.setOnClickListener(stopListener);

        play_Butn = (Button) findViewById(R.id.btn_play);
        play_Butn.setEnabled(false);
        play_Butn.setOnClickListener(playListener);

        pause_Butn = (Button) findViewById(R.id.btn_pause);
        pause_Butn.setEnabled(false);
        pause_Butn.setOnClickListener(pauseListener);

        delete_Butn = (Button) findViewById(R.id.btn_trash);
        delete_Butn.setEnabled(false);
        delete_Butn.setOnClickListener(deleteListener);

        //set up user recording list view
        userRecsLV = (ListView) findViewById(R.id.lv_recordings_audio);
        userRecsAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, userRecsList);
        userRecsLV.setAdapter(userRecsAdapter);
        userRecsLV.setOnItemClickListener(this);
        updateUserAudioFiles();

        //set up recordings tabs
        vpRecordings = (ViewPager) findViewById(R.id.vp_recording);
        vpRecordings.setAdapter(new RecordingsCustomAdapter(getSupportFragmentManager(),getApplicationContext()));

        tlRecordings = (TabLayout) findViewById(R.id.tl_recording);
        tlRecordings.setupWithViewPager(vpRecordings);

        tlRecordings.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpRecordings.setCurrentItem(tab.getPosition());
                RecordingsFragment.setPageSelected(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                vpRecordings.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                vpRecordings.setCurrentItem(tab.getPosition());
            }
        });
    }


    /**
     * setting up the recorder
     */
    public void setupRecorder(){
        myRecorder = new MediaRecorder();
        String currentDT = DateFormat.getDateTimeInstance().format(new Date());
        outputFile = spgFolderLoc+formatDateTime(currentDT);
        userRecsList.add(formatOutputFile(outputFile));
        Log.d("OUTPUT FILE::: ",outputFile);
        myRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        myRecorder.setOutputFile(outputFile);
    }

    /**
     * formatting the date and time to use as a name of the file
     * @param str
     * @return
     */
    public String formatDateTime(String str){
        Log.d("THE REAL STRING:",str);
        String result="";
        for (int i =0;i<str.length();i++){

            if (str.charAt(i)==':'){
                Log.d("CurrentWord:",str.charAt(i)+"");
                result+="-";
            }else if(str.charAt(i)==' ' ){

                result+="_";

            } else {
                result+=str.charAt(i);
            }
        }

        return result;

    }

    /**
     * updating the audio files in the list view
     */
    public void updateUserAudioFiles(){

        File folder = new File(spgFolderLoc);
        Log.d("update Path::",spgFolderLoc);
        File[] audioFiles = folder.listFiles();
        Log.d("Length updated Files:",audioFiles.length+"");
        for (int i =0; i<audioFiles.length;i++){

            userRecsList.add(audioFiles[i].getName());
        }

    }

    /**
     * formatting the output file path to get the actual file name
     * @param str
     * @return
     */
    public String formatOutputFile(String str){

        String path="/storage/emulated/0/SPG_App/";
        String result = str.replace(path,"");

        return result;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        isStart = true;

        if (txtv!=null){
            txtv.setBackgroundColor(Color.WHITE);
        }

        play_Butn.setEnabled(true);
        delete_Butn.setEnabled(true);


        try {
            TextView txtView = (TextView) view;
            if (adapterView.equals(userRecsLV)) {
                mediaPlayer = new MediaPlayer();
                delete_Butn.setEnabled(true);
                Log.d("IT's USER:", adapterView + "");
                selectedAudioPath = spgFolderLoc + txtView.getText();
                selectedAudioName = txtView.getText().toString();
                mediaPlayer.setDataSource(selectedAudioPath);
                Log.d("THE CURRENT FILEPATH:", selectedAudioPath);
                Log.d("THE CURRENT FILE", selectedAudioName);

                isUserView = true;
                isSavedView = false;
            } else {
                Log.d("IT's SAVED:", adapterView + "");
                if (mediaPlayer!= null){
                    Log.d("IT's PLAYING","I STOP");
                    try{
                        mediaPlayer.stop();
                        mediaPlayer.release();
                        mediaPlayer=null;
                    }catch (IllegalStateException e){
                        e.printStackTrace();
                    }

                }
                mediaPlayer = new MediaPlayer();
                delete_Butn.setEnabled(false);
                mediaPlayer = MediaPlayer.create(this,
                        this.getResources().getIdentifier(txtView.getText().toString(), "raw", this.getPackageName()));
                isUserView = false;
                isSavedView = true;
            }

            txtView.setBackgroundColor(Color.LTGRAY);

        } catch (IOException e){
            e.printStackTrace();
        }

        try{mediaPlayer.prepare();}catch (IOException e){e.printStackTrace();}
        mediaPlayer.start();
        stop_Butn.setEnabled(false);
        pause_Butn.setEnabled(false);
        record_Butn.setEnabled(true);
        play_Butn.setEnabled(true);

        txtv = (TextView) view;
    }

    public void clickedRecording(int recording) {
        String rawName = getResources().getResourceName(recording);

        isStart = true;

        if (txtv!=null){
            txtv.setBackgroundColor(Color.WHITE);
        }

        play_Butn.setEnabled(true);
        delete_Butn.setEnabled(true);


        Log.d("IT's SAVED:",  "");
        if (mediaPlayer!= null){
            Log.d("IT's PLAYING","I STOP");
            try{
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer=null;
            }catch (IllegalStateException e){
                e.printStackTrace();
            }

        }
        mediaPlayer = new MediaPlayer();
        delete_Butn.setEnabled(false);
        mediaPlayer = MediaPlayer.create(this,
                this.getResources().getIdentifier(rawName, "raw", this.getPackageName()));
        mediaPlayer.start();
        isUserView = false;
        isSavedView = true;

        stop_Butn.setEnabled(false);
        pause_Butn.setEnabled(false);
        record_Butn.setEnabled(true);
        play_Butn.setEnabled(true);
    }


    /**
     * On click action for the recordbtn button
     */
    private View.OnClickListener recordListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        Log.d("THE RECORD BUTTON","working");
        setupRecorder();
        try {
            myRecorder.prepare();
            myRecorder.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        record_Butn.setEnabled(false);
        stop_Butn.setEnabled(true);
        play_Butn.setEnabled(false);
        pause_Butn.setEnabled(false);
        isRec = true;
        isPlay = false;
        record_Butn.setBackgroundResource(R.drawable.recordingbtn);

        Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_SHORT).show();

        }
    };

    /**
     * On click action for the pausebtn button
     */
    private View.OnClickListener pauseListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {


            Log.d("THE PAUSE BUTTON","working");

            mediaPlayer.pause();

            pause_Butn.setEnabled(false);
            play_Butn.setEnabled(true);
            stop_Butn.setEnabled(true);

            Toast.makeText(RecordingActivity.this, "Audio Paused",
                    Toast.LENGTH_SHORT).show();

        }
    };

    /**
     * On click action for the playbtn button
     */
    private View.OnClickListener playListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Log.d("THE PLAY BUTTON","working");

            if(isPlay&&isUserView) {
                Log.d("It's PLAY USER","");
                try {
                    mediaPlayer.setDataSource(selectedAudioPath);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    isPlay = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else if (isPlay&&isSavedView){


                Log.d("It's PLAY Saved","");

                mediaPlayer.start();

            } else if (isStart){


                Log.d("It's PLAY start","");
                mediaPlayer.start();
                isStart = false;

            } else{

                Log.d("It's PLAY else","");
                mediaPlayer.start();

            }

            record_Butn.setEnabled(true);
            pause_Butn.setEnabled(true);
            play_Butn.setEnabled(true);
            stop_Butn.setEnabled(true);
            isRec = false;
            isPlay = true;

            Toast.makeText(RecordingActivity.this, "Recording Playing",
                    Toast.LENGTH_SHORT).show();

        }
    };

    /**
     * On click action for the stopbtn button
     */
    private View.OnClickListener stopListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            Log.d("THE STOP BUTTON","working");

            if (isRec){

                try{
                    myRecorder.stop();
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }

                isRec = false;

                record_Butn.setBackgroundResource(R.drawable.recordbtn);

            }else{

                mediaPlayer.stop();

                isPlay = false;
            }

            stop_Butn.setEnabled(false);
            record_Butn.setEnabled(true);
            play_Butn.setEnabled(true);
            pause_Butn.setEnabled(false);
            userRecsAdapter.notifyDataSetChanged();

            Toast.makeText(getApplicationContext(), "Audio recorded successfully", Toast.LENGTH_SHORT).show();

        }
    };

    /**
     * On click action for the deletebtn button
     */
    private View.OnClickListener deleteListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d("THE DELETE BUTTON","working");
            File file = new File(selectedAudioPath);
            Log.d("THE PATH", selectedAudioPath);
            boolean deleted = file.delete();
            Log.d("The NAME",selectedAudioName);
            userRecsList.remove(selectedAudioName);
            userRecsAdapter.notifyDataSetChanged();

        }
    };

    @Override
    public void onBackPressed() {
        if (isPlay&&mediaPlayer.isPlaying()){
        mediaPlayer.stop();
        mediaPlayer.release();}
        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra(MainActivity.TOPIC, getIntent().getStringExtra(MainActivity.TOPIC));
        startActivity(intent);
        super.onBackPressed();
    }
}