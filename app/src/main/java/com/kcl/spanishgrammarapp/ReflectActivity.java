package com.kcl.spanishgrammarapp;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;

public class ReflectActivity extends Activity implements OnClickListener {
    public final static String CURRENT_TOPIC = "TOPIC";
    public final static String SUBTOPIC = "SUBTOPIC";
    public final static String SUBTOPIC_NO = "SUBTOPIC_NO";
    private String topic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Get current topic from intent
        topic = getIntent().getStringExtra(MainActivity.TOPIC);

        //Set up
        TextView textView = (TextView) findViewById(R.id.tv_topic);
        textView.setText(topic);

        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.main_layout_id);
        mainLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        // Arranging data for buttons
        String[] greetings = {"Pronombres", "Llamarse", "Ser y Estar", "Vocabulario"};
        String[] checkingin = {"Nacionalidades", "Verbos Regulares", "Verbos \"go\"", "Vocabulario"};
        String[] sightseeing = {"Hay que y Tener que", "Las Preposiciones", "El Verbo Estar", "Vocabulario"};
        String[] directions = {"Imperativo", "Tú o Usted", "El Verbo Ir", "Vocabulario"};
        String[] eating = {"Verbos Irregulares", "Comida y Precios", "Vocabulario"};
        String[] likes = {"Los Gustos", "El Verbo Gustar" ,"Vocabulario"};
        String[] planning = {"El  Futuro con Ir", "El Verbo Doler", "Vocabulario"};
        String[] shopping = {"Adjetivos", "Objeto Directo", "Vocabulario"};
        String[] dating = { "Interrogativos","Posesivos", "Vocabulario"};
        HashMap<String, String[]> subtopics = new HashMap<>();
        subtopics.put("Greetings", greetings);
        subtopics.put("Checking-in", checkingin);
        subtopics.put("Sightseeing", sightseeing);
        subtopics.put("Directions", directions);
        subtopics.put("Eating", eating);
        subtopics.put("Likes", likes);
        subtopics.put("Planning", planning);
        subtopics.put("Shopping", shopping);
        subtopics.put("Dating", dating);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10,10,10,10);

        String[] subtopics_of_topic = subtopics.get(topic);
        for (int i = 0; i < subtopics_of_topic.length; i++) {
            String subtopicTitle = subtopics_of_topic[i];
            ParagonCustomButton button = new ParagonCustomButton(this);
            button.setText(subtopicTitle);
            if(i == subtopics_of_topic.length-1) {//is last element, then vocab
                button.setTag("vocab");
            } else {
                button.setTag("0"+(i+1)+"_gra");
            }
            button.setOnClickListener(this);
            mainLayout.addView(button, params);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        intent = new Intent(this, GrammarVideoActivity.class);
        String subtopicTitle = ((TextView) v).getText().toString();
        String subtopicNo = ((Button) v).getTag().toString();
        intent.putExtra(CURRENT_TOPIC, topic);
        intent.putExtra(SUBTOPIC, subtopicTitle);
        intent.putExtra(SUBTOPIC_NO, subtopicNo);

        startActivity(intent);}

        /*
        * Greetings: "Pronombres", "Llamarse", "Ser y Estar", "Vocabulario"
        * Checking-in: "Nacionalidades", "Verbos Regulares", "Verbos \"go\"", "Vocabulario"
        * Sightseeing: "Hay que y Tener que", "Las Preposiciones", "El Verbo Estar", "Vocabulario"
        * Directions: "Imperativo", "Tú o Usted", "El Verbo Ir", "Vocabulario"
        * Eating: "Verbos Irregulares", "Comida y Precios", "Vocabulario"
        * Likes: "Lost Gustos", "El Verbo Gustar", "Vocabulario"
        * Planning: "El  Futuro con Ir", "El Verbo Doler", "Vocabulario"
        * Shopping: "Adjetivos", "Objeto Directo", "Vocabulario"
        * Dating: "Interrogativos", "Posesivos", "Vocabulario"
        * */
}
