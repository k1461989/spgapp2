package com.kcl.spanishgrammarapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.kcl.spanishgrammarapp.resources.activities.AlphabetActivity;
import com.kcl.spanishgrammarapp.resources.activities.DayActivity;
import com.kcl.spanishgrammarapp.resources.activities.HolidaysActivity;
import com.kcl.spanishgrammarapp.resources.activities.NumbersActivity;
import com.kcl.spanishgrammarapp.resources.activities.SeasonsAndMonthsActivity;
import com.kcl.spanishgrammarapp.resources.activities.TimeActivity;
import com.kcl.spanishgrammarapp.resources.data.Region;

import java.util.ArrayList;

public class ResourcesActivity extends Activity {
    public static Region DIALECT = Region.SPAIN;
    public static String RESOURCE_NAME = "Resource";
    public static final String ALPHABET = "Alphabet";
    public static final String NUMBERS = "Numbers";
    public static final String DAYS = "Days";
    public static final String HOLIDAYS = "Holidays";
    public static final String SEASONS_MONTHS = "Seasons and Months";
    public static final String TIME = "Time";
    private ArrayList<View> icons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resources);

        GridLayout glResources = (GridLayout) findViewById(R.id.gl_resources);
        glResources.removeAllViews();

        icons = new ArrayList<>(); //create and populate this ArrayList because there are 2 ways these icons can appear, this makes the code more efficient.
        String[] topics = {"El Alfabeto", "Los Números", "Los Días",
                "Festividades", "Estaciones y Meses", "La Hora"
        };
        String[] tags = {ALPHABET, NUMBERS, DAYS,
                 HOLIDAYS, SEASONS_MONTHS,
                TIME
        };
        int[] imgs = {R.drawable.alphabet, R.drawable.numbers, R.drawable.week,
                 R.drawable.festival, R.drawable.seasons,
                R.drawable.clock
        };

        for (int i = 0; i < topics.length; i++) {
            View iconView =
                    null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                iconView = getLayoutInflater().inflate(R.layout.icon, glResources, false);
            } else {
                iconView = getLayoutInflater().inflate(R.layout.icon, null);
            }
            iconView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showActivity(v);
                }
            });
            iconView.setTag(tags[i]);
            glResources.addView(iconView);

            ImageView ivIcon = (ImageView) iconView.findViewById(R.id.iv_icon);
            ivIcon.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), imgs[i]));

            TextView tvIcon = (TextView) iconView.findViewById(R.id.tv_icon);
            tvIcon.setText(topics[i]);
            tvIcon.setTypeface(Typeface.createFromAsset(getAssets(), "arial_bold.ttf"));

            icons.add(iconView);
        }
        setDefaultImageButtonSizes();

        //set dialect buttons
        ParagonCustomButton btnSpanish = (ParagonCustomButton) findViewById(R.id.btn_spanish);
        ParagonCustomButton btnMexican = (ParagonCustomButton) findViewById(R.id.btn_mexican);
        btnMexican.changeToPressedColor();//default dialect is Spanish

        DialectButtonListener dbl = new DialectButtonListener(btnSpanish, btnMexican);
        btnMexican.setOnClickListener(dbl);
        btnSpanish.setOnClickListener(dbl);
    }

    private void setDefaultImageButtonSizes() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        // ---------------------------------------modified
        int screenWidth = displayMetrics.widthPixels;
        int screenHeight = displayMetrics.heightPixels;

        int idealSize = (screenWidth-64)/3;

        for(View icon : icons){
            ViewGroup.LayoutParams params = icon.findViewById(R.id.iv_icon).getLayoutParams();
            params.width = idealSize;
            params.height = idealSize;
        }
    }

    public void showActivity(View v) {
        switch (v.getTag().toString()) {
            case ALPHABET:
                showAlphabetActivity(v);
                return;
            case NUMBERS:
                showNumbersActivity(v);
                return;
            case DAYS:
                showDaysActivity(v);
                return;
            case HOLIDAYS:
                showHolidaysActivity(v);
                return;
            case SEASONS_MONTHS:
                showSeasonsAndMonthsActivity(v);
                return;
            case TIME:
                showTimeActivity(v);
                return;
            default:
                return;
        }
    }

    public void showAlphabetActivity(View v){
        Intent intent = new Intent(this, AlphabetActivity.class);
        intent.putExtra(RESOURCE_NAME, ALPHABET);
        intent.putExtra(MainActivity.DIALECT, getIntent().getStringExtra(MainActivity.DIALECT));
        startActivity(intent);
    }

    public void showNumbersActivity(View v){
        Intent intent = new Intent(this, NumbersActivity.class);
        intent.putExtra(RESOURCE_NAME, NUMBERS);
        intent.putExtra(MainActivity.DIALECT, getIntent().getStringExtra(MainActivity.DIALECT));
        startActivity(intent);
    }

    public void showDaysActivity(View v){
        Intent intent = new Intent(this, DayActivity.class);
        intent.putExtra(RESOURCE_NAME, DAYS);
        intent.putExtra(MainActivity.DIALECT, getIntent().getStringExtra(MainActivity.DIALECT));
        startActivity(intent);
    }

    public void showHolidaysActivity(View v){
        Intent intent = new Intent(this, HolidaysActivity.class);
        intent.putExtra(RESOURCE_NAME, HOLIDAYS);
        intent.putExtra(MainActivity.DIALECT, getIntent().getStringExtra(MainActivity.DIALECT));
        startActivity(intent);
    }

    public void showSeasonsAndMonthsActivity(View v) {
        Intent intent = new Intent(this, SeasonsAndMonthsActivity.class);
        intent.putExtra(RESOURCE_NAME, SEASONS_MONTHS);
        intent.putExtra(MainActivity.DIALECT, getIntent().getStringExtra(MainActivity.DIALECT));
        startActivity(intent);
    }

    public void showTimeActivity(View v) {
        Intent intent = new Intent(this, TimeActivity.class);
        intent.putExtra(RESOURCE_NAME, TIME);
        intent.putExtra(MainActivity.DIALECT, getIntent().getStringExtra(MainActivity.DIALECT));
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_resources, menu);
        return true;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_resources);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            System.out.println("ORIENTATION_LANDSCAPE");
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            System.out.println("ORIENTATION_PORTRAIT");
        }
    }
}
