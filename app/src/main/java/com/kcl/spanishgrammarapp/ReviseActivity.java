package com.kcl.spanishgrammarapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * This Activity displays all the
 */
public class ReviseActivity extends Activity implements AdapterView.OnItemClickListener{

    private RelativeLayout mainLayout; //This is the layout for this Activity
    private Context context; // This is the Context of this Activity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_revise);
        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        context = this;

        /* This sets up the current Activity to show all the completed Exercises on a list View*/
        showListView();
    }

    /**
     * This creates a ListView of all the completed exercises based on correctness rating.
     * There is also text explaining this screen, and a message if there are no exercises to display.
     */
    public void showListView(){
        ListView listView = (ListView) findViewById(R.id.lv_revise);

        ArrayList<Exercise> exerciseList = new ArrayList<>(UserProgress.completedExercises);
        TextView tvInstructions = (TextView) findViewById(R.id.tv_revise_instructions);
        tvInstructions.setText("This is the place where you can find all the exercises you have previously completed. Exercises here are sorted by their correctness rating. You can retry the exercises from here. ");
        tvInstructions.setTypeface(Typeface.createFromAsset(getAssets(), "font2.ttf"));
        tvInstructions.setTextSize(16f);

        if(exerciseList.size()==0){
            tvInstructions.setText("It appears you have not yet completed any exercises. You can find exercises in the Practice section available in the main menu.");
        }else {
            Collections.sort(exerciseList, new Comparator<Exercise>() {
                @Override
                public int compare(Exercise exercise1, Exercise exercise2) {
                    if (exercise1.getCorrectnessRating() >= exercise2.getCorrectnessRating()) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            });
            ArrayAdapter<Exercise> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, exerciseList);
            listView.setOnItemClickListener(this);
            listView.setAdapter(adapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_revise, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {

        final Exercise curExe = (Exercise) parent.getAdapter().getItem(position);
        final String identifier = curExe.getIdentifier();

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Do you want to Attempt this Exercise?");
        dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context, ExercisesActivity.class);
                intent.putExtra("IDENTIFIER", identifier);
                intent.putExtra(MainActivity.TOPIC, identifier.split("/")[0]);
                intent.putExtra("RESET", "true"); //this is for me, so when you start your Exercise from THIS class, I'll know to reset the exercise rather than ask user to reset it.
                startActivity(intent);
            }
        });
        dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            //do nothing
            }
        });

        dialogBuilder.create();
        dialogBuilder.show();
    }
    /**This method ensures the correct behaviour of the app when the back button is pressed.*/
    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(MainActivity.TOPIC, getIntent().getStringExtra(MainActivity.TOPIC));
        startActivity(intent);
    }
}
