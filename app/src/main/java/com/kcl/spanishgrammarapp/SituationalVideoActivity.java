package com.kcl.spanishgrammarapp;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.MediaController;
import android.widget.ToggleButton;
import android.widget.VideoView;

import com.kcl.spanishgrammarapp.CMS.CMS;

/**
 * Created by janaldoustorres on 03/03/2016.
 */
public class SituationalVideoActivity extends Activity {
    //private static final String urlCMS = "https://lang-it-up.herokuapp.com/";
    /**
     * Current topic of this situational video
     */
    private String current_topic;
    /**
     * Url of situational video with transcript
     */
    private Uri uri_with_transcript;
    /**
     * Url of situational video without transcript
     */
    private Uri uri_without_transcript;
    /**
     * Stop position of video when paused
     */
    private static int stopPosition;
    /**
     * File that is currently playing on VideoView
     */
    private Uri currentVidPath;

    private VideoView video_player_view;
    private MediaController vidControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_situational_video);

        //get current topic from intent
        current_topic = getIntent().getStringExtra(ReflectActivity.CURRENT_TOPIC);

        int topicNo = CMS.getTopicNo(current_topic);

        uri_without_transcript = Uri.parse("android.resource://" + getPackageName() + "/raw/lc_u0"+topicNo+"_01_no_sub");
        uri_with_transcript = Uri.parse("android.resource://" + getPackageName() + "/raw/lc_u0"+topicNo+"_02");

        Log.d("URI PARSED", uri_without_transcript.toString() + "...." + uri_with_transcript.toString());

        //View dynamic setup
        video_player_view = (VideoView) findViewById(R.id.vv_situational_video);
        video_player_view.start();

        vidControl = new MediaController(this);
        vidControl.setAnchorView(video_player_view);
        video_player_view.setMediaController(vidControl);

        ToggleButton toggleButton_transcript = (ToggleButton) findViewById(R.id.toggleButton_with_transcript);

        toggleButton_transcript.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                stopPosition = video_player_view.getCurrentPosition();
                if (isChecked) {
                    //with transcript
                    if(currentVidPath.equals(uri_without_transcript)) {
                        currentVidPath = uri_with_transcript;
                        videoPlay();
                        video_player_view.seekTo(stopPosition);
                        video_player_view.start();
                    }
                } else {
                    //without transcript
                    if(currentVidPath.equals(uri_with_transcript)) {
                        currentVidPath = uri_without_transcript;
                        videoPlay();
                        video_player_view.seekTo(stopPosition);
                        video_player_view.start();
                    }
                }
            }
        });

        //set video to vid1 first
        if(currentVidPath==null) {
            currentVidPath = uri_without_transcript;
        }

        videoPlay();

        if(stopPosition>0) {
            video_player_view.seekTo(stopPosition);
            video_player_view.start();
        }
    }

    @Override
    protected void onResume() {
        Log.d("log", "resumed"+stopPosition);
        super.onResume();
    }

    @Override
    protected void onPause() {
        video_player_view.pause();
        stopPosition = video_player_view.getCurrentPosition(); //stopPosition is an int
        Log.d("log", "paused"+stopPosition);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d("log", "Destroyed");
        super.onDestroy();
        stopPosition = 0;
    }


    public void videoPlay() {
        video_player_view.setVideoURI(currentVidPath);
        video_player_view.setMediaController(vidControl);
    }
}
