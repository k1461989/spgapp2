package com.kcl.spanishgrammarapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.HashMap;

public class SubtopicsActivity extends Activity implements View.OnClickListener {
    public static final String SUBTOPIC = "SUBTOPIC";
    private String topic;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subtopics);

        context = getApplicationContext();

        topic = getIntent().getStringExtra(MainActivity.TOPIC); //This is how we tell what topic we've entered.
        LinearLayout subtopicLayout = (LinearLayout) findViewById(R.id.main_layout_subtopic_id);

        String[] greetings = {"Pronombres", "Ser y Estar", "Llamarse", "Las Letras", "Vocabulario", "Comprensión Auditiva"};
        String[] checkingin = {"Artículo Indeterminado", "Artículo Determinado", "Números", "Nacionalidades", "Vocabulario", "Comprensión Auditiva"};
        String[] sightseeing = {"Preposiciones", "Hay que y Tener que", "Verbo Estar", "Vocabulario", "Comprensión Auditiva"};
        String[] directions = {"Imperativo 1", "Imperativo 2", "Tú o Usted", "Vocabulario", "Comprensión Auditiva"};
        String[] eating = {"Verbos Querer y Pensar", "Verbo Poder", "Verbo Jugar", "Vocabulario", "Comprensión Auditiva"};
        String[] likes = {"El Verbo Gustar", "Los Pronombres", "Gustos", "Vocabulario", "Comprensión Auditiva"};
        String[] planning = {"Futuro con Ir","El Verbo Doler", "Practica la Hora", "Vocabulario", "Comprensión Auditiva"};
        String[] shopping = {"Los Adjetivos", "Objeto Directo", "Quisiera", "Vocabulario", "Comprensión Auditiva"};
        String[] dating = {"Interrogativos", "Pronombres Posesivos","Adjetivos Posesivos", "Vocabulario", "Comprensión Auditiva"};
        HashMap<String, String[]> subtopics = new HashMap<>();
        subtopics.put("Greetings", greetings);
        subtopics.put("Checking-in", checkingin);
        subtopics.put("Sightseeing", sightseeing);
        subtopics.put("Directions", directions);
        subtopics.put("Eating", eating);
        subtopics.put("Likes", likes);
        subtopics.put("Planning", planning);
        subtopics.put("Shopping", shopping);
        subtopics.put("Dating", dating);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(10,20,10,10);

        String[] subtopics_of_topic = subtopics.get(topic);
        for (int i = 0; i < subtopics_of_topic.length; i++) {
            String subtopic = subtopics_of_topic[i];
            ParagonCustomButton button = new ParagonCustomButton(this);
            if(subtopic.contains("Vocabulario")) {
                button.setText("vocabulario");
            } else {
                button.setText(subtopic);
            }
            button.setTag(subtopic);
            if(i == subtopics_of_topic.length-1) {
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,ListeningComprehensionActivity.class);
                        intent.putExtra(ReflectActivity.CURRENT_TOPIC, topic);
                        intent.putExtra(MainActivity.TOPIC, topic);
                        startActivity(intent);
                    }
                });
            } else {
                button.setOnClickListener(this);
            }
            subtopicLayout.addView(button, layoutParams);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, ExercisesActivity.class); //create a new intent
        //The identifier is created differently here than in the rest of the app. It's constructed
        //from it's primary components.
        intent.putExtra("IDENTIFIER", topic+"/"+v.getTag());
        intent.putExtra(SubtopicsActivity.SUBTOPIC, ((Button)v).getText().toString());
        intent.putExtra(MainActivity.TOPIC, topic);
        startActivity(intent); //start the activity
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(MainActivity.TOPIC, topic);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_subtopics, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
       /*
        * .Greetings: Pronombres, Ser y Estar, Llamarse, Las Letras, Vocabulario, Comprensión Auditiva
        * .Checking-in: Artículo Indeterminado, Artículo Determinado, Números, Nacionalidades, Vocabulario, Comprensión Auditiva
        * .Sightseeing: Preposiciones, Hay que y Tener que, Verbo Estar, Vocabulario, Comprensión Auditiva
        * .Directions: Imperativo 1, Imperativo 2, Tú o Usted, Vocabulario, Comprensión Auditiva
        * .Eating: Verbos Querer y Pensar, Verbo Poder, Verbo Jugar, Vocabulario, Comprensión Auditiva
        * .Likes: El Verbo Gustar, Los Pronombres, Gustos, Vocabulario, Comprensión Auditiva
        * .Planning: Futuro con Ir, El Verbo Doler, Practica la Hora ,Vocabulario, Comprensión Auditiva
        * Shopping: Los Adjetivos, Objeto Directo, Quisiera ,Vocabulario, Comprensión Auditiva
        * Dating: Interrogativos, Pronombres Posesivos, Adjetivos Posesivos ,Vocabulario, Comprensión Auditiva
        * */
}
