package com.kcl.spanishgrammarapp;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Messenger;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.vending.expansion.downloader.DownloadProgressInfo;
import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;
import com.google.android.vending.expansion.downloader.DownloaderServiceMarshaller;
import com.google.android.vending.expansion.downloader.Helpers;
import com.google.android.vending.expansion.downloader.IDownloaderClient;
import com.google.android.vending.expansion.downloader.IDownloaderService;
import com.google.android.vending.expansion.downloader.IStub;
import com.kcl.spanishgrammarapp.downloader.Downloader;

import java.util.ArrayList;

public class TopicsActivity extends Activity implements IDownloaderClient {
    private ArrayList<View> icons;
    private GridLayout mainLayout; //this layout is accessed in several places
    private IStub mDownloaderClientStub;
    private IDownloaderService mRemoteService; //needed for download progress
    private boolean mStatePaused;

    /**Download ui elements*/
    ProgressBar progressBar;
    TextView progressPercent;
    TextView averageSpeed;
    TextView timeRemaining;
    Button pauseDLButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /** This code embedded in the if statement is necessary as part of a check if Play Store has
         * downloaded the APK Expansion files automatically. Sometimes this does not happen, and all this
         * code along with the downloader package are necessary to account for those rare cases.*/
        if(!checkExpansionFiles()){
            Intent notifierIntent = new Intent(this, TopicsActivity.this.getClass());
            notifierIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                    Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(TopicsActivity.this,
                    0, notifierIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            // Start the download service (if required)
            try {
                int startResult =
                        DownloaderClientMarshaller.startDownloadServiceIfRequired(this,
                                pendingIntent, Downloader.class);
                // If download has started, initialize this activity to show
                // download progress
                if (startResult != DownloaderClientMarshaller.NO_DOWNLOAD_REQUIRED) {
                    // Instantiate a member instance of IStub
                    mDownloaderClientStub = DownloaderClientMarshaller.CreateStub(this,
                            Downloader.class);
                    // Inflate layout that shows download progress
                    setContentView(R.layout.downloader_ui);
                    initialiseDownloadUI(); //initialise the UI
                    return;
                }// If the download wasn't necessary, fall through to start the app

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        startApp();
    }

    /**
     * Does what one might expect it to do. It opens up the main screen of the app, which is the Topics screen*/
    private void startApp(){
        setContentView(R.layout.activity_learn); //don't move this pls, it's initialised in createGUI()
        mainLayout = (GridLayout) findViewById(R.id.gl_learn);
        createGUI(Configuration.ORIENTATION_PORTRAIT); //expansion file ready
    }

    /**
     * This method checks that the Main APK Expansion file has been delivered.
     * It is important that this method is executed in case Play Store does not
     * automatically download the APK Expansion file itself.*/
    private boolean checkExpansionFiles() {
        String filename = Helpers.getExpansionAPKFileName(this, true, 3); //main file, version 2
        return Helpers.doesFileExist(this, filename, 129916851L, false); //length of file 129,916,851 Bytes
         /*  PLEASE NOTE: if you switch build type from release to debug, it is possible that the obb directory will be removed
             and the APK expansion file along with it. This may not instantly show up in Windows Explorer! It will look like
             the file is there, but it's not.*/
    }

    private void initialiseDownloadUI(){
        progressBar = (ProgressBar) findViewById(R.id.progressBarDL);
        progressPercent = (TextView) findViewById(R.id.textProgressPercent);
        averageSpeed = (TextView) findViewById(R.id.textAverageSpeed);
        timeRemaining = (TextView) findViewById(R.id.textTimeRemaining);
        pauseDLButton = (Button) findViewById(R.id.pauseDLButton);

        pauseDLButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mStatePaused) {
                    mRemoteService.requestContinueDownload();
                } else {
                    mRemoteService.requestPauseDownload();
                }
                setButtonPausedState(!mStatePaused);
            }
        });
    }

    private void setButtonPausedState(boolean paused) {
        mStatePaused = paused;
        int stringResourceID = paused ? R.string.text_button_resume :
                R.string.text_button_pause;
        pauseDLButton.setText(stringResourceID);
    }

    /**This method creates the GUI for this activity*/
    private void createGUI(int orientation){
        /*A remark on how the icons work:
        * There is an ArrayList of all the topic icons.
        * As we create the icons, we add a tag with the name of the Topic of that button.
        * We then set the listener for that button. The listener retrieves the tag of the button.
        * The tag is used to open the correct exercise.*/
        icons = new ArrayList<>(); //create and populate this ArrayList because there are 2 ways these icons can appear, this makes the code more efficient.
        String[] topics = {"Greetings", "Checking-in", "Sightseeing",
                "Directions", "Eating", "Likes",
                "Planning", "Shopping", "Dating"
        };
        int[] imgs = {R.drawable.greetings, R.drawable.checkin, R.drawable.sight,
                R.drawable.directions, R.drawable.eating, R.drawable.likes,
                R.drawable.planning, R.drawable.shopping, R.drawable.dating,
                R.drawable.recording
        };

        for (int i = 0; i < topics.length; i++) {
            View iconView =
                    null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                iconView = getLayoutInflater().inflate(R.layout.icon, mainLayout, false);
            } else {
                iconView = getLayoutInflater().inflate(R.layout.icon, null);
            }
            mainLayout.addView(iconView);

            ImageView ivIcon = (ImageView) iconView.findViewById(R.id.iv_icon);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ivIcon.setImageDrawable(getDrawable(imgs[i]));
            } else {
                ivIcon.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), imgs[i]));
            }

            TextView tvIcon = (TextView) iconView.findViewById(R.id.tv_icon);
            tvIcon.setText((i+1)+". "+topics[i]);
            if(i==2){tvIcon.setText(" 3. "+topics[i]);} //hacky fix for the "3. Sightseeing" being too close to "2. Checking-in"
            tvIcon.setTypeface(Typeface.createFromAsset(getAssets(), "arial_bold.ttf"));

            iconView.setTag(topics[i]);
            iconView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TopicsActivity.this, MainActivity.class);
                    intent.putExtra(MainActivity.TOPIC, v.getTag().toString());
                    startActivity(intent);
                }
            });
            icons.add(iconView);
        }
        setDefaultImageButtonSizes(orientation);

        //set small buttons
        ParagonCustomButton btnResources = (ParagonCustomButton) findViewById(R.id.btn_resources);
        btnResources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TopicsActivity.this, ResourcesActivity.class);
                startActivity(intent);
            }
        });

        ParagonCustomButton btnGlossary = (ParagonCustomButton) findViewById(R.id.btn_glossary);
        btnGlossary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TopicsActivity.this, GlossaryActivity.class);
                startActivity(intent);
            }
        });

        ParagonCustomButton btnExtra = (ParagonCustomButton) findViewById(R.id.btn_extra);
        btnExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TopicsActivity.this, ExtraActivity.class));
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mainLayout =  new GridLayout(this);
        setContentView(mainLayout);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            System.out.println("ORIENTATION_LANDSCAPE");
            createGUI(Configuration.ORIENTATION_LANDSCAPE);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            System.out.println("ORIENTATION_PORTRAIT");
            createGUI(Configuration.ORIENTATION_PORTRAIT);
        }
        setDefaultImageButtonSizes(newConfig.orientation);
    }

    /**
     * Initially the app design was made to accommodate any language, any difficulty level, and
     * any number of topics. This method would have been used to make sure that a different number
     * of topics would be displayed correctly. It was also used to adjust button sizes when the orientation
     * was changed, however this has since been scrapped. The method is used only in a basic sense
     * of sizing the buttons, but could be removed and the buttons could be created in the XML file,
     * since now we know this app will only host 1 language nad 1 level.*/
    private void setDefaultImageButtonSizes(int orientation) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);

        int screenWidth;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            screenWidth = displayMetrics.widthPixels;
        } else {
            screenWidth = displayMetrics.heightPixels;
        }
        int idealSize = (screenWidth-64)/3;
        for(View icon : icons){
            ViewGroup.LayoutParams params = icon.findViewById(R.id.iv_icon).getLayoutParams();
            params.width = idealSize;
            params.height = idealSize;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_learn, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        if (null != mDownloaderClientStub) {
            mDownloaderClientStub.connect(this);
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        if (null != mDownloaderClientStub) {
            mDownloaderClientStub.disconnect(this);
        }
        super.onStop();
    }

    /*These methods are required for the IDownloaderClient interface.*/
    /**This method is required to connect to an instance of DownloaderService (Downloader class extends this)
     * and allows sending requests such as pause or resume for the download. This is allowed through
     * the creation of the proxy (createProxy()).*/
    @Override
    public void onServiceConnected(Messenger m) {
        mRemoteService = DownloaderServiceMarshaller.CreateProxy(m);
        mRemoteService.onClientUpdated(mDownloaderClientStub.getMessenger());
    }

    /**The download service calls this when a change in download state occurs,
     * such as the download begins or completes. */
    @Override
    public void onDownloadStateChanged(int newState) {
        boolean paused;
        switch (newState) {
            case IDownloaderClient.STATE_IDLE:
                // STATE_IDLE means the service is listening, so it's
                // safe to start making calls via mRemoteService.
            case IDownloaderClient.STATE_CONNECTING:
            case IDownloaderClient.STATE_FETCHING_URL:
            case IDownloaderClient.STATE_DOWNLOADING:
                paused = false;
                break;

            case IDownloaderClient.STATE_FAILED_CANCELED:
            case IDownloaderClient.STATE_FAILED:
            case IDownloaderClient.STATE_FAILED_FETCHING_URL:
            case IDownloaderClient.STATE_FAILED_UNLICENSED:
            case IDownloaderClient.STATE_PAUSED_NEED_CELLULAR_PERMISSION:
            case IDownloaderClient.STATE_PAUSED_WIFI_DISABLED_NEED_CELLULAR_PERMISSION:
            case IDownloaderClient.STATE_PAUSED_BY_REQUEST:
            case IDownloaderClient.STATE_PAUSED_ROAMING:
            case IDownloaderClient.STATE_PAUSED_SDCARD_UNAVAILABLE:
                paused = true;
                break;

            case IDownloaderClient.STATE_COMPLETED:
                paused = false;
                startApp();
                break;
            default:
                paused = true;
        }
        setButtonPausedState(paused);
    }

    @Override
    public void onDownloadProgress(DownloadProgressInfo progress) {
        averageSpeed.setText(getString(R.string.kilobytes_per_second,
                Helpers.getSpeedString(progress.mCurrentSpeed)));
        timeRemaining.setText(getString(R.string.time_remaining,
                Helpers.getTimeRemaining(progress.mTimeRemaining)));

        progressBar.setMax((int) (progress.mOverallTotal >> 8));
        progressBar.setProgress((int) (progress.mOverallProgress >> 8));
        String progressPercentString = (Long.toString(progress.mOverallProgress
                * 100 /
                progress.mOverallTotal) + "%");
        progressPercent.setText(progressPercentString);
    }

    private long backPressed;

    @Override
    public void onBackPressed() {
        int time_interval = 2000;
        if(backPressed + time_interval > System.currentTimeMillis()){
            this.finishAffinity();
        }else{
            Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show();
            backPressed = System.currentTimeMillis();
        }
    }
}
