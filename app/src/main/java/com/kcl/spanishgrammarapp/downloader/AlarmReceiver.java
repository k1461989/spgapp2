package com.kcl.spanishgrammarapp.downloader;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;

/**
 * Class necessary to facilitate the downloads of APK extension files in case
 * Google Play does not download them automatically along with the app itself.
 */

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            DownloaderClientMarshaller.startDownloadServiceIfRequired(context,
                    intent, Downloader.class);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }
}
