package com.kcl.spanishgrammarapp.downloader;

import com.google.android.vending.expansion.downloader.impl.DownloaderService;
import com.kcl.spanishgrammarapp.downloader.AlarmReceiver;

/**
 * Class necessary to facilitate the downloads of APK extension files in case
 * Google Play does not download them automatically along with the app itself.
 */

public class Downloader extends DownloaderService {

    public static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkAYChgVyGaUWwemcR9UaOwbbtO23vPeOjgPOAYNLib4SjpdHfCNLss/XS6UKLfiivx0q1Yx3C8DmwEPvqyGrHFo3MUkGZb5lBxvrTkLqXD9ssqRj6VXvqQfnBqVPZD9B30LUwc/qWu3j4NAT9ixKilIVMUP+6wa0rPUF4E4MoQVvXzqqf0On8rCNtWRaj6i82AD+YqfP3p/A4uzBRt2eHFyfqra7ictfCW2gsDNHAcoAVG4Y+hU0VtPqwc5IMM8fNE+VGkT3tF1My9S75iW+2NnB1vPlFa+OptDGV5jRgSIUlbOgmfOEB4IqLCT40NfxmO6U97jskSeoP3wkWD7nIQIDAQAB";

    public static final byte[] SALT = new byte[] { 3, 42, -17, -1, 56, 58,
            -100, -41, 66, 2, 8, -9, 0, 28, -123, 108, -33, -45, -1, 127
    };

    @Override
    public String getPublicKey() {
        return BASE64_PUBLIC_KEY;
    }

    @Override
    public byte[] getSALT() {
        return SALT;
    }

    @Override
    public String getAlarmReceiverClassName() {
        return AlarmReceiver.class.getName();
    }
}
