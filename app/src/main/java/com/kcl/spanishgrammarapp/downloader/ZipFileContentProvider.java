package com.kcl.spanishgrammarapp.downloader;

import com.android.vending.expansion.zipfile.APEZProvider;

/**
 * This class is used to help access media files from a zip (APK expansion file).
 * Since we can only upload 2 APK Expansion files, a zip has to be used for the
 * numerous video files included in it.
 */

public class ZipFileContentProvider extends APEZProvider {
    @Override
    public String getAuthority() {
        return "com.kcl.spanishgrammarapp.downloader.ZipFileContentProvider";
    }
}
