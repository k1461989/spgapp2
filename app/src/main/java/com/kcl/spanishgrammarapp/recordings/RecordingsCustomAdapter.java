package com.kcl.spanishgrammarapp.recordings;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by janaldoustorres on 19/12/2016.
 */
public class RecordingsCustomAdapter extends FragmentPagerAdapter {
    private String fragments [] = {"A","N", "D", "F", "M", "T"};

    public RecordingsCustomAdapter(FragmentManager supportFragmentManager, Context applicationContext) {
        super(supportFragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        RecordingsFragment recordingsFragment = new RecordingsFragment();
        recordingsFragment.setPageNumber(position);

        return recordingsFragment;
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragments[position];
    }
}
