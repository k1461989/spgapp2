package com.kcl.spanishgrammarapp.recordings;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.kcl.spanishgrammarapp.CMS.CMS;
import com.kcl.spanishgrammarapp.R;
import com.kcl.spanishgrammarapp.RecordingActivity;
import com.kcl.spanishgrammarapp.resources.data.Day;
import com.kcl.spanishgrammarapp.resources.data.Holiday;
import com.kcl.spanishgrammarapp.resources.data.Letter;
import com.kcl.spanishgrammarapp.resources.data.Number;
import com.kcl.spanishgrammarapp.resources.data.Region;
import com.kcl.spanishgrammarapp.resources.data.Season;
import com.kcl.spanishgrammarapp.resources.data.Time;

import java.util.ArrayList;

/**
 * Created by janaldoustorres on 19/12/2016.
 */
public class RecordingsFragment extends Fragment {
    private ListView savedRecsLV;
    private ArrayList<String> savedRecsList = new ArrayList<>();
    private ArrayAdapter savedRecsAdapter;
    private int pageNumber;
    private static int pageSelected;
    private TextView tvSelected;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.frag1, container, false);

        savedRecsLV = (ListView) rootView.findViewById(R.id.lv_recordings_audio);
        //Log.d("lv", savedRecsLV.toString());
        savedRecsAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1, savedRecsList);
        savedRecsLV.setAdapter(savedRecsAdapter);
        savedRecsLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("pagesleected", pageSelected+"");
                Log.d("position", position+"");

                //highlight item
                if (tvSelected!=null){
                    tvSelected.setBackgroundColor(Color.WHITE);
                }
                tvSelected = (TextView) view;
                view.setBackgroundColor(Color.GRAY);

                int audio = 0;
                if(pageSelected==0) {
                    //alphabet
                    Log.d("in al", pageSelected+"");
                    audio = CMS.getLetters().get(position).getLetterAudioSP();
                } else if (pageSelected==1) {
                    //numbers
                    Log.d("in numbs", "");
                    audio = CMS.getAllNumbers().get(position).getNumbAudioSP();
                } else if (pageSelected==2) {
                    //days
                    audio = CMS.getDays().get(position).getDayAudioSP();
                } else if (pageSelected==3) {
                    //festivals
                    audio = getResources().getIdentifier(
                            CMS.getHolidays(getContext(), Region.SPAIN).get(position).getHolidayAudio(),
                            "raw", "com.kcl.spanishgrammarapp");
                } else if (pageSelected==4) {
                    //months
                    audio = getResources().getIdentifier(
                            CMS.getAllMonths(getContext()).get(position).getAudioFilenameSP(),
                            "raw", "com.kcl.spanishgrammarapp");
                } else if (pageSelected==5) {
                    //time
                    audio = CMS.getTimes(getContext()).get(position).getTimeAudio();
                }
                ((RecordingActivity) getActivity()).clickedRecording(audio);
            }
        });
        updateSavedAudioFiles();

        return rootView;
    }

    public void updateSavedAudioFiles(){
        savedRecsList.clear();

        Log.d("page number", pageNumber+"");

        switch (pageNumber) {
            case 0: //alphabet
                ArrayList<Letter> letters = CMS.getLetters();
                for(Letter letter: letters) {
                    savedRecsList.add(letter.getLetter() + " (" + letter.getPronunciation() + ")");
                }
                return;
            case 1: //numbers
                ArrayList<Number> numbers = CMS.getAllNumbers();
                for(Number number: numbers) {
                    savedRecsList.add(number.getNumber() + " (" + number.getPronunciation() + ")");
                }
                return;
            case 2: //days
                ArrayList<Day> days = CMS.getDays();
                for(Day day: days) {
                    savedRecsList.add(day.getDay());
                }
                return;
            case 3: //holidays
                ArrayList<Holiday> holidays = CMS.getHolidays(getContext(), Region.SPAIN);
                for(Holiday holiday: holidays) {
                    savedRecsList.add(holiday.getDateSpa() + " - " + holiday.getName_InLanguage());
                }
                return;
            case 4: //seasons, months
                ArrayList<Season.Month> months = CMS.getAllMonths(getContext());
                for(Season.Month month: months) {
                    savedRecsList.add(month.getName());
                }
                return;
            case 5: //time
                ArrayList<Time> times = CMS.getTimes(getContext());
                for(Time time: times) {
                    savedRecsList.add(time.getTime_language() + " (" + time.getTime_digital() + ")");
                }
                return;
        }
    }

    public void setPageNumber(int num) {
        pageNumber = num;
    }

    public static void setPageSelected(int ps) {
        pageSelected = ps;
    }
}
