package com.kcl.spanishgrammarapp.resources.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;

import com.kcl.spanishgrammarapp.CMS.CMS;
import com.kcl.spanishgrammarapp.R;
import com.kcl.spanishgrammarapp.resources.activities.alphabet.LetterAdapter;
import com.kcl.spanishgrammarapp.resources.data.Letter;

import java.util.ArrayList;

/**
 * Alphabet of chosen language, or Numbers or Days is created and shown here depending on the String
 * Extra. Letters are dynamically created and can be clicked to listen to the pronunciation.
 */
public class AlphabetActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alphabet);

        ArrayList<Letter> letters = CMS.getLetters();

        GridView gridView = (GridView) findViewById(R.id.gridView1);
        gridView.setAdapter(new LetterAdapter(this, letters));

        ImageButton ib = (ImageButton) findViewById(R.id.iv_instructions);
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(AlphabetActivity.this);
                alert.setTitle("Instructions");
                alert.setMessage("Tap an item and then listen to the recording. \n" +
                        "\nCompare your pronounciation to the recordings with the Record tool.");
                alert.setCancelable(true);
                alert.create();
                alert.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_resources, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_instructions:
                final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Instructions");
                alert.setMessage("Tap an item and then listen to the recording.");
                alert.setCancelable(true);
                alert.create();
                alert.show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
