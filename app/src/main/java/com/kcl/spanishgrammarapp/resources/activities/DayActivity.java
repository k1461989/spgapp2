package com.kcl.spanishgrammarapp.resources.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kcl.spanishgrammarapp.CMS.CMS;
import com.kcl.spanishgrammarapp.R;
import com.kcl.spanishgrammarapp.ResourcesActivity;
import com.kcl.spanishgrammarapp.resources.data.Day;

import java.util.ArrayList;

/**
 * Alphabet of chosen language, or Numbers or Days is created and shown here depending on the String
 * Extra. Letters are dynamically created and can be clicked to listen to the pronunciation.
 */
public class DayActivity extends Activity {
    private int curColorIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day);

        //colors array
        int colors[] = {
                ContextCompat.getColor(this, R.color.pacific_blue),
                ContextCompat.getColor(this, R.color.yellow_green),
                ContextCompat.getColor(this, R.color.meat_brown),
                ContextCompat.getColor(this, R.color.tigers_eye)
        };

        //get data
        ArrayList<Day> days = CMS.getDays();

        //set title
        TextView tvTitle = (TextView) findViewById(R.id.tv_topic);
        tvTitle.setText("Days of the week");

        //set list
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.ll_number);

        CardView cardView;
        for(final Day day: days) {
            cardView =
                    (CardView) getLayoutInflater().inflate(R.layout.cardview_numbers, mainLayout,false);
            mainLayout.addView(cardView);

            //set background color
            cardView.setCardBackgroundColor(colors[curColorIndex]);

            //set text
            TextView tvSpa = (TextView)cardView.findViewById(R.id.number_eng);
            tvSpa.setText(day.getDay());
            TextView tvEng = (TextView)cardView.findViewById(R.id.number_spa);
            tvEng.setText(day.getPronunciation());

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MediaPlayer player;
                    if(ResourcesActivity.DIALECT.equals("Spanish")) {
                        player = MediaPlayer.create(DayActivity.this, day.getDayAudioSP());
                    }else{
                        player = MediaPlayer.create(DayActivity.this, day.getDayAudioMX());
                    }
                    player.start();
                }
            });

            curColorIndex++;
            if (curColorIndex == colors.length) {
                curColorIndex = 0;
            }
        }

        ImageButton ib = (ImageButton) findViewById(R.id.iv_instructions);
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(DayActivity.this);
                alert.setTitle("Instructions");
                alert.setMessage("Tap an item and then listen to the recording. \n" +
                        "\nCompare your pronounciation to the recordings with the Record tool.");
                alert.setCancelable(true);
                alert.create();
                alert.show();
            }
        });
    }
}

