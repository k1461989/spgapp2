package com.kcl.spanishgrammarapp.resources.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kcl.spanishgrammarapp.CMS.CMS;
import com.kcl.spanishgrammarapp.R;
import com.kcl.spanishgrammarapp.ResourcesActivity;
import com.kcl.spanishgrammarapp.resources.data.Holiday;
import com.kcl.spanishgrammarapp.resources.data.Region;

import java.util.ArrayList;

/**
 * Created by Jat, later developed by Oskar for adding audio.
 */
public class HolidaysActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holidays);

        Region region = ResourcesActivity.DIALECT;

        //get data
        ArrayList<Holiday> holidays = CMS.getHolidays(getApplicationContext(), region);

        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.ll_holidays);

        View cardView;
        for(final Holiday holiday: holidays) {
            cardView =
                    getLayoutInflater().inflate(R.layout.cardview_holiday, mainLayout,false);
            mainLayout.addView(cardView);

            TextView tvDate = (TextView)cardView.findViewById(R.id.tv_holiday_date);
            tvDate.setText(holiday.getDateSpa());

            TextView lEng = (TextView)cardView.findViewById(R.id.holiday_eng);
            lEng.setText(holiday.getDateEng()+" - "+holiday.getName_English());

            TextView lSpa = (TextView)cardView.findViewById(R.id.holiday_spa);
            lSpa.setText(holiday.getName_InLanguage());

            ImageView ivHoliday = (ImageView) cardView.findViewById(R.id.iv_holiday);
            Uri imgUri = Uri.parse("android.resource://"+getPackageName()+"/drawable/"+holiday.getImageFilename());
            ivHoliday.setImageURI(imgUri);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MediaPlayer player;
                    Uri audioUri = Uri.parse("android.resource://"+getPackageName()+"/raw/"+holiday.getHolidayAudio());
                    Log.d("AUDIO URI:", audioUri.toString());
                    player = MediaPlayer.create(HolidaysActivity.this, audioUri);
                    player.start();
                }
            });
        }

        ImageButton ib = (ImageButton) findViewById(R.id.iv_instructions);
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(HolidaysActivity.this);
                alert.setTitle("Instructions");
                alert.setMessage("Tap an item and then listen to the recording. \n" +
                        "\nCompare your pronounciation to the recordings with the Record tool.");
                alert.setCancelable(true);
                alert.create();
                alert.show();
            }
        });
    }
}