package com.kcl.spanishgrammarapp.resources.activities;

/**
 * Created by janaldoustorres on 02/12/2016.
 * from http://android-coding.blogspot.co.uk/2014/03/implement-sliding-page-using-viewpager.html
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import com.kcl.spanishgrammarapp.R;
import com.kcl.spanishgrammarapp.resources.activities.numbers.NumbersGridFragment;
import com.kcl.spanishgrammarapp.resources.activities.numbers.NumbersListFragment;

public class NumbersActivity extends FragmentActivity {

    private static final int NUM_PAGES = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers);

        ViewPager mPager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter mPagerAdapter = new MyFragmentStatePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        Toast.makeText(this, "Tap a number to hear it, then repeat it",
                Toast.LENGTH_LONG).show();
    }

    private class MyFragmentStatePagerAdapter extends FragmentStatePagerAdapter {
        public MyFragmentStatePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment tmpFragment;
            if(position==0){
                tmpFragment = new NumbersGridFragment();
                ((NumbersGridFragment)tmpFragment).setPageNumber(position);
            }else{
                tmpFragment = new NumbersListFragment();
                ((NumbersListFragment)tmpFragment).setPageNumber(position);
            }
            return tmpFragment;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}
