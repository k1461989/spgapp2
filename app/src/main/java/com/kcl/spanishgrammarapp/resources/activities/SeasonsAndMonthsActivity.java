package com.kcl.spanishgrammarapp.resources.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageButton;

import com.kcl.spanishgrammarapp.CMS.CMS;
import com.kcl.spanishgrammarapp.R;
import com.kcl.spanishgrammarapp.resources.activities.seasons.SeasonsExpandableListAdapter;
import com.kcl.spanishgrammarapp.resources.data.Season;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by janaldoustorres on 18/03/2016.
 */
public class SeasonsAndMonthsActivity extends Activity {
    private ArrayList<String> listDataHeader;
    private HashMap<String, List<Season.Month>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seasons_and_months);

        // preparing list data
        prepareSeasonsListData();

        //Set up expandable list view
        ExpandableListView expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        SeasonsExpandableListAdapter listAdapter = new SeasonsExpandableListAdapter(this, listDataHeader, listDataChild, getIntent());
        expandableListView.setAdapter(listAdapter);

        ImageButton ib = (ImageButton) findViewById(R.id.iv_instructions);
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(SeasonsAndMonthsActivity.this);
                alert.setTitle("Instructions");
                alert.setMessage("Tap an item and then listen to the recording. \n" +
                        "\nCompare your pronounciation to the recordings with the Record tool.");
                alert.setCancelable(true);
                alert.create();
                alert.show();
            }
        });
    }

    private void prepareSeasonsListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<String, List<Season.Month>>();

        // Adding header data
        ArrayList<Season> seasons = CMS.getSeasonsAndMonths(getApplicationContext());

        // Adding child data
        for(Season season: seasons) {
            String name = season.getSeason();
            listDataHeader.add(name);
            listDataChild.put(name, season.getMonths());
        }
    }
}
