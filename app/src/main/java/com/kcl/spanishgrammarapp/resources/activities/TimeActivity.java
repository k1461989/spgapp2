package com.kcl.spanishgrammarapp.resources.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kcl.spanishgrammarapp.CMS.CMS;
import com.kcl.spanishgrammarapp.R;
import com.kcl.spanishgrammarapp.resources.data.Time;

import java.util.ArrayList;

/**
 * Created by janaldoustorres on 02/12/2016.
 */

public class TimeActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //get data
        ArrayList<Time> times = CMS.getTimes(getApplicationContext());

        //set content
        setContentView(R.layout.activity_holidays);

        TextView textView = (TextView) findViewById(R.id.tv_topic);
        textView.setText("¿Qué hora es?");

        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.ll_holidays);

        View cardView;
        for(final Time time: times) {
            cardView =
                    getLayoutInflater().inflate(R.layout.cardview_holiday, mainLayout,false);
            mainLayout.addView(cardView);
            TextView tvDigital = (TextView)cardView.findViewById(R.id.tv_holiday_date);
            tvDigital.setText(time.getTime_digital());
            TextView tvSpa = (TextView)cardView.findViewById(R.id.holiday_spa);
            tvSpa.setText(time.getTime_language());
            TextView tv = (TextView)cardView.findViewById(R.id.holiday_eng);
            tv.setText("");
            ImageView ivHoliday = (ImageView) cardView.findViewById(R.id.iv_holiday);
            Uri imgUri = Uri.parse("android.resource://"+getPackageName()+"/drawable/"+time.getClockPicName());
            ivHoliday.setImageURI(imgUri);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MediaPlayer player;
                    player = MediaPlayer.create(TimeActivity.this, time.getTimeAudio());
                    player.start();
                }
            });
        }

        ImageButton ib = (ImageButton) findViewById(R.id.iv_instructions);
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(TimeActivity.this);
                alert.setTitle("Instructions");
                alert.setMessage("Tap an item and then listen to the recording. \n\nCompare your pronounciation to the recordings with the Record tool.");
                alert.setCancelable(true);
                alert.create();
                alert.show();
            }
        });
    }
}