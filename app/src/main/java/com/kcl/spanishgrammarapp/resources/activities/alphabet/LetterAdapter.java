package com.kcl.spanishgrammarapp.resources.activities.alphabet;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kcl.spanishgrammarapp.R;
import com.kcl.spanishgrammarapp.ResourcesActivity;
import com.kcl.spanishgrammarapp.resources.data.Letter;
import com.kcl.spanishgrammarapp.resources.data.Region;

import java.util.ArrayList;


public class LetterAdapter extends BaseAdapter {
    private Context context;
    private final ArrayList<Letter> mobileValues;

    public LetterAdapter(Context context, ArrayList<Letter> mobileValues) {
        this.context = context;
        this.mobileValues = mobileValues;
    }

    public View getView(int position, final View convertView, ViewGroup parent) {
        //colors array
        int colors[] = {
                ContextCompat.getColor(context, R.color.pacific_blue),
                ContextCompat.getColor(context, R.color.yellow_green),
                ContextCompat.getColor(context, R.color.meat_brown),
                ContextCompat.getColor(context, R.color.tigers_eye)
        };

        View gridView;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            gridView = inflater.inflate(R.layout.letter_item, null);
            gridView.setBackgroundColor(colors[position%3]);
        } else {
            gridView = convertView;
        }

        final Letter letter = mobileValues.get(position);

        // set value into Word
        TextView textView = (TextView) gridView
                .findViewById(R.id.grid_item_letter);
        textView.setText(letter.getLetter().toUpperCase());

        // set value into Another word
        TextView textView2 = (TextView) gridView
                .findViewById(R.id.grid_item_another_word);
        textView2.setText(letter.getPronunciation());

        gridView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer player;
                if(ResourcesActivity.DIALECT == Region.SPAIN) {
                    player = MediaPlayer.create(context, letter.getLetterAudioSP());
                }else{
                    player = MediaPlayer.create(context, letter.getLetterAudioMX());
                }
                player.start();
            }
        });

        return gridView;
    }

    @Override
    public int getCount() {
        return mobileValues.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}