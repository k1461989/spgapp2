package com.kcl.spanishgrammarapp.resources.activities.numbers;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kcl.spanishgrammarapp.R;
import com.kcl.spanishgrammarapp.ResourcesActivity;
import com.kcl.spanishgrammarapp.resources.data.Number;
import com.kcl.spanishgrammarapp.resources.data.Region;

import java.util.ArrayList;


public class NumberItemAdapter extends BaseAdapter {
    private Context context;
    private final ArrayList<Number> mobileValues;

    public NumberItemAdapter(Context context, ArrayList<Number> mobileValues) {
        this.context = context;
        this.mobileValues = mobileValues;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        View gridView;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.word_item, null);

            //set random color to background
            switch (position%9) {
                case 0: gridView.setBackgroundColor(Color.parseColor("#4286f4")); break;
                case 1: gridView.setBackgroundColor(Color.parseColor("#8FC93A")); break;
                case 2: gridView.setBackgroundColor(Color.parseColor("#E4CC37")); break;
                case 3: gridView.setBackgroundColor(Color.parseColor("#8FC93A")); break;
                case 4: gridView.setBackgroundColor(Color.parseColor("#E4CC37")); break;
                case 5: gridView.setBackgroundColor(Color.parseColor("#4286f4")); break;
                case 6: gridView.setBackgroundColor(Color.parseColor("#E4CC37")); break;
                case 7: gridView.setBackgroundColor(Color.parseColor("#4286f4")); break;
                case 8: gridView.setBackgroundColor(Color.parseColor("#8FC93A")); break;
                default: gridView.setBackgroundColor(Color.BLACK); break;
            }

        } else {
            gridView = (View) convertView;
        }
        /*Modded this method because it was scrambling the order of the items every time we scrolled.*/
        // set value into Word
        TextView tvWordEng = (TextView) gridView
                .findViewById(R.id.grid_item_word);
        String eng = mobileValues.get(position).getNumber();
        tvWordEng.setText(eng);

        // set value into Another word
        TextView tvWordSpa = (TextView) gridView
                .findViewById(R.id.grid_item_another_word);
        String spa = mobileValues.get(position).getPronunciation();
        tvWordSpa.setText(spa);
        tvWordSpa.setTextSize(20);

        gridView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer player;
                if(ResourcesActivity.DIALECT == Region.SPAIN) {
                    player = MediaPlayer.create(context, mobileValues.get(position).getNumbAudioSP());
                }else{
                    player = MediaPlayer.create(context, mobileValues.get(position).getNumbAudioMX());
                }
                player.start();
            }
        });

        return gridView;
    }

    @Override
    public int getCount() {
        return mobileValues.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}