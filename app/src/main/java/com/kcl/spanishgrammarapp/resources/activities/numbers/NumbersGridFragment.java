package com.kcl.spanishgrammarapp.resources.activities.numbers;

/**
 * Created by janaldoustorres on 02/12/2016.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import com.kcl.spanishgrammarapp.CMS.CMS;
import com.kcl.spanishgrammarapp.R;
import com.kcl.spanishgrammarapp.resources.data.Number;

import java.util.ArrayList;

public class NumbersGridFragment extends Fragment {

    int pageNumber = 0;  //default

    public void setPageNumber(int num) {
        pageNumber = num;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_number_grid, container, false);

        //get data
        final ArrayList<Number> numbers = CMS.get0to40();

        //set page content
        GridView gridView = (GridView) rootView.findViewById(R.id.gridView1);

        gridView.setAdapter(new NumberItemAdapter(getContext(), numbers));

        //toast to indicate current page
        Toast.makeText(getActivity(),
                "Swipe right for more",
                Toast.LENGTH_SHORT).show();

        return rootView;
    }
}