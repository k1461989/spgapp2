package com.kcl.spanishgrammarapp.resources.activities.numbers;

/**
 * Created by janaldoustorres on 02/12/2016.
 * from http://android-coding.blogspot.co.uk/2014/03/implement-sliding-page-using-viewpager.html
 */

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kcl.spanishgrammarapp.CMS.CMS;
import com.kcl.spanishgrammarapp.R;
import com.kcl.spanishgrammarapp.ResourcesActivity;
import com.kcl.spanishgrammarapp.resources.data.Number;
import com.kcl.spanishgrammarapp.resources.data.Region;

import java.util.ArrayList;


public class NumbersListFragment extends Fragment {

    int pageNumber = 0;  //default
    private String title;
    private int curColorIndex = 0;
    //TODO FIXME
    //private Toast toast;

    public void setPageNumber(int num) {
        pageNumber = num;
    }

    private ArrayList<Number> getData() {
        switch (pageNumber) {
            case 1:
                title = "<  40 - 100   >";
                return CMS.get40to100();
            case 2:
                title = "<  100 - 1000  >";
                return CMS.get1hto1k();
            case 3:
                title = "<  1000s  >";
                return CMS.get1000s();
            case 4:
                title = "<  11k - 986k  >";
                return CMS.get10kto986k();
            case 5:
                title = "<  Millions  >";
                return CMS.get1mto1c();
            case 6:
                title = "<  Ordinal Numbers 1-10";
                return CMS.getOrdinalNos();
            default:
                return null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        int colors[] = {
                ContextCompat.getColor(getContext(), R.color.pacific_blue),
                ContextCompat.getColor(getContext(), R.color.yellow_green),
                ContextCompat.getColor(getContext(), R.color.meat_brown),
                ContextCompat.getColor(getContext(), R.color.tigers_eye)
        };

        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_number_list, container, false);

        //get data
        final ArrayList<Number> numbers = getData();

        //set page content
        TextView tvTitle = (TextView) rootView.findViewById(R.id.tv_seasons_title);
        tvTitle.setText(title);

        LinearLayout mainLayout = (LinearLayout) rootView.findViewById(R.id.ll_number);

        CardView cardView;
        for (final Number number : numbers) {
            cardView =
                    (CardView) getActivity().getLayoutInflater().inflate(R.layout.cardview_numbers, mainLayout, false);
            //set background color
            cardView.setCardBackgroundColor(colors[curColorIndex]);
            mainLayout.addView(cardView);
            TextView lEng = (TextView) cardView.findViewById(R.id.number_eng);
            lEng.setText(number.getNumber());
            TextView lSpa = (TextView) cardView.findViewById(R.id.number_spa);
            lSpa.setText(number.getPronunciation());

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MediaPlayer player;
                    if(ResourcesActivity.DIALECT == Region.SPAIN) {
                        player = MediaPlayer.create(getContext(), number.getNumbAudioSP());
                    }else{
                        player = MediaPlayer.create(getContext(), number.getNumbAudioMX());
                    }
                    player.start();
                }
            });

            curColorIndex++;
            if (curColorIndex == colors.length) {
                curColorIndex = 0;
            }
        }
        
        //TODO // FIXME: 18/12/2016 
        //toast to indicate current page
//        toast = Toast.makeText(getActivity(),
//                "Swipe right for more",
//                Toast.LENGTH_SHORT);
        
        return rootView;
    }
}