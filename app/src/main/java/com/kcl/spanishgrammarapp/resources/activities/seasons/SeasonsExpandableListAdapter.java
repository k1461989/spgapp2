package com.kcl.spanishgrammarapp.resources.activities.seasons;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.kcl.spanishgrammarapp.R;
import com.kcl.spanishgrammarapp.ResourcesActivity;
import com.kcl.spanishgrammarapp.resources.data.Region;
import com.kcl.spanishgrammarapp.resources.data.Season;

import java.util.HashMap;
import java.util.List;

public class SeasonsExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<Season.Month>> _listDataChild;
    private int colors[];
    private Intent intent;

    public SeasonsExpandableListAdapter(Context context, List<String> listDataHeader,
                                        HashMap<String, List<Season.Month>> listChildData,
                                        Intent intent) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.intent = intent;

        //colors array
        colors = new int[] {
                ContextCompat.getColor(context, R.color.meat_brown),
                ContextCompat.getColor(context, R.color.yellow_green),
                ContextCompat.getColor(context, R.color.tigers_eye),
                ContextCompat.getColor(context, R.color.pacific_blue)
        };
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final Season.Month child = (Season.Month)getChild(groupPosition, childPosition);
        final String childText = child.getName();

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);

        Log.d("CHILD TEXT", childText);
        Log.d("POSITION", childPosition+"");
        Log.d("TEXT VIEW", convertView.toString());

        txtListChild.setText(childText);

        convertView.setBackgroundColor(colors[groupPosition]);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String filename;
                if(ResourcesActivity.DIALECT== Region.SPAIN) {
                    filename = child.getAudioFilenameSP();
                }else{
                    filename = child.getAudioFilenameMX();
                }
                MediaPlayer player = MediaPlayer.create(_context,
                        _context.getResources().getIdentifier(filename,
                                "raw", _context.getPackageName()));
                player.start();
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setText(headerTitle);

        convertView.setBackgroundColor(colors[groupPosition]);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}