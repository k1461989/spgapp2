package com.kcl.spanishgrammarapp.resources.data;

import com.kcl.spanishgrammarapp.R;

import java.util.ArrayList;

/**This class is used for the DayActivity
 * Initially created and developed by Jat, with tweaks from Oskar for the audio
 * The class is somewhat asymmetric to the previous Resource section activities in that the objects
 * of this class are created within it.*/
public class Day {
    private String day;
    private String pronunciation;
    private int dayAudioSP;
    private int dayAudioMX;

    public Day(String l, String pronunciation, int dayAudioSP, int dayAudioMX) {
        day = l;
        this.pronunciation = pronunciation;
        this.dayAudioSP = dayAudioSP;
        this.dayAudioMX = dayAudioMX;
    }

    public String getDay() {
        return day;
    }

    public static ArrayList<Day> getDays() {
        ArrayList<Day> days = new ArrayList<>();
        days.add(new Day("Lunes", "Monday", R.raw.sp_d01_lunes, R.raw.mx_d01_lunes));
        days.add(new Day("Martes", "Tuesday", R.raw.sp_d02_martes, R.raw.mx_d02_martes));
        days.add(new Day("Miércoles", "Wednesday", R.raw.sp_d03_miercoles, R.raw.mx_d03_miercoles));
        days.add(new Day("Jueves", "Thursday", R.raw.sp_d04_jueves, R.raw.mx_d04_jueves));
        days.add(new Day("Viernes", "Friday", R.raw.sp_d05_viernes, R.raw.mx_d05_viernes));
        days.add(new Day("Sabado", "Saturday", R.raw.sp_d06_sabado, R.raw.mx_d06_sabado));
        days.add(new Day("Domingo", "Sunday", R.raw.sp_d07_domingo, R.raw.mx_d07_domingo));

        return days;
    }

    public int getDayAudioMX() {
        return dayAudioMX;
    }

    public int getDayAudioSP() {
        return dayAudioSP;
    }

    public String getPronunciation(){
        return pronunciation;
    }
}
