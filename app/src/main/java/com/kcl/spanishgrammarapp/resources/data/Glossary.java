package com.kcl.spanishgrammarapp.resources.data;

import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by JAESEOKAN on 20/03/2016.
 * Modified by jat torres on 1/12/2016
 * DATA FOR GLOSSARY
 */
public class Glossary {
    private String letter;
    private String word;
    private String definitions;

    public Glossary (String letter, String word, String definitions) {
        this.letter = letter;
        this.word = word;
        this.definitions = definitions;
    }

    public String getWord() {
        return word;
    }

    public String getDefinitions() {
        return definitions;
    }

    public String getLetter() {
        return letter;
    }

    public static ArrayList<Glossary> getGlossaryFromFile(InputStream inputStream) {
        ArrayList<Glossary> glossary = new ArrayList<>();

        try {
            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                //skip first line
                //bufferedReader.readLine();

                String str, letter = "";
                while((str = bufferedReader.readLine())!=null) {
                    if(str.equals("$")) {
                        //skip next line
                        letter = bufferedReader.readLine();
                    } else {
                        //get word
                        //Log.d("ASDF", str);
                        String l[] = str.split(":");

                        glossary.add(new Glossary(letter, l[0],l[1].substring(1)));
                        //Log.d("GLOSSARY OUTPUT", l[0] + "," + l[1]);
                    }
                }

                inputStream.close();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return glossary;
    }

    public String toString() {
        return word + ": " + definitions;
    }
}
