package com.kcl.spanishgrammarapp.resources.data;

import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by janaldoustorres on 17/03/2016. Later developed by Oskar to add audio. images and dates.
 */
public class Holiday {
    private String name_English;
    private String name_InLanguage;
    private String dateEng;
    private String dateSpa;
    private String imageID;
    private String holidayAudio;

    public Holiday(String nE, String nL, String d, String ds, String i, String holidayAudio) {
        name_English = nE;
        name_InLanguage = nL;
        dateEng = d;
        dateSpa = ds;
        imageID = i;
        this.holidayAudio = holidayAudio;
    }

    public String getHolidayAudio() {
        return holidayAudio;
    }

    public String getName_English() {
        return name_English;
    }

    public String getName_InLanguage() {
        return name_InLanguage;
    }

    public String getDateEng() {
        return dateEng;
    }

    public String getImageFilename() {
        return imageID;
    }

    public String getDateSpa() { return dateSpa; }

    public static ArrayList<Holiday> getHolidays(InputStream inputStream, Region region) {
        //read from file
        ArrayList<Holiday> holidays = new ArrayList<>();
        try {
            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                //skip first line
                bufferedReader.readLine();

                //skip Spanish holidays if mexican
                if(region.equals(Region.MEXICO)) {
                    for (int i = 0; i < 10+2; i++) {
                        bufferedReader.readLine();
                    }
                } else if(region.equals(Region.SPAIN)) {
                    //skip first line
                    bufferedReader.readLine();
                }
                //repeat for each holiday
                for (int i = 0; i < 10; i++) {
                    String str[] = bufferedReader.readLine().split(",");
                    String nameEng = str[0];
                    String nameSpa = str[1];
                    String dateEng = str[2];
                    String dateSpa = str[3];
                    String audioFilename = str[4];
                    //create Holiday obj
                    holidays.add(new Holiday(nameEng, nameSpa, dateEng, dateSpa, audioFilename.substring(0,6), audioFilename));
                }
                inputStream.close();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return holidays;
    }
}
