package com.kcl.spanishgrammarapp.resources.data;

/**
 * Created by janaldoustorres on 17/03/2016.
 * Modified by Oskar Slyk on 26/11/16
 */
public class Letter {
    private String letter;
    private String pronunciation;
    private int letterAudioSP;
    private int letterAudioMX;

    public Letter (String l, String p, int letterAudioSP, int letterAudioMX) {
        letter = l;
        pronunciation = p;
        this.letterAudioSP = letterAudioSP;
        this.letterAudioMX = letterAudioMX;
    }

    public String getLetter() {
        return letter;
    }

    public String getPronunciation() {
        return pronunciation;
    }

    public int getLetterAudioSP(){
        return letterAudioSP;
    }

    public int getLetterAudioMX(){
        return letterAudioMX;
    }
}
