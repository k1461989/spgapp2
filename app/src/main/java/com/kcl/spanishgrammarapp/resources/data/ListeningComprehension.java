package com.kcl.spanishgrammarapp.resources.data;

import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Data structure to hold listening comprehension data
 */
public class ListeningComprehension {
    private ArrayList<String> choices;
    private ArrayList<Integer> cAnswers;

    public ListeningComprehension(InputStream inputStream, int topicNo) {
        choices = new ArrayList<>();
        cAnswers = new ArrayList<>();
        //read file starting from 9xtopicNo
        try {
            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                int startLineNo = (topicNo-1)*9;

                //skip to topic
                for (int i = 0; i < startLineNo; ++i) {
                    String str = bufferedReader.readLine();
                }
                //skip topic heading
                bufferedReader.readLine();
                //read questions
                for (int i = 0; i < 6; ++i) {
                    String str = bufferedReader.readLine();
                    choices.add(str);
                    Log.d("OUTPUT--", str);
                }
                //read 7th question if there is
                String str = bufferedReader.readLine();
                if (!str.equals("-")) {
                    choices.add(str);
                    Log.d("OUTPUT=:", str);
                }
                str = bufferedReader.readLine();
                cAnswers = toArrayList(str);

                inputStream.close();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
    }

    private ArrayList<Integer> toArrayList(String str) {
        String[] ans = str.split(",");
        cAnswers = new ArrayList<Integer>();
        for (int i = 0; i < ans.length; i++) {
            cAnswers.add(Integer.parseInt(ans[i]));
        }
        return cAnswers;
    }

    public ArrayList<String> getChoices() {
        return choices;
    }

    public ArrayList<Integer> getAnswers() {
        return cAnswers;
    }
}
