package com.kcl.spanishgrammarapp.resources.data;

/**
 * Created by janaldoustorres on 17/03/2016.
 */
public class Number {
    private String numb;
    private String pronunciation;
    private int numbAudioSP;
    private int numbAudioMX;

    public Number(String l, String p, int numbAudioSP, int numbAudioMX) {
        numb = l;
        pronunciation = p;
        this.numbAudioSP = numbAudioSP;
        this.numbAudioMX = numbAudioMX;
    }

    public String getNumber() {return numb;}

    public String getPronunciation() {
        return pronunciation;
    }

    public int getNumbAudioSP() {
        return numbAudioSP;
    }

    public int getNumbAudioMX() {
        return numbAudioMX;
    }
}
