package com.kcl.spanishgrammarapp.resources.data;

/**
 * Created by janaldoustorres on 18/12/2016.
 */

public enum Region {
    SPAIN, MEXICO;

    public static Region toRegion(String r) {
        switch (r) {
            case "Spanish": return SPAIN;
            case "Mexican": return MEXICO;
            default: return null;
        }
    }
}
