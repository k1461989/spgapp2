package com.kcl.spanishgrammarapp.resources.data;

import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by janaldoustorres on 18/03/2016.
 */
public class Season {
    private String season;
    private Month month1, month2, month3;
    private String image;

    public Season(String n, Month m1, Month m2, Month m3, String img) {
        season = n;
        month1 = m1;
        month2 = m2;
        month3 = m3;
        image = img;
    }

    public String getSeason() {
        return season;
    }

    public ArrayList<Month> getMonths() {
        ArrayList<Month> months = new ArrayList();
        months.add(month1);
        months.add(month2);
        months.add(month3);

        return months;
    }


    public String getImage() {
        return image;
    }

    public static ArrayList<Season> getSeasons(InputStream inputStream) {
        ArrayList<Season> seasons = new ArrayList<>();

        try {
            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                //repeat for each season
                for (int i = 0; i < 4; i++) {
                    //get season
                    String seasonStr = bufferedReader.readLine();
                    //get 3 months of the season
                    String m1[] = bufferedReader.readLine().split(",");
                    String m2[] = bufferedReader.readLine().split(",");
                    String m3[] = bufferedReader.readLine().split(",");
                    Season.Month month1 = new Season.Month(m1[1], "m"+m1[0]+"_"+m1[1]);
                    Season.Month month2 = new Season.Month(m2[1], "m"+m2[0]+"_"+m2[1]);
                    Season.Month month3 = new Season.Month(m3[1], "m"+m3[0]+"_"+m3[1]);
                    //create Season obj
                    seasons.add(new Season(seasonStr, month1, month2, month3, "season"+i));
                }

                inputStream.close();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return seasons;
    }

    public static class Month {
        String name;
        String audioFilename;

        public Month(String name, String audioFilename) {
            this.name = name;
            this.audioFilename = audioFilename;
        }

        public String getName() {
            return name;
        }

        public String getAudioFilenameSP() {
            return "sp_"+audioFilename;
        }

        public String getAudioFilenameMX() {
            return "mx_"+audioFilename;
        }
    }
}
