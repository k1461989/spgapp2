package com.kcl.spanishgrammarapp.resources.data;

import android.util.Log;

import com.kcl.spanishgrammarapp.R;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by janaldoustorres on 18/03/2016.
 */
public class Time {
    private String time_digital;
    private String time_language;
    private String clockPicName;
    private int timeAudio;

    public Time(String tE, String tL) {
        time_digital = tE;
        time_language = tL;
    }

    public Time(String tE, String tL, String p, int timeAudio) {
        time_digital = tE;
        time_language = tL;
        clockPicName = p;
        this.timeAudio = timeAudio;
    }

    public String getTime_digital() {
        return time_digital;
    }

    public String getTime_language() {
        return time_language;
    }

    public String getClockPicName() {
        return clockPicName;
    }

    public int getTimeAudio() {
        return timeAudio;
    }

    public static ArrayList<Time> getTimesFromFile(InputStream inputStream) {
        int[] timeAudioIDs = {R.raw.time_12_00, R.raw.time_12_05, R.raw.time_12_15, R.raw.time_12_30, R.raw.time_12_45, R.raw.time_12_50};
        ArrayList<Time> times = new ArrayList<>();
        try {
            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                //read questions
                for (int i = 0; i < 6; ++i) {
                    String str[] = bufferedReader.readLine().split(",");
                    String digital = str[0];
                    String words = str[1];
                    Time time = new Time(digital, words, "clock_"+digital.replace(".", ""), timeAudioIDs[i]);
                    times.add(time);
                }
                inputStream.close();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return times;
    }

}
