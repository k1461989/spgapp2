package com.kcl.spanishgrammarapp.resources.data;

/**
 * Created by janaldoustorres on 19/12/2016.
 */

public interface Word {
    public String getEnglishWord();

    public String getSpanishWord();

    public int getSpaAudio();

    public int getMexAudio();
}
